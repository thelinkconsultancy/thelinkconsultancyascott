﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    public class TLCRoleViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
