﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaSipInterfaceListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the CataleyaSipInterfaceListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    /// <summary>
    /// The CataleyaSipInterfaceListViewModel.
    /// </summary>
    public class CataleyaSipInterfaceListViewModel : CataleyaInterfaceViewModelBase
    {
        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>The port.</value>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the transport.
        /// </summary>
        /// <value>The transport.</value>
        public string Transport { get; set; }

        /// <summary>
        /// Gets or sets the shared.
        /// </summary>
        /// <value>The shared.</value>
        public string Shared { get; set; }
    }
}
