﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaIpInterfaceListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the CataleyaIpInterfaceListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    /// <summary>
    /// The CataleyaIpInterfaceListViewModel.
    /// </summary>
    public class CataleyaIpInterfaceListViewModel : CataleyaInterfaceViewModelBase
    {
    }
}
