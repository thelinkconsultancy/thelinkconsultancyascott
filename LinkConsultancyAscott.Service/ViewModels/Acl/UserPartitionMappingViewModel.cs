﻿using LinkConsultancyAscott.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LinkConsultancyAscott.Service.Repository.PartitionLabel.UserPartitionMappingRepository;

namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    public class UserPartitionMappingViewModel : ViewModelBase//AuditableViewModelBase<int, PartitionLabel>
    {
        /// <summary>
        /// Gets or sets the sip interfaces.
        /// </summary>
        /// <value>The sip interfaces.</value>
        //    [UIHint("Partition")]
        //   public IEnumerable<FilterListViewModel> Partition { get; set; }
        public UserPartitionMappingViewModel()
        {
            this.TLCRole = new TLCRoleViewModel();
            this.Partition = new PartitionLabelViewModel();
        }

        public PartitionLabelViewModel Partition { get; set; }
        public int CataleyaId { get; set; }
        public string TLCId { get; set; }
        public TLCRoleViewModel TLCRole { get; set; }
        public string CataleyaUserName { get; set; }
        public string CataleyaPassword { get; set; }
        public Nullable<int> PartitionId { get; set; }
        public Nullable<System.DateTime> LastAccountVerificationDate { get; set; }
        public Nullable<bool> Status { get; set; }
        public string TLCEmail { get; set; }


    }
}