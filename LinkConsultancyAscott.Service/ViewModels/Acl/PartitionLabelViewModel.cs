﻿using LinkConsultancyAscott.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    public class PartitionLabelViewModel : ViewModelBase//AuditableViewModelBase<int, PartitionLabel>
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int? PartitionId { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string PartitionLabels { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string PartitionLabelId { get; set; }

        /// <summary>
        /// Gets or sets the controller.
        /// </summary>
        public string IstraIp { get; set; }

        /// <summary>
        /// Gets or sets the Directory Path For Istra.
        /// </summary>
        public string DirectoryPath { get; set; }
    }
}
