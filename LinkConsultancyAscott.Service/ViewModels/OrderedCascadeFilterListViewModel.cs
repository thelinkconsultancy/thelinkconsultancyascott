﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderedCascadeFilterListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The filter list view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System;

    /// <summary>
    /// The filter list view model.
    /// </summary>
    public class OrderedCascadeFilterListViewModel : OrderedFilterListViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderedFilterListViewModel"/> class.
        /// </summary>
        public OrderedCascadeFilterListViewModel() : base()
        {
            this.ParentId = string.Empty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OrderedFilterListViewModel" /> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="displayOrder">The display order.</param>
        /// <param name="parentId">The parent identifier.</param>
        public OrderedCascadeFilterListViewModel(string id, string name, int displayOrder, string parentId) : base(id, name, displayOrder)
        {
            this.ParentId = parentId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderedFilterListViewModel" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="displayOrder">The display order.</param>
        /// <param name="parentId">The parent identifier.</param>
        public OrderedCascadeFilterListViewModel(int id, string name, int displayOrder, int parentId) : base( id,  name,  displayOrder)
        {
            this.ParentId = parentId.ToString();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string ParentId { get; set; }
    }
}