﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderedFilterListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Ordered Filter List View Model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System;

    /// <summary>
    /// The filter list view model.
    /// </summary>
    public class OrderedFilterListViewModel : IComparable<OrderedFilterListViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderedFilterListViewModel"/> class.
        /// </summary>
        public OrderedFilterListViewModel()
        {
            this.Id = string.Empty;
            this.Name = string.Empty;
            this.DisplayOrder = 0;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OrderedFilterListViewModel" /> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="displayOrder">The display order.</param>
        public OrderedFilterListViewModel(string id, string name, int displayOrder)
        {
            this.Id = id;
            this.Name = name;
            this.DisplayOrder = displayOrder;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderedFilterListViewModel" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="displayOrder">The display order.</param>
        public OrderedFilterListViewModel(int id, string name, int displayOrder)
        {
            this.Id = id.ToString();
            this.Name = name;
            this.DisplayOrder = displayOrder;
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the display order.
        /// </summary>
        /// <value>The display order.</value>
        public int DisplayOrder { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="other">The object.</param>
        /// <returns>System Int32.</returns>
        public int CompareTo(OrderedFilterListViewModel other)
        {
            return this.DisplayOrder.CompareTo(other.DisplayOrder);
        }
    }
}