﻿using LinkConsultancyAscott.Service.Models.Cataleya.Common;
using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.ViewModels.Cataleya
{
    public class Operator
    {
        public int id { get; set; }
        public string name { get; set; }
        public object description { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public int? partitionCacId { get; set; }
        public int sdrCtrlParamsId { get; set; }
        public object contactDetails { get; set; }
        public List<object> mediations { get; set; }
        public object nodeId { get; set; }
        public int operatorId { get; set; }
    }

    public class TransparencyProfile
    {
        public CompoundKey compoundKey { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int ver { get; set; }
        public string topology { get; set; }
        public string callId { get; set; }
        public string header { get; set; }
        public string body { get; set; }
        public int operatorId { get; set; }
        public Operator @operator { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }

    public class SipAdaptationFramework
    {
        public CompoundKey compoundKey { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int ver { get; set; }
        public string sipAllowHeaders { get; set; }
        public string sipRule { get; set; }
        public int chmod { get; set; }
        public int operatorId { get; set; }
        public Operator @operator { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }

    public class SipProfile
    {
        public CompoundKey compoundKey { get; set; }
        public string name { get; set; }
        public int ver { get; set; }
        public string description { get; set; }
        public string methodsallowed { get; set; }
        public int maxRetxCount { get; set; }
        public int timerT1 { get; set; }
        public int timerT2 { get; set; }
        public int timerC { get; set; }
        public int minSessionTimer { get; set; }
        public int sessionTimer { get; set; }
        public string reliableReq { get; set; }
        public string reliableResp { get; set; }
        public int optionsTimer { get; set; }
        public string optionsForwarding { get; set; }
        public int minRegInterval { get; set; }
        public int maxRegInterval { get; set; }
        public string feNatMode { get; set; }
        public int dummyPktInterval { get; set; }
        public string regThrotteling { get; set; }
        public int ueRegInterval { get; set; }
        public int networkRegInterval { get; set; }
        public string refresherType { get; set; }
        public int operatorId { get; set; }
        public Operator @operator { get; set; }
        public int? chmod { get; set; }
        public string enableSessionTimer { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }

    public class MediaProfile
    {
        public CompoundKey compoundKey { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string mediaProxy { get; set; }
        public string mediaLatch { get; set; }
        public string codecSelection { get; set; }
        public int ver { get; set; }
        public int operatorId { get; set; }
        public string dtmfMode { get; set; }
        public Operator @operator { get; set; }
        public List<object> audioCodecs { get; set; }
        public List<object> videoCodecs { get; set; }
        public List<object> imageCodecs { get; set; }
        public string srtpMode { get; set; }
        public string srtpSrtcpEnabled { get; set; }
        public string srtpAllowPassThru { get; set; }
        public string srtpCryptoSuiteList { get; set; }
        public string srtpAuthEnabled { get; set; }
        public int? chmod { get; set; }
        public string keyExchangeMode { get; set; }
        public object dtlsProfileId { get; set; }
        public object dtlsProfile { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }

    public class CacProfile
    {
        public CompoundKey compoundKey { get; set; }
        public int ver { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int maxSessions { get; set; }
        public int maxSessionsIn { get; set; }
        public int maxSessionsOut { get; set; }
        public int rateSessionsIn { get; set; }
        public int rateSessionsOut { get; set; }
        public int rateXcodSessions { get; set; }
        public int maxXcodSessions { get; set; }
        public int burstRate { get; set; }
        public int burstDuration { get; set; }
        public int nonInvTxnRate { get; set; }
        public int pktRate { get; set; }
        public int mediaBandwidth { get; set; }
        public int regTxnRate { get; set; }
        public int maxRegistrations { get; set; }
        public int operatorId { get; set; }
        public int? chmod { get; set; }
        public string cacProfileType { get; set; }
        public Operator @operator { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }

    public class XcodProfile
    {
        public CompoundKey compoundKey { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int ver { get; set; }
        public string commonParameters { get; set; }
        public string jitterMode { get; set; }
        public string amrMode { get; set; }
        public string silenceSuppression { get; set; }
        public string negotiateFaxRelayInSignaling { get; set; }
        public int operatorId { get; set; }
        public Operator @operator { get; set; }
        public List<object> audioCodecs { get; set; }
        public List<object> videoCodecs { get; set; }
        public List<object> imageCodecs { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }
    
    //public class SipInterface
    //{
    //    public CompoundKey compoundKey { get; set; }
    //    public string name { get; set; }
    //    public string description { get; set; }
    //    public int port { get; set; }
    //    public string transport { get; set; }
    //    public string shared { get; set; }
    //    public int ver { get; set; }
    //    public int ipInterfaceId { get; set; }
    //    public int operatorId { get; set; }
    //    public int? chmod { get; set; }
    //    public IpInterface ipInterface { get; set; }
    //    public Operator @operator { get; set; }
    //    public int nodeId { get; set; }
    //    public int id { get; set; }
    //}

    public class DmRule
    {
        public int id { get; set; }
        public string numberType { get; set; }
        public string @operator { get; set; }
        public string value { get; set; }
        public string action { get; set; }
        public string actionValue { get; set; }
        public int digitManipRulesId { get; set; }
        public object nodeId { get; set; }
        public object operatorId { get; set; }
    }

    public class Mediation
    {
        public int id { get; set; }
        public int operatorId { get; set; }
        public int mediationId { get; set; }
        public object nodeId { get; set; }
    }

    public class IDmRules
    {
        public int id { get; set; }
        public int ver { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<DmRule> dmRules { get; set; }
        public int operatorId { get; set; }
        public Operator @operator { get; set; }
        public object nodeId { get; set; }
    }

    public class EDmRules
    {
        public int id { get; set; }
        public int ver { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<DmRule> dmRules { get; set; }
        public int operatorId { get; set; }
        public Operator @operator { get; set; }
        public object nodeId { get; set; }
    }

    public class UeCacProfile
    {
        public CompoundKey compoundKey { get; set; }
        public int ver { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int maxSessions { get; set; }
        public int maxSessionsIn { get; set; }
        public int maxSessionsOut { get; set; }
        public int rateSessionsIn { get; set; }
        public int rateSessionsOut { get; set; }
        public int rateXcodSessions { get; set; }
        public int maxXcodSessions { get; set; }
        public int burstRate { get; set; }
        public int burstDuration { get; set; }
        public int nonInvTxnRate { get; set; }
        public int pktRate { get; set; }
        public int mediaBandwidth { get; set; }
        public int regTxnRate { get; set; }
        public int maxRegistrations { get; set; }
        public int operatorId { get; set; }
        public object chmod { get; set; }
        public string cacProfileType { get; set; }
        public Operator @operator { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }

    public class Datum
    {
        public CompoundKey compoundKey { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string customer { get; set; }
        public int? egressZone { get; set; }
        public int? routingPolicy { get; set; }
        public int? registrationPolicy { get; set; }
        public string redirectMode { get; set; }
        public int maxReroutes { get; set; }
        public object localDomain { get; set; }
        public int ver { get; set; }
        public string zoneType { get; set; }
        public object ioi { get; set; }
        public string tgId { get; set; }
        public string trunkGroupType { get; set; }
        public string timeZone { get; set; }
        public int? siprulesId { get; set; }
        public string serviceLevel { get; set; }
        public object signalingTos { get; set; }
        public object mediaTos { get; set; }
        public string trustLevel { get; set; }
        public int blkInterval { get; set; }
        public int blkRegInterval { get; set; }
        public int blkThInvSessionPct { get; set; }
        public int blkThMalformedMsgRate { get; set; }
        public int blkThNonInvTxnPct { get; set; }
        public int blkThRegRejCount { get; set; }
        public int blkThRegTxnPct { get; set; }
        public int? blkThUnAuthMsgRejCount { get; set; }
        public object tlsprofileId { get; set; }
        public string privacy { get; set; }
        public string callingPtyIdHdr { get; set; }
        public string qosAnalysis { get; set; }
        public int networkRtDelay { get; set; }
        public int operatorId { get; set; }
        public string operationalState { get; set; }
        public string adminState { get; set; }
        public int sipprofileId { get; set; }
        public int mediaprofileId { get; set; }
        public int cacprofileId { get; set; }
        public string xcodEnabled { get; set; }
        public int? xcodprofileId { get; set; }
        public string traceOn { get; set; }
        public int? transparencyprofileId { get; set; }
        public int maxCallDuration { get; set; }
        public int? gracefulPeriod { get; set; }
        public int? chmod { get; set; }
        public string siptEnabled { get; set; }
        public string isupBase { get; set; }
        public string isupVer { get; set; }
        public object servicePolicyId { get; set; }
        public string ldcHandlerType { get; set; }
        public int? mediaInactivityTimer { get; set; }
        public string mediaInactivityPolicy { get; set; }
        public int? byeTimer { get; set; }
        public string trustDomain { get; set; }
        public string qosPreconditionEnabled { get; set; }
        public object segmentedPreconditions { get; set; }
        public List<object> e2ePreconditions { get; set; }
        public object iwfProfileId { get; set; }
        public string sendQosReport { get; set; }
        public object qosAnalysisRptDuration { get; set; }
        public object qosAnalysisGap { get; set; }
        public int? iDmRulesId { get; set; }
        public int? eDmRulesId { get; set; }
        public int? ueCacprofileId { get; set; }
        public string localRingbackType { get; set; }
        public object localRingbackAnnId { get; set; }
        public object allowedPrefixList { get; set; }
        public TransparencyProfile transparencyProfile { get; set; }
        public object tlsProfile { get; set; }
        public Operator @operator { get; set; }
        public object egressZoneObj { get; set; }
        public object registrationPolicyObj { get; set; }
        public object routingPolicyObj { get; set; }
        public SipAdaptationFramework sipAdaptationFramework { get; set; }
        public SipProfile sipProfile { get; set; }
        public MediaProfile mediaProfile { get; set; }
        public CacProfile cacProfile { get; set; }
        public XcodProfile xcodProfile { get; set; }
        public List<object> remoteEndPoints { get; set; }
        public List<object> sourceLists { get; set; }
        public List<object> mediaInterfaces { get; set; }
        public List<SipInterface> sipInterfaces { get; set; }
        public object transientEgressZone { get; set; }
        public object servicePolicy { get; set; }
        public object iwfProfile { get; set; }
        public IDmRules iDmRules { get; set; }
        public EDmRules eDmRules { get; set; }
        public UeCacProfile ueCacProfile { get; set; }
        public object localRingbackAnn { get; set; }
        public string recordDtmfDigits { get; set; }
        public int nodeId { get; set; }
        public int id { get; set; }
    }

    public class CataleyaZoneViewModel 
    {
        public bool success { get; set; }
        public object message { get; set; }
        public object detailErrorMessage { get; set; }
        public int total { get; set; }
        public object pageNumber { get; set; }
        public object resultsPerPage { get; set; }
        public List<Datum> data { get; set; }
        public object objId { get; set; }
    }
}
