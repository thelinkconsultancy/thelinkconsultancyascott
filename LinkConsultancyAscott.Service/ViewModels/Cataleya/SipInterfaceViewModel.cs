﻿namespace LinkConsultancyAscott.Service.ViewModels.Cataleya
{
    public class SipInterfaceViewModel
    {
        public int InterfaceId { get; set; }
        public int ZoneId { get; set; }
        public string InterfaceName { get; set; }
        public string InterfaceDescription { get; set; }
        public string Port { get; set; }
        public int IpInterfaceId { get; set; }
        public string ipSubnetId { get; set; }
        public int NodeId { get; set; }
    }
}
