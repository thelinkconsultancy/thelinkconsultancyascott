﻿using System;

namespace LinkConsultancyAscott.Service.ViewModels.Cataleya
{
    public class CataleyaAclHistoryViewModel
    {
        public int Id { get; set; }
        public int NodeId { get; set; }
        public int SipInterfaceId { get; set; }
        public string Action { get; set; }
        public string AppType { get; set; }
        public string Transport { get; set; }
        public string SipInterfaceAddressType { get; set; }
        public string RemoteIpAddress { get; set; }
        public string SipInterfaceIpAddress { get; set; }
        public string RemotePort { get; set; }
        public string LocalPort { get; set; }
        public string RemotePrefix { get; set; }
        public int Ver { get; set; }
        public DateTime CurrentDate { get; set; }
        public bool isDeleted { get; set; }
        public int BatchNo { get; set; }
        public int? CataleyaSecurityAclId { get; set; }
        public string NodeName { get; set; }
    }
}
