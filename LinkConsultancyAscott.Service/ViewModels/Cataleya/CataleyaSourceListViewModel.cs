﻿namespace LinkConsultancyAscott.Service.ViewModels.Cataleya
{
    public class CataleyaSourceListViewModel
    {
        public int id { get; set; }
        public string addressType { get; set; }
        public string ipAddress { get; set; }
        public string subnetMask { get; set; }
        public int zoneId { get; set; }
        public int nodeId { get; set; }
        public int? operatorId { get; set; }
    }
}
