﻿using LinkConsultancyAscott.Service.Models.Cataleya.Common;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.ViewModels.Cataleya
{
    public class Platform
    {
        public CompoundKey compoundKey { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string uid { get; set; }
        public string mgmtIp { get; set; }
        public string haIp { get; set; }
        public string npuHostIp { get; set; }
        public int ver { get; set; }
        public string description { get; set; }
        public string swVersion { get; set; }
        public string location { get; set; }
        public int actualAction { get; set; }
        public int adminState { get; set; }
        public object transitionalState { get; set; }
        public string adminStateChangeTs { get; set; }
        public int opState { get; set; }
        public int prevOpState { get; set; }
        public string opStateChangeTs { get; set; }
        public int linkStatus { get; set; }
        public string linkStatusChangeTs { get; set; }
        public string opStateLabel { get; set; }
    }

    public class CataleyaNodeViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string centralMgmtIp { get; set; }
        public string redundancy { get; set; }
        public int ver { get; set; }
        public string positionX { get; set; }
        public string positionY { get; set; }
        public string description { get; set; }
        public string timeZone { get; set; }
        public string location { get; set; }
        public string contact { get; set; }
        public List<Platform> platforms { get; set; }
        //    public class CataleyaNodeViewModel
        //{
        //}
    }
}