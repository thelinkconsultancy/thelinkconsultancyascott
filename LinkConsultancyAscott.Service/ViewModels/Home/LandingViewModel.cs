﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LandingViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Landing View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Home
{
    using System.Collections.Generic;

    /// <summary>
    /// The Page Data View Model.
    /// </summary>
    public class LandingViewModel : PageDataViewModel 
    {

        
    }
}
