﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageFilterListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The filter list view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    /// <summary>
    /// The filter list view model.
    /// </summary>
    public class ImageFilterListViewModel : FilterListViewModel
    {
       /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string ImageFile { get; set; }

        /// <summary>
        /// Gets or sets the display order.
        /// </summary>
        /// <value>The display order.</value>
        public int DisplayOrder { get; set; }
    }
}