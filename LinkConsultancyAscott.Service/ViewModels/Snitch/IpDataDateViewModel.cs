﻿namespace LinkConsultancyAscott.Service.ViewModels.Snitch
{
    public class IpDataDateViewModel
    {
        public string LastUpdate { get; set; }
        public string DataCount { get; set; }
    }
}
