﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LookUpViewModelBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The look up view model base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    using Data;

    /// <summary>
    /// Class LookUpViewModelBase.
    /// </summary>
    /// <typeparam name="T"> The database type</typeparam>
    /// <typeparam name="TM">The type of the tm.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.ViewModels.AuditableViewModelBase{T, TM}" />
    public abstract class LookUpViewModelBase<T, TM> : BasicLookUpViewModelBase<T, TM>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where TM : ILookUp<T>, IAuditedEntity<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LookUpViewModelBase{T, TM}"/> class.
        /// </summary>
        protected LookUpViewModelBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LookUpViewModelBase{T, TM}"/> class.
        /// </summary>
        /// <param name="details">The details.</param>
        protected LookUpViewModelBase(TM details) : base(details)
        {
            this.Name = details.Name;
        }

        /// <summary>
        /// Sets the data object.
        /// </summary>
        /// <value>The data object.</value>
        public override TM DataObject
        {
            set
            {
                base.DataObject = value;
                this.Name = this.details.Name;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [Required(ErrorMessage = "Name is required")]
        [DisplayName("Name")]
        [AllowHtml]
        [StringLength(250)]
        [Display(Order = 20)]
        public string Name { get; set; }

        /// <summary>
        /// Updates the data object.
        /// </summary>
        /// <param name="dto">The details.</param>
        public override void UpdateDataObject(TM dto)
        {
            base.UpdateDataObject(dto);
            dto.Name = this.Name;
        }
    }
}