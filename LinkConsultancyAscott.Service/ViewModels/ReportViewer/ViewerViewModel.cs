﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewerViewModel.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the ViewerViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LinkConsultancyAscott.Service.ViewModels.ReportViewer
{
    using System.Collections.Generic;

    /// <summary>
    /// The viewer view model.
    /// </summary>
    public class ViewerViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewerViewModel"/> class.
        /// </summary>
        public ViewerViewModel()
        {
            this.Parameters = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets or sets the report name.
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// Gets or sets the parameters.
        /// </summary>
        public Dictionary<string, string> Parameters { get; set; }
    }
}
