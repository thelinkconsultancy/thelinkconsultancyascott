﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTypeViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the EmailType View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The EmailType View Model.
    /// </summary>
    public class EmailTypeViewModel : LookUpViewModelBase<int, EmailType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTypeViewModel"/> class.
        /// </summary>
        public EmailTypeViewModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTypeViewModel"/> class.
        /// </summary>
        /// <param name="details">The details.</param>
        public EmailTypeViewModel(EmailType details)
            : base(details)
        {
        }
    }
}
