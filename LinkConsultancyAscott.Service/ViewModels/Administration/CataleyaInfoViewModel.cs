﻿

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteActivityLogViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Site Activity Log View Model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using System;

    /// <summary>
    /// The Log View Model.
    /// </summary>
    public class CataleyaInfoViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public long CataleyaId { get; set; }
       
        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string TLCId { get; set; }

        /// <summary>
        /// Gets or sets the controller.
        /// </summary>
        public int TLCRole { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        public string CataleyaUserName { get; set; }

        /// <summary>
        /// Gets or sets the area.
        /// </summary>
        /// <value>The area.</value>
        public string CataleyaPassword { get; set; }
        public string TLCEmail { get; set; }
        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        public int PartitionId { get; set; }

    }
}
