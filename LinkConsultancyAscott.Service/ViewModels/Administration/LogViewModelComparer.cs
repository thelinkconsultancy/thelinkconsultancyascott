﻿namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Class LogViewModelComparer.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.IEqualityComparer{LogViewModel}" />
    class LogViewModelComparer : IEqualityComparer<LogViewModel>
    {
        /// <summary>
        /// The field
        /// </summary>
        private readonly string field;

        /// <summary>
        /// The property
        /// </summary>
        private readonly PropertyInfo prop;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogViewModelComparer"/> class.
        /// </summary>
        /// <param name="field">The field.</param>
        public LogViewModelComparer(string field)
        {
            this.field = field;
            this.prop = typeof(LogViewModel).GetProperty(this.field);
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type </param>
        /// <param name="y">The second object of type </param>
        /// <returns>true if the specified objects are equal; otherwise, false.</returns>
        public bool Equals(LogViewModel x, LogViewModel y)
        {
            var valueX = this.prop.GetValue(x, null);
            var valueY = this.prop.GetValue(y, null);
            if (valueX == null)
            {
                return valueY == null;
            }
            return valueX.Equals(valueY);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object" /> for which a hash code is to be returned.</param>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        public int GetHashCode(LogViewModel obj)
        {
            var value = this.prop.GetValue(obj, null);
            if (value == null)
            {
                return 0;
            }

            return value.GetHashCode();
        }
    }
}
