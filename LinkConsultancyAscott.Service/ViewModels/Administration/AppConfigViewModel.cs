﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConfigViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AppConfig View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The AppConfig View Model.
    /// </summary>
    public class AppConfigViewModel : LookUpViewModelBase<int, AppConfig>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfigViewModel"/> class.
        /// </summary>
        public AppConfigViewModel()
        {
            this.Value = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfigViewModel"/> class.
        /// </summary>
        /// <param name="details">The details.</param>
        public AppConfigViewModel(AppConfig details)
            : base(details)
        {
            this.Value = details.Value;
        }


        /// <summary>
        /// Gets or sets the name of the icon.
        /// </summary>
        /// <value>The name of the icon.</value>
        [DisplayName("Value")]
        [Display(Order = 800)]
        public string Value { get; set; }

        /// <summary>
        /// Sets the data object.
        /// </summary>
        /// <value>The data object.</value>
        public override AppConfig DataObject
        {
            set
            {
                base.DataObject = value;
                this.Value = this.details.Value;
            }
        }

        /// <summary>
        /// Updates the data object.
        /// </summary>
        /// <param name="dto">The details.</param>
        public override void UpdateDataObject(AppConfig dto)
        {
            base.UpdateDataObject(dto);
            dto.Value = this.Value;
        }
    }
}
