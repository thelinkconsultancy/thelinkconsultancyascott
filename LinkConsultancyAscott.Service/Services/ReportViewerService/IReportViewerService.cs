﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReportViewerService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The IReportViewerService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.ReportViewerService
{
    using System.Collections.Generic;

    using LinkConsultancyAscott.Service.Types;

    using Telerik.Reporting.Processing;

    /// <summary>
    /// The IReportViewerService interface.
    /// </summary>
    public interface IReportViewerService : IServiceBase
    {
        RenderingResult ExportToPdf(string reportName, RenderingExtensions extension, Dictionary<string,string> parameters);
    }
}
