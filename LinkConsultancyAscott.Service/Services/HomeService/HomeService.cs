﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Home Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.HomeService
{
    /// <summary>
    /// Class HomeService.
    /// </summary>
    public class HomeService : IHomeService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeService"/> class.
        /// </summary>
        public HomeService()
        {
        }

    }
}