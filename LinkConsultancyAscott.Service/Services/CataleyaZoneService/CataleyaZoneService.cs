﻿using LinkConsultancyAscott.Service.Models.Cataleya.Common;
using LinkConsultancyAscott.Service.Repository.CataleyaNodeRepository;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Services.CataleyaZoneService
{
    public class CataleyaZoneService : ServiceBase, ICataleyaZoneService
    {
        private readonly ICataleyaZoneRepository cataleyaZoneRepository;

        public CataleyaZoneService(ICataleyaZoneRepository cataleyaZoneRepository)
        {
            this.cataleyaZoneRepository = cataleyaZoneRepository;
        }

        public IList<CataleyaZoneViewModel> ReadCataleyaZoneList()
        {
            IList<CataleyaZoneViewModel> obj = this.cataleyaZoneRepository.Get();
            return new List<CataleyaZoneViewModel>(obj);
        }

        public Datum CreateCataleyaZone(Datum viewModel)
        {
            Datum obj = new Datum();
            obj.compoundKey = new CompoundKey();
            obj.compoundKey.id = Convert.ToInt32(viewModel.compoundKey.id);
            obj.compoundKey.nodeid = Convert.ToInt32(viewModel.compoundKey.nodeid);
            obj.operatorId = Convert.ToInt32(viewModel.operatorId);
            obj.name = viewModel.name;
            obj.zoneType = viewModel.zoneType;
            obj.description = viewModel.description;
            viewModel.compoundKey.id = this.cataleyaZoneRepository.Add(obj);
            return viewModel;
        }

        public bool CheckIfCataleyaZoneAlreadyExists(int zoneId, int nodeId, int operatorId)
        {
            return this.cataleyaZoneRepository.CheckIfCataleyaZoneAlreadyExists(zoneId, nodeId, operatorId);
        }

        public void DeleteAllCataleyaZones() {
            this.cataleyaZoneRepository.DeleteAllCataleyaZones();
        }
    }
}
