﻿using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Services.CataleyaZoneService
{
    public interface ICataleyaZoneService
    {
        Datum CreateCataleyaZone(Datum viewModel);

        IList<CataleyaZoneViewModel> ReadCataleyaZoneList();

        bool CheckIfCataleyaZoneAlreadyExists(int zoneId, int nodeId, int operatorId);

        void DeleteAllCataleyaZones();
    }
}
