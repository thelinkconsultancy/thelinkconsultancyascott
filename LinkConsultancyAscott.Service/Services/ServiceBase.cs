﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The ServiceBase base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// The service base.
    /// </summary>
    public abstract class ServiceBase : IServiceBase
    {
        /// <summary>
        /// The log.
        /// </summary>
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected IEnumerable<int> StringToIntList(string intListString)
        {
            var intArray = (intListString ?? string.Empty).Split(',').Select<string, int>(int.Parse);
            return intArray;
        }

        public void LogError(string error)
        {
            TextWriter tsw = new StreamWriter(@"E:\" + DateTime.Now.ToString("MM.dd.yyyy") + "_CataleyaErrorLog.txt", true);
            //Writing text to the file.
            string textData = error;
            tsw.WriteLine(textData);
            //Close the file.
            tsw.Close();
        }
    }

}
