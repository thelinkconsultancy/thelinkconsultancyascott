﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteActivityLogService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Site Activity Log Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.SiteActivityLogService
{
    using System.Collections.Generic;
    using System.Linq;

    using Repository.SiteActivityLogRepository;
    using ViewModels.Administration;

    /// <summary>
    /// Class SiteActivityLogService.
    /// </summary>
    public class SiteActivityLogService : ISiteActivityLogService
    {
        /// <summary>
        /// The repository
        /// </summary>
        private readonly ISiteActivityLogRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteActivityLogService"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public SiteActivityLogService(ISiteActivityLogRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <returns>
        /// The lit of view models.
        /// </returns>
        public IEnumerable<SiteActivityLogViewModel> Read()
        {
            return this.repository.GetAll().Select(
                    n => new SiteActivityLogViewModel
                    {
                        Id = n.Id,
                        Action = n.Action,
                        ActionDuration = n.ActionDuration,
                        IpAddress = n.IPAddress,
                        UserName = n.UserName,
                        ActivityTimestamp = n.ActivityTimestamp,
                        Browser = n.Browser,
                        Area = n.Area,
                        Controller = n.Controller,
                        Domain = n.Domain,
                        IsMobileDevice = n.IsMobileDevice,
                        Platform = n.Platform,
                        RouteInfo = n.RouteInfo,
                        HttpMethod = n.HttpVerb,
                        ResponseStatusDescription = n.ResponseStatusDescription,
                        ResponseData = n.ResponseData
                    });
        }
    }
}