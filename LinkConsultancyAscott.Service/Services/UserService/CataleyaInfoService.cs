﻿


// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the User Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.UserService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Security;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Repository.RoleRepository;
    using LinkConsultancyAscott.Service.Repository.UserRepository;
    using LinkConsultancyAscott.Service.Services.CacheService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels;
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    using Newtonsoft.Json;

    /// <summary>
    /// The account membership service.
    /// </summary>
    public class CataleyaInfoService : ICataleyaInfoService
    {

        /// <summary>
        /// The _provider.
        /// </summary> ICalaleyaInfoRepository
        private readonly ICalaleyaInfoRepository cataleyaRepository;



        /// <summary>
        /// Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="appConfigRepository">The application configuration repository.</param>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="cacheService">The cache service.</param>
        public CataleyaInfoService(

            ICalaleyaInfoRepository cataleyaRepository
           )
        {

            this.cataleyaRepository = cataleyaRepository;

        }

        /// <summary>
        /// The create user.
        /// </summary>
        /// <param name="viewModel">
        /// The user view model.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        public CataleyaInfoViewModel CreateCataleyaInfoUser(CataleyaInfoViewModel viewModel)
        {
            CataleyaInformation obj = new CataleyaInformation();
            obj.CataleyaId = Convert.ToInt32(viewModel.CataleyaId);
            obj.TLCId = viewModel.TLCId;
            obj.TLCRole = viewModel.TLCRole;
            obj.CataleyaUserName = viewModel.CataleyaUserName;
            obj.CataleyaPassword = viewModel.CataleyaPassword;
            obj.PartitionId = viewModel.PartitionId;
            obj.TLCEmail = viewModel.TLCEmail;
            this.cataleyaRepository.Add(obj);
            return viewModel;// this.GetUserByName(viewModel.UserName);
        }

    }
}
