﻿

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IUserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.UserService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    /// <summary>
    /// The User Service Interface.
    /// </summary>
    public interface ICataleyaInfoService
    {
        /// <summary>
        /// The create user.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        CataleyaInfoViewModel CreateCataleyaInfoUser(CataleyaInfoViewModel viewModel);

        
    }
}

