﻿using LinkConsultancyAscott.Service.Repository.SnitchRepository;
using LinkConsultancyAscott.Service.ViewModels.Snitch;
using System.Linq;

namespace LinkConsultancyAscott.Service.Services.SnitchService
{
    public class SnitchService : ServiceBase, ISnitchService
    {
        private readonly ISnitchRepository snitchRepository;

        public SnitchService(ISnitchRepository snitchRepository)
        {
            this.snitchRepository = snitchRepository;
        }

        public IpDataDateViewModel ReadIpDataDateList()
        {
            return this.snitchRepository.Get().Last();
        }
    }
}
