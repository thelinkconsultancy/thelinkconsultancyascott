﻿using LinkConsultancyAscott.Service.ViewModels.Snitch;

namespace LinkConsultancyAscott.Service.Services.SnitchService
{
    public interface ISnitchService
    {
        IpDataDateViewModel ReadIpDataDateList();
    }
}
