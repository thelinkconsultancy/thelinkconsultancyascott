﻿using LinkConsultancyAscott.Service.Models.Cataleya.Common;
using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;
using LinkConsultancyAscott.Service.Repository.CataleyaSipInterfaceRepository;
using System;

namespace LinkConsultancyAscott.Service.Services.SipInterfaceService
{
    public class CataleyaSipInterfaceService : ServiceBase, ICataleyaSipInterfaceService
    {
        private readonly ICataleyaSipInterfaceRepository cataleyaSipInterfaceRepository;

        public CataleyaSipInterfaceService(ICataleyaSipInterfaceRepository cataleyaSipInterfaceRepository)
        {
            this.cataleyaSipInterfaceRepository = cataleyaSipInterfaceRepository;
        }

        public SipInterface CreateCataleyaSipInterface(int nodeId, int zoneId, SipInterface sipInterface)
        {
            try
            {
                SipInterface obj = new SipInterface();
                obj.ipInterface = new IpInterface();
                obj.compoundKey = new CompoundKey();
                obj.compoundKey.id = Convert.ToInt32(sipInterface.compoundKey.id);
                obj.name = sipInterface.name;
                obj.description = sipInterface.description;
                obj.port = sipInterface.port;
                obj.ipInterfaceId = sipInterface.ipInterfaceId;
                obj.ipInterface.ipaddress = sipInterface.ipInterface.ipaddress;
                sipInterface.compoundKey.id = this.cataleyaSipInterfaceRepository.Add(nodeId, zoneId, obj);
                return sipInterface;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public bool CheckIfCataleyaSipInterfaceAlreadyExists(int interfaceId, int zoneId)
        {
            return this.cataleyaSipInterfaceRepository.CheckIfSipInterfaceAlreadyExists(interfaceId, zoneId);
        }

        public void DeleteAllCataleyaSipInterfaces() {
            this.cataleyaSipInterfaceRepository.DeleteAllCataleyaSipInterfaces();
        }
    }
}
