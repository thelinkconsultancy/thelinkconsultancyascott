﻿using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;

namespace LinkConsultancyAscott.Service.Services.SipInterfaceService
{
    public interface ICataleyaSipInterfaceService
    {
        SipInterface CreateCataleyaSipInterface(int nodeId, int zoneId, SipInterface sipInterface);

        bool CheckIfCataleyaSipInterfaceAlreadyExists(int interfaceId, int zoneId);

        void DeleteAllCataleyaSipInterfaces();
    }
}
