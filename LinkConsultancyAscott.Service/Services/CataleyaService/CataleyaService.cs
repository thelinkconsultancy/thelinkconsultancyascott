﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the CATALEYA Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CataleyaService
{
    using LinkConsultancyAscott.Service.Extensions;
    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.MediaInterface;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone;
    using LinkConsultancyAscott.Service.Models.Cataleya.Token;
    using LinkConsultancyAscott.Service.Repository.AclHistoryRepository;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Repository.UserRepository;
    using LinkConsultancyAscott.Service.Services.AclService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using LinkConsultancyAscott.Service.ViewModels.Cataleya;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Class CATALEYA Service.
    /// </summary>
    public class CataleyaService : ServiceBase, ICataleyaService
    {
        /// <summary>
        /// The CATALEYA API URL
        /// </summary>
        private readonly string cataleyaApiUrl;

        /// <summary>
        /// The CATALEYA username
        /// </summary>
        private string cataleyaUsername;

        /// <summary>
        /// The CATALEYA password
        /// </summary>
        private string cataleyaPassword;

        /// <summary>
        /// The default node identifier
        /// </summary>
        private readonly int defaultNodeId;

        /// <summary>
        /// The security ACL route
        /// </summary>
        private readonly string securityAclRoute = "api/configuration/security_acl/";

        /// <summary>
        /// The configuration interface route
        /// </summary>
        private readonly string configurationInterfaceRoute = "api/configuration/{0}_interface/";

        /// <summary>
        /// The configuration IP interface route
        /// </summary>
        private readonly string configurationIpInterfaceRoute = "api/configuration/ip_subnet/ip_interface/";

        /// <summary>
        /// The software about route
        /// </summary>
        private readonly string softwareAboutRoute = "api/software/about";

        /// <summary>
        /// The zone route
        /// </summary>
        private readonly string zoneRoute = "api/configuration/zone/";

        private readonly string getAllNodesRoute = "api/configuration/getallnodes";

        private readonly string getAllZonesRoute = "api/configuration/zone/get";

        private string loggedInUserName = "";

        private string cataleyaUserEmail = "";

        private ICalaleyaInfoRepository calaleyaInfoRepository;

        private readonly IAclHistoryRepository aclHistoryRepository;

        UserPartitionMappingViewModel userPartitionMappingViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="CataleyaService" /> class.
        /// </summary>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="appConfigRepository">The application configuration repository.</param>
        public CataleyaService(ICalaleyaInfoRepository calaleyaInfoRepository,
            ICurrentUserService currentUserService, IAppConfigRepository appConfigRepository,
            IAclHistoryRepository aclHistoryRepository)
        {
            this.aclHistoryRepository = aclHistoryRepository;
            this.calaleyaInfoRepository = calaleyaInfoRepository;
            this.loggedInUserName = currentUserService.UserName();
            this.cataleyaApiUrl = currentUserService.CataleyaApiUrl;
            this.cataleyaUsername = currentUserService.CataleyaUsername;
            this.cataleyaPassword = currentUserService.CataleyaPassword;
            this.defaultNodeId = currentUserService.CataleyaDefaultNodeId;

            // If the paramters aren't set fall back to defaults
            if (string.IsNullOrEmpty(this.cataleyaApiUrl))
            {
                this.cataleyaApiUrl = appConfigRepository.GetValue(AppConstants.CataleyaApiUrlKey);
                this.cataleyaUsername = appConfigRepository.GetValue(AppConstants.CataleyaUsernameKey);
                this.cataleyaPassword = appConfigRepository.GetValue(AppConstants.CataleyaPasswordKey);
                this.defaultNodeId = Convert.ToInt32(appConfigRepository.GetValue(AppConstants.CataleyaDefaultNodeId));
            }
        }

        /// <summary>
        /// Creates the specified acl entity model.
        /// </summary>
        /// <param name="aclEntityModel">The ACL entity model.</param>
        /// <returns>Returns Task.</returns>
        public async Task<CreateSecurityAclResponse> CreateSecurityAcl(AclEntityModel aclEntityModel)
        {
            var requestString = await this.BuildWebApiUrl(this.securityAclRoute + "create").ConfigureAwait(false);
            CreateSecurityAclResponse retVal = null;

            // Convert to the ACL Entity Model to the Cataleya version
            var aclEntity = this.BuildAcl(aclEntityModel);

            string serializedObject = JsonConvert.SerializeObject(aclEntity);
            var jsonContent = new StringContent(serializedObject, Encoding.UTF8, "application/json");
            var response = await this.PostAsync(requestString, jsonContent).ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                Log.Error("Create failed");
                var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                Log.Error(jsonString);
            }
            else
            {
                // Process the returned result
                var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                retVal = JsonConvert.DeserializeObject<CreateSecurityAclResponse>(jsonString);
            }

            return retVal;
        }

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Returns Task.</returns>
        public async Task<DeleteSecurityAclResponse> DeleteSecurityAcl(CompoundKey key)
        {
            DeleteSecurityAclResponse retVal = null;

            var requestString = await this.BuildWebApiUrl(this.securityAclRoute + "delete").ConfigureAwait(false);
            requestString += $"&id={key.id}&nodeId={key.nodeid}";

            var response = await this.DeleteAsync(requestString).ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                return retVal;
            }
            else
            {
                // Process the returned result
                var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                retVal = JsonConvert.DeserializeObject<DeleteSecurityAclResponse>(jsonString);
            }

            return retVal;
        }

        /// <summary>
        /// Lists all.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns data.</returns>
        public async Task<SecurityAcl> SecurityAclListAsync(int? nodeId)
        {
            try
            {
                // Build the request string
                var requestString = await this.BuildWebApiUrl(this.securityAclRoute + "get").ConfigureAwait(false);
                requestString += "&nodeId=" + (nodeId ?? this.defaultNodeId);

                var retVal = await this.GetRequest<SecurityAcl>(requestString).ConfigureAwait(false);

                return retVal;
            }
            catch (Exception e)
            {
                LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Lists One.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns data.</returns>
        public async Task<SecurityAcl> SecurityAclByIdAsync(int id, int? nodeId)
        {
            try
            {
                // Build the request string
                var requestString = await this.BuildWebApiUrl(this.securityAclRoute + "get").ConfigureAwait(false);
                requestString += "&id=" + id + "&nodeId=" + (nodeId ?? this.defaultNodeId);

                var retVal = await this.GetRequest<SecurityAcl>(requestString).ConfigureAwait(false);

                return retVal;
            }
            catch (Exception e)
            {
                LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// zone list as an asynchronous operation.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;Zones&gt;.</returns>
        public async Task<Zones> ZoneListAsync(int? nodeId)
        {
            var requestString = await this.BuildWebApiUrl(this.zoneRoute + "get").ConfigureAwait(false);
            requestString += "&nodeId=" + (nodeId ?? this.defaultNodeId);

            var retVal = await this.GetRequest<Zones>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// public ip interfaces from zones list as an asynchronous operation.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;IEnumerable&lt;IpInterface&gt;&gt;.</returns>
        public async Task<IEnumerable<IpInterface>> PublicIpInterfacesFromZonesListAsync(int? nodeId)
        {
            var allZones = await this.ZoneListAsync(nodeId).ConfigureAwait(false);

            // Get the public zones
            // TODO It would be nice to be able to filter on zoneType
            var publicZones = allZones.data.Where(
                    z => z.zoneType.Equals(AppConstants.AccessPublicKey, StringComparison.InvariantCultureIgnoreCase))
                .ToList();

            var interfaceList = new Dictionary<int, IpInterface>();

            foreach (var zone in publicZones)
            {
                foreach (var sipInterface in zone.sipInterfaces)
                {
                    if (!interfaceList.ContainsKey(sipInterface.ipInterfaceId))
                    {
                        interfaceList.Add(sipInterface.ipInterfaceId, sipInterface.ipInterface);
                    }
                }
            }

            return interfaceList.Values.ToList();
        }

        /// <summary>
        /// Sips the interface list.
        /// </summary>
        /// <typeparam name="T"> Entity Type </typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns Task&lt;SipInterfaces&gt;.</returns>
        public async Task<DataWrapperListBase<T>> InterfaceList<T>(InterfaceType interfaceType, int? nodeId)
        {
            // Build the request string
            string routePath;
            if (interfaceType.Equals(InterfaceType.Media) || interfaceType.Equals(InterfaceType.Sip))
            {
                routePath = string.Format(this.configurationInterfaceRoute, interfaceType.GetDescription().ToLower());
            }
            else
            {
                routePath = this.configurationIpInterfaceRoute;
            }

            var requestString = await this.BuildWebApiUrl(routePath + "get").ConfigureAwait(false);
            requestString += "&nodeId=" + (nodeId ?? this.defaultNodeId);

            var retVal = await this.GetRequest<DataWrapperListBase<T>>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// Gets the interface.
        /// </summary>
        /// <typeparam name="T"> Entity Type </typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns Task&lt;DataWrapperEntityBase&lt;MediaInterface&gt;&gt;.</returns>
        public async Task<DataWrapperEntityBase<T>> GetInterface<T>(InterfaceType interfaceType, int nodeId, int id)
        {
            // Build the request string
            var routePath = string.Format(this.configurationInterfaceRoute, interfaceType.GetDescription().ToLower());
            var parameter = nodeId + "/" + id;
            var requestString = await this.BuildWebApiUrl(routePath + parameter).ConfigureAwait(false);

            var retVal = await this.GetRequest<DataWrapperEntityBase<T>>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// Gets the ip interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;DataWrapperBase&lt;T&gt;&gt;.</returns>
        public async Task<DataWrapperBase<T>> GetIpInterface<T>(int nodeId, int id)
        {
            // Build the request string
            var routePath = this.configurationIpInterfaceRoute;
            var parameter = nodeId + "/" + id;
            var requestString = await this.BuildWebApiUrl(routePath + parameter).ConfigureAwait(false);

            var retVal = await this.GetRequest<DataWrapperBase<T>>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// Set Data and Batch # in Cataleya Acl History Model for saving to db purposes
        /// </summary>
        /// <param name="acl"></param>
        /// <param name="batchNo"></param>
        /// <returns></returns>
        private CataleyaAclHistoryViewModel SetCataleyaAclHistoryViewModel(Acl acl, int batchNo)
        {
            CataleyaAclHistoryViewModel cataleyaAclHistory = new CataleyaAclHistoryViewModel();
            cataleyaAclHistory.Action = acl.action;
            cataleyaAclHistory.AppType = acl.appType;
            cataleyaAclHistory.BatchNo = batchNo;
            cataleyaAclHistory.LocalPort = Convert.ToString(acl.localPort);
            cataleyaAclHistory.NodeId = acl.nodeId;
            cataleyaAclHistory.RemoteIpAddress = acl.remoteIpAddress;
            cataleyaAclHistory.RemotePort = Convert.ToString(acl.remotePort);
            cataleyaAclHistory.RemotePrefix = Convert.ToString(acl.remotePrefix);
            cataleyaAclHistory.SipInterfaceAddressType = acl.ipInterface.addressType;
            cataleyaAclHistory.SipInterfaceId = acl.ipinterfaceId;
            cataleyaAclHistory.SipInterfaceIpAddress = acl.ipInterface.ipaddress;
            cataleyaAclHistory.Transport = acl.transport;
            cataleyaAclHistory.Ver = acl.ver;
            return cataleyaAclHistory;
        }

        /// <summary>
        /// Delete ACL entries from Database 
        /// </summary>
        /// <param name="securityAcls"></param>
        /// <param name="batchNo"></param>
        /// <returns></returns>
        public CataleyaAclHistoryViewModel DeleteCataleyaAclHistory(SecurityAcl securityAcls, int batchNo)
        {
            CataleyaAclHistoryViewModel cataleyaAclHistoryViewModel = new CataleyaAclHistoryViewModel();
            foreach (var securityAcl in securityAcls.data)
            {
                cataleyaAclHistoryViewModel.Action = securityAcl.action;
                cataleyaAclHistoryViewModel.AppType = securityAcl.appType;
                cataleyaAclHistoryViewModel.BatchNo = batchNo;
                cataleyaAclHistoryViewModel.LocalPort = Convert.ToString(securityAcl.localPort);
                cataleyaAclHistoryViewModel.NodeId = securityAcl.nodeId;
                cataleyaAclHistoryViewModel.RemoteIpAddress = securityAcl.remoteIpAddress;
                cataleyaAclHistoryViewModel.RemotePort = Convert.ToString(securityAcl.remotePort);
                cataleyaAclHistoryViewModel.RemotePrefix = Convert.ToString(securityAcl.remotePrefix);
                cataleyaAclHistoryViewModel.SipInterfaceAddressType = securityAcl.ipInterface.addressType;
                cataleyaAclHistoryViewModel.SipInterfaceId = securityAcl.ipinterfaceId;
                cataleyaAclHistoryViewModel.SipInterfaceIpAddress = securityAcl.ipInterface.ipaddress;
                cataleyaAclHistoryViewModel.Transport = securityAcl.transport;
                cataleyaAclHistoryViewModel.Ver = securityAcl.ver;
                this.aclHistoryRepository.DeleteCataleyaAclHistory(cataleyaAclHistoryViewModel);
            }
            return cataleyaAclHistoryViewModel;
        }

        /// <summary>
        /// Delete Security ACL from Cataleya and create history 
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="securityAclId"></param>
        /// <param name="batchNo"></param>
        /// <returns></returns>
        public async Task DestroySecurityACL(int nodeId, int? securityAclId, int batchNo)
        {
            try
            {
                if (securityAclId == null)
                {
                    var result = await this.SecurityAclListAsync(nodeId);
                    foreach (var item in result.data)
                    {
                        var res = await this.DeleteSecurityAcl(new CompoundKey { id = item.id, nodeid = item.nodeId });
                        if (res.success)
                        {
                            await Task.Delay(1000);
                            var cataleyaAclHistory = this.SetCataleyaAclHistoryViewModel(item, batchNo);
                            this.aclHistoryRepository.DeleteCataleyaAclHistory(cataleyaAclHistory);
                        }
                        else
                        {
                            LogError(res.message);
                            var resp = await this.DeleteSecurityAcl(new CompoundKey { id = item.id, nodeid = item.nodeId });
                            if (resp.success)
                            {
                                var cataleyaAclHistory = this.SetCataleyaAclHistoryViewModel(item, batchNo);
                                this.aclHistoryRepository.DeleteCataleyaAclHistory(cataleyaAclHistory);
                            }
                        }
                    }
                }
                else
                {
                    var result = await this.SecurityAclByIdAsync((int)securityAclId, nodeId);
                    if (result.total > 0)
                    {
                        var res = await this.DeleteSecurityAcl(new CompoundKey { id = (int)securityAclId, nodeid = nodeId });
                        if (res.success)
                        {
                            await Task.Delay(1000);
                            this.DeleteCataleyaAclHistory(result, batchNo);
                        }
                        else
                        {
                            LogError(res.message);
                            var resp = await this.DeleteSecurityAcl(new CompoundKey { id = (int)securityAclId, nodeid = nodeId });
                            if (res.success)
                            {
                                await Task.Delay(1000);
                                this.DeleteCataleyaAclHistory(result, batchNo);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message);
                await this.DestroySecurityACL(nodeId, securityAclId, batchNo);
            }
        }

        /// <summary>
        /// Delete The Security ACL Ips from Cataleya if they are in SourceList
        /// </summary>
        /// <param name="initalSourceList"></param>
        /// <param name="finalSourceList"></param>
        /// <returns></returns>
        public async Task DestroyNewSourceListFromSecurityACL(IList<CataleyaSourceListViewModel> initalSourceList, IList<CataleyaSourceListViewModel> finalSourceList)
        {
            int batchNo = this.aclHistoryRepository.GetLastBatchNo();
            batchNo++;
            var differenceSourceList = new List<CataleyaSourceListViewModel>();

            foreach (var item in finalSourceList)
            {
                var matchId = initalSourceList.Any(x => x.id == item.id);
                if (!matchId)
                {
                    differenceSourceList.Add(item);
                }
            }

            foreach (var item in differenceSourceList)
            {
                if (this.aclHistoryRepository.CheckIfTheIpAddressIsBlacklisted(item.ipAddress))
                {
                    var securityAcl = this.aclHistoryRepository.GetCataleyaAclHistoryByNode(item.nodeId, item.zoneId, item.ipAddress);
                    if (securityAcl != null && securityAcl.CataleyaSecurityAclId > 0)
                    {
                        await this.DestroySecurityACL(securityAcl.NodeId, securityAcl.CataleyaSecurityAclId, batchNo);
                    }
                }
            }
        }

        /// <summary>
        /// Get The Nodes List from Cataleya
        /// </summary>
        /// <returns></returns>
        public async Task<List<CataleyaNodeViewModel>> CataleyaNodeListAsync()
        {
            // Build the request string
            var requestString = await BuildWebApiUrl(this.getAllNodesRoute).ConfigureAwait(false);
            if (!string.IsNullOrEmpty(requestString))
            {
                var response = await GetAsync(requestString).Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<CataleyaNodeViewModel>>(response);
            }
            return new List<CataleyaNodeViewModel>();
        }

        /// <summary>
        /// Get the Zones List From Cataleya
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public async Task<CataleyaZoneViewModel> CataleyaZoneListAsync(int nodeId, int operatorId)
        {
            try
            {
                // Build the request string
                var requestString = await BuildWebApiUrl(this.getAllZonesRoute).ConfigureAwait(false);
                requestString += "&zoneType=AccessPublic" + "&nodeId=" + nodeId + "&operatorId=" + operatorId;
                if (!string.IsNullOrEmpty(requestString))
                {
                    var response = await GetAsync(requestString).Result.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<CataleyaZoneViewModel>(response);
                }
                return new CataleyaZoneViewModel();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new CataleyaZoneViewModel();
            }
        }

        /// <summary>
        /// Send email for token confirmation after the account new is created for Cataleya
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public Task SendEmailForTokenConfirmation(string destination, string subject, string body)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                //client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("noreplythelinkconsultancy@gmail.com", "tlcrezaid");

                return client.SendMailAsync("noreplythelinkconsultancy@gmail.com", destination, subject, body);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Set Cataleya UserName and Password in Model for User Partition Mapping 
        /// </summary>
        /// <param name="cataleyaId"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        public void SetCataleyaUserNamePassword(int cataleyaId, string username, string password, string email)
        {
            this.cataleyaUsername = username;
            this.cataleyaPassword = password;
            this.cataleyaUserEmail = email;
            userPartitionMappingViewModel = new UserPartitionMappingViewModel { CataleyaId = cataleyaId, LastAccountVerificationDate = DateTime.Now };
        }

        /// <summary>
        /// Send Email to User or SuperAdmin and Set status to true or false if Cataleya User is Validated
        /// </summary>
        /// <param name="status"></param>
        /// <param name="destination"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        private void UpdateStatusAndSendEmail(bool status, string destination, string subject, string body)
        {
            try
            {
                userPartitionMappingViewModel.Status = status;
                this.calaleyaInfoRepository.Edit(userPartitionMappingViewModel);
                SendEmailForTokenConfirmation(destination, subject, body);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        /// <summary>
        /// Get Access token for the current logged in user from Cataleya
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetAccessTokenForCurrentUser()
        {
            Log.Info("Entered CataleyaService.GetAccessToken");
            var accessToken = string.Empty;

            try
            {
                var webApiUrl = this.cataleyaApiUrl +
                                "oauth/token?grant_type=password&client_id=restapp&client_secret=restapp&username=" +
                                this.cataleyaUsername + "&password=" +
                                this.cataleyaPassword;

                var response = await this.PostAsync(webApiUrl, null).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    // Get the response
                    var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)
                    var result = JsonConvert.DeserializeObject<Authentication>(jsonString);

                    if (result != null)
                    {
                        accessToken = result.access_token;
                        if (String.IsNullOrEmpty(accessToken))
                        {
                            UpdateStatusAndSendEmail(false, this.loggedInUserName, "Error", "Cataleya account validation failed for user " + this.cataleyaUsername + ". Please check!");
                        }
                        else
                        {
                            UpdateStatusAndSendEmail(true, this.cataleyaUserEmail, "Partition Successfully Assigned", "SBC partition has been assigned to you successfully.");
                        }
                    }
                    else
                    {
                        UpdateStatusAndSendEmail(false, this.loggedInUserName, "Error", "Cataleya account validation failed for user " + this.cataleyaUsername + ". Please check!");
                    }
                }
                else
                {
                    Log.Error("CataleyaService.GetAccessToken [" + response.StatusCode + "] [" + response.ReasonPhrase + "]");
                    UpdateStatusAndSendEmail(false, this.loggedInUserName, "Error", "Cataleya account validation failed for user " + this.cataleyaUsername + ". Please check!");
                }

            }
            catch (Exception e)
            {
                Log.Error("Error CataleyaService.GetAccessToken [" + e.Message + "]");
                Log.Error(e.ToString());
                Log.Error(e.StackTrace);
                UpdateStatusAndSendEmail(false, this.loggedInUserName, "Technical Error", e.Message);
            }

            Log.Info("Exited CataleyaService.GetAccessToken [" + accessToken + "]");
            return accessToken;
        }

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <returns>Returns Task&lt;System.String&gt;.</returns>
        public async Task<string> GetAccessToken()
        {
            Log.Info("Entered CataleyaService.GetAccessToken");
            var accessToken = string.Empty;

            try
            {
                var webApiUrl = this.cataleyaApiUrl +
                                "oauth/token?grant_type=password&client_id=restapp&client_secret=restapp&username=" +
                                this.cataleyaUsername + "&password=" +
                                this.cataleyaPassword;

                var response = await this.PostAsync(webApiUrl, null).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    // Get the response
                    var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)
                    var result = JsonConvert.DeserializeObject<Authentication>(jsonString);

                    if (result != null)
                    {
                        accessToken = result.access_token;
                    }
                }
                else
                {
                    Log.Error("CataleyaService.GetAccessToken [" + response.StatusCode + "] [" + response.ReasonPhrase + "]");
                }

            }
            catch (Exception e)
            {
                Log.Error("Error CataleyaService.GetAccessToken [" + e.Message + "]");
                Log.Error(e.ToString());
                Log.Error(e.StackTrace);
            }

            Log.Info("Exited CataleyaService.GetAccessToken [" + accessToken + "]");
            return accessToken;
        }

        /// <summary>
        /// Validates the access token.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <returns>Returns Task&lt;HttpStatusCode&gt;.</returns>
        public async Task<HttpStatusCode> ValidateAccessToken(string accessToken)
        {
            Log.Info("CataleyaService.ValidateAccessToken: accessToken [" + accessToken + "]");
            HttpStatusCode retVal;
            if (!string.IsNullOrEmpty(accessToken))
            {
                var webApiUrl = this.cataleyaApiUrl + this.softwareAboutRoute + "?access_token=" + accessToken;

                var response = await this.GetAsyncWithAccessHeader(webApiUrl).ConfigureAwait(false);

                // Check the result
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    Log.Info("CataleyaService.ValidateAccessToken: jsonString [" + jsonString + "]");
                    dynamic result = JsonConvert.DeserializeObject(jsonString);

                    retVal = result.success == true ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
                }
                else
                {
                    Log.Error("CataleyaService.ValidateAccessToken: Failted Status Code [" + response.StatusCode + "] [" + response.ReasonPhrase + "]");
                    retVal = response.StatusCode;
                }
            }
            else
            {
                retVal = HttpStatusCode.BadRequest;
            }

            Log.Info("CataleyaService.ValidateAccessToken: retVal [" + retVal + "]");
            return retVal;
        }

        /// <summary>
        /// Builds the web API URL.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns>Return Task&lt;System.String&gt;.</returns>
        public async Task<string> BuildWebApiUrl(string route)
        {
            try
            {
                var webApiUrl = string.Empty;

                // Get the access token
                var accessToken = await this.GetAccessToken().ConfigureAwait(false);

                if (!string.IsNullOrEmpty(accessToken))
                {
                    webApiUrl = this.cataleyaApiUrl + route + "?access_token=" + accessToken;
                }

                return webApiUrl;
            }
            catch (Exception e)
            {
                LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets the target interface.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IpInterface&gt;.</returns>
        public async Task<IpInterface> GetTargetInterface(InterfaceType interfaceType, int nodeId, int id)
        {
            DataEntityInterfaceBase parentInterface;
            IpInterface retVal = null;
            if (interfaceType.Equals(InterfaceType.Sip))
            {
                var sipWrapper = await this.GetInterface<SipInterface>(interfaceType, nodeId, id)
                                          .ConfigureAwait(false);
                parentInterface = sipWrapper.data;
                retVal = parentInterface.ipInterface;
            }
            else if (interfaceType.Equals(InterfaceType.Media))
            {
                var mediaWrapper = await this.GetInterface<MediaInterface>(interfaceType, nodeId, id)
                                                          .ConfigureAwait(false);
                parentInterface = mediaWrapper.data;
                retVal = parentInterface.ipInterface;
            }
            else if (interfaceType.Equals(InterfaceType.Ip))
            {
                var mediaWrapper = await this.GetIpInterface<IpInterface>(nodeId, id)
                                                          .ConfigureAwait(false);
                retVal = mediaWrapper.data;
            }
            else
            {
                throw new InvalidOperationException();
            }

            return retVal;
        }

        /// <summary>
        /// Builds the acl.
        /// </summary>
        /// <param name="aclEntityModel">The acl entity model.</param>
        /// <returns>Task&lt;Acl&gt;.</returns>
        private CreateAcl BuildAcl(AclEntityModel aclEntityModel)
        {
            return new CreateAcl
            {
                ipInterface = aclEntityModel.IpInterface,
                nodeId = this.defaultNodeId,
                action = aclEntityModel.AclType.Equals(AclListType.BlackList) ? "Drop" : "Accept",
                appType = aclEntityModel.InterfaceType.ToString(),
                remoteIpAddress = aclEntityModel.IpAddress,
                localPort = 0,
                remotePort = 0,
                remotePrefix = aclEntityModel.Prefix,
                transport = "UDP",
                ver = 1,
                ipinterfaceId = aclEntityModel.IpInterface.id,
                compoundKey = new CompoundKey { nodeid = this.defaultNodeId, id = 0 }
            };
        }

        /// <summary>
        /// get as an asynchronous operation.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> GetAsync(string requestString)
        {
            using (var client = this.GetHttpClient())
            {
                var result = await client.GetAsync(requestString).ConfigureAwait(false);

                if (result != null)
                {
                    Log.Info("CataleyaService.GetAsync: Request [" + requestString + "] Response [" + result.StatusCode + "]");
                }
                else
                {
                    Log.Error("CataleyaService.GetAsync: Request[" + requestString + "] returned null result");
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the request.
        /// </summary>
        /// <typeparam name="T"> Data type </typeparam>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;T&gt;.</returns>
        private async Task<T> GetRequest<T>(string requestString)
            where T : class, new()
        {
            var response = await this.GetAsyncWithAccessHeader(requestString).ConfigureAwait(false);
            T retVal = null;

            if (response.IsSuccessStatusCode)
            {
                retVal = await response.Content.ReadAsAsync<T>().ConfigureAwait(false);
            }

            return retVal;
        }

        /// <summary>
        /// Gets the asynchronous with access header.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> GetAsyncWithAccessHeader(string requestString)
        {
            HttpResponseMessage result;
            Log.Info("Entered CataleyaService.GetAsyncWithAccessHeader: Request [" + requestString + "]");
            using (var client = this.GetHttpClient())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestString);
                requestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                result = await client.SendAsync(requestMessage).ConfigureAwait(false);

                if (result != null)
                {
                    Log.Info("CataleyaService.GetAsyncWithAccessHeader: Request [" + requestString + "] Response [" + result.StatusCode + "]");
                }
                else
                {
                    Log.Error("CataleyaService.GetAsyncWithAccessHeader: Request[" + requestString + "] returned null result");
                }
            }

            Log.Info("Exited  CataleyaService.GetAsyncWithAccessHeader: Request [" + requestString + "]");

            return result;
        }

        /// <summary>
        /// post as an asynchronous operation.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <param name="content">The content.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> PostAsync(string requestString, HttpContent content)
        {
            using (var client = this.GetHttpClient())
            {
                var result = await client.PostAsync(requestString, content).ConfigureAwait(false);

                return result;
            }
        }

        /// <summary>
        /// delete as an asynchronous operation.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> DeleteAsync(string requestString)
        {
            using (var client = this.GetHttpClient())
            {
                var result = await client.DeleteAsync(requestString).ConfigureAwait(false);

                return result;
            }
        }

        /// <summary>
        /// Gets the HTTP client.
        /// </summary>
        /// <returns>Returns HttpClient.</returns>
        private HttpClient GetHttpClient()
        {
            HttpClientHandler handler = new HttpClientHandler()
            {
                MaxConnectionsPerServer = 20,
                Proxy = null,
                UseDefaultCredentials = true
            };

            // Set the default connection limit
            ServicePointManager.DefaultConnectionLimit = 20;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            var client = new HttpClient(handler, true);
            client.Timeout = TimeSpan.FromMinutes(2);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}