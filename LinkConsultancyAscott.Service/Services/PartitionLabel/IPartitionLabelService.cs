﻿
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IUserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.PartitionLabel
{
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using System.Collections.Generic;

    /// <summary>
    /// The User Service Interface.
    /// </summary>
    public interface IPartitionLabelService
    {
        /// <summary>
        /// The create user.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        PartitionLabelViewModel CreatePartitionLabel(PartitionLabelViewModel viewModel);

        IList<PartitionLabelViewModel> ReadPartitionLabelList();//PartitionLabelViewModel partitionLabel, string user, bool showAllRows);

        PartitionLabelViewModel UpdatePartitionLabel(PartitionLabelViewModel viewModel);
        void DeletePartitionLabel(PartitionLabelViewModel viewModel);
    }
}

