﻿

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the User Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.PartitionLabel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using LinkConsultancyAscott.Service.Repository.PartitionLabel;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.ViewModels.Acl;

    /// <summary>
    /// The account membership service.
    /// </summary>
    public class UserPartitionMappingService : ServiceBase, IUserPartitionMappingService
    {
        /// <summary>
        /// The _provider.
        /// </summary> ICalaleyaInfoRepository
        private readonly IUserPartitionMappingRepository userPartitionMappingRepository;

        private readonly ICataleyaService cataleyaService;

        private readonly ICurrentUserService currentUserService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="appConfigRepository">The application configuration repository.</param>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="cacheService">The cache service.</param>
        public UserPartitionMappingService(
            IUserPartitionMappingRepository userPartitionMappingRepository,
            ICurrentUserService currentUserService,
            ICataleyaService cataleyaService
           )
        {
            this.cataleyaService = cataleyaService;
            this.currentUserService = currentUserService;
            this.userPartitionMappingRepository = userPartitionMappingRepository;
        }

        /// <summary>
        /// Reads the acl list.
        /// </summary>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <param name="user">The user.</param>
        /// <param name="showAllRows">if set to <c>true</c> [show all rows].</param>
        /// <returns>IQueryable&lt;AclListViewModel&gt;.</returns>
        public IList<UserPartitionMappingViewModel> ReadUserPartitionMappingList()//PartitionLabelViewModel partitionLabel, string user, bool showAllRows)
        {
            IList<UserPartitionMappingViewModel> obj = this.userPartitionMappingRepository.Get();// partitionLabel);
            return new List<UserPartitionMappingViewModel>(obj);
        }


        public Task<UserPartitionMappingViewModel> UpdateUserPartitionMapping(UserPartitionMappingViewModel viewModel)//, string user, bool showAllRows)
        {
            return this.userPartitionMappingRepository.Edit(viewModel);
        }
        public async Task<UserPartitionMappingViewModel> DeleteUserPartitionMapping(UserPartitionMappingViewModel viewModel)//, string user, bool showAllRows)
        {
            var loggedInUser = this.currentUserService.UserName();
            var result = this.userPartitionMappingRepository.Delete(viewModel);
            if (result != null && result.CataleyaId > 0) {
                await this.cataleyaService.SendEmailForTokenConfirmation(loggedInUser, "User Deleted", "User with email: " + viewModel.TLCEmail + " and username: " + viewModel.CataleyaUserName + " was deleted successfully!");
                return result;
            }
            return result;
        }
    }
}

