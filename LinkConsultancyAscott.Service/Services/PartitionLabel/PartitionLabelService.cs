﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the User Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.PartitionLabel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LinkConsultancyAscott.Service.Repository.PartitionLabel;
    using LinkConsultancyAscott.Service.ViewModels.Acl;

    /// <summary>
    /// The account membership service.
    /// </summary>
    public class PartitionLabelService : ServiceBase, IPartitionLabelService
    {
        /// <summary>
        /// The _provider.
        /// </summary> ICalaleyaInfoRepository
        private readonly IPartitionLabelRepository partitionlabelRepository;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="appConfigRepository">The application configuration repository.</param>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="cacheService">The cache service.</param>
        public PartitionLabelService(IPartitionLabelRepository partitionLabelRepository)
        {

            this.partitionlabelRepository = partitionLabelRepository;

        }

        /// <summary>
        /// The create user.
        /// </summary>
        /// <param name="viewModel">
        /// The user view model.
        /// </param>
        /// <returns>
        /// The <see cref="PartitionLabelViewModel"/>.
        /// </returns>
        public PartitionLabelViewModel CreatePartitionLabel(PartitionLabelViewModel viewModel)
        {
            PartitionLabelViewModel obj = new PartitionLabelViewModel();
            obj.PartitionId = Convert.ToInt32(viewModel.PartitionId);
            obj.PartitionLabels = viewModel.PartitionLabels;
            obj.PartitionLabelId = viewModel.PartitionLabelId;
            obj.IstraIp = viewModel.IstraIp;
            obj.DirectoryPath = viewModel.DirectoryPath;
            int? id = this.partitionlabelRepository.Adds(obj);//.ConfigureAwait(false);
            viewModel.PartitionId = id;
            return viewModel;
        }

        /// <summary>
        /// Reads the acl list.
        /// </summary>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <param name="user">The user.</param>
        /// <param name="showAllRows">if set to <c>true</c> [show all rows].</param>
        /// <returns>IQueryable&lt;AclListViewModel&gt;.</returns>
        public IList<PartitionLabelViewModel> ReadPartitionLabelList()//PartitionLabelViewModel partitionLabel, string user, bool showAllRows)
        {
            IList<PartitionLabelViewModel> obj = this.partitionlabelRepository.Get();// partitionLabel);
            return new List<PartitionLabelViewModel>(obj);
        }

        public PartitionLabelViewModel UpdatePartitionLabel(PartitionLabelViewModel viewModel)//, string user, bool showAllRows)
        {
            return this.partitionlabelRepository.Edit(viewModel);
        }

        public void DeletePartitionLabel(PartitionLabelViewModel viewModel)//, string user, bool showAllRows)
        {
            this.partitionlabelRepository.Delete(viewModel);
        }
    }
}
