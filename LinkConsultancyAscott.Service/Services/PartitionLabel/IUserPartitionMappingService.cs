﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IUserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.PartitionLabel
{
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The User Service Interface.
    /// </summary>
    public interface IUserPartitionMappingService
    {
       
        IList<UserPartitionMappingViewModel> ReadUserPartitionMappingList();
        Task<UserPartitionMappingViewModel> UpdateUserPartitionMapping(UserPartitionMappingViewModel viewModel);
        Task<UserPartitionMappingViewModel> DeleteUserPartitionMapping(UserPartitionMappingViewModel viewModel);
    }
}

