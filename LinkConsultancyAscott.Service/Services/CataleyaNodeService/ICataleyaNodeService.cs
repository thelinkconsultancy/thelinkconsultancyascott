﻿using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Services.CataleyaNodeService
{
    public interface ICataleyaNodeService
    {
        CataleyaNodeViewModel CreateCataleyaNode(CataleyaNodeViewModel viewModel);

        IList<CataleyaNodeViewModel> ReadCataleyaNodeList();

        bool CheckIfCataleyaNodeAlreadyExists(int Id);

        void DeleteAllCataleyaNodes();
    }
}
