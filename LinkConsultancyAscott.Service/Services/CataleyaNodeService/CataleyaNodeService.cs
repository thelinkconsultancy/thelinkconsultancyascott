﻿using LinkConsultancyAscott.Service.Repository.CataleyaNodeRepository;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Services.CataleyaNodeService
{
    public class CataleyaNodeService : ServiceBase, ICataleyaNodeService
    {
        private readonly ICataleyaNodeRepository cataleyaNodeRepository;

        public CataleyaNodeService(ICataleyaNodeRepository cataleyaNodeRepository)
        {
            this.cataleyaNodeRepository = cataleyaNodeRepository;
        }

        public IList<CataleyaNodeViewModel> ReadCataleyaNodeList()
        {
            IList<CataleyaNodeViewModel> obj = this.cataleyaNodeRepository.Get();
            return new List<CataleyaNodeViewModel>(obj);
        }

        public CataleyaNodeViewModel CreateCataleyaNode(CataleyaNodeViewModel viewModel)
        {
            CataleyaNodeViewModel obj = new CataleyaNodeViewModel();
            obj.id = Convert.ToInt32(viewModel.id);
            obj.name = viewModel.name;
            obj.centralMgmtIp = viewModel.centralMgmtIp;
            obj.description = viewModel.description;
            obj.location = viewModel.location;
            viewModel.id = this.cataleyaNodeRepository.Add(obj);
            return viewModel;
        }

        public bool CheckIfCataleyaNodeAlreadyExists(int Id)
        {
            return this.cataleyaNodeRepository.CheckIfCataleyaNodeAlreadyExists(Id);
        }

        public void DeleteAllCataleyaNodes()
        {
            this.cataleyaNodeRepository.DeleteAllCataleyaNodes();
        }
    }
}
