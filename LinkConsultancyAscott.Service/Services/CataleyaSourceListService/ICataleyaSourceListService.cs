﻿using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Services.CataleyaSourceListService
{
    public interface ICataleyaSourceListService
    {
        CataleyaSourceListViewModel CreateCataleyaSourceList(CataleyaSourceListViewModel viewModel);
        bool CheckIfCataleyaSourceListAlreadyExists(int id, int zoneId, int nodeId);
        IList<CataleyaSourceListViewModel> ReadCataleyaSourceList();
        void DeleteAllCataleyaSourceList();
    }
}
