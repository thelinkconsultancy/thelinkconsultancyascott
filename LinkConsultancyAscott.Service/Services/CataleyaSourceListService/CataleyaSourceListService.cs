﻿using LinkConsultancyAscott.Service.Repository.CataleyaSourceListRepository;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Services.CataleyaSourceListService
{
    public class CataleyaSourceListService : ServiceBase, ICataleyaSourceListService
    {
        private readonly ICataleyaSourceListRepository cataleyaSourceListRepository;

        public CataleyaSourceListService(ICataleyaSourceListRepository cataleyaSourceListRepository)
        {
            this.cataleyaSourceListRepository = cataleyaSourceListRepository;
        }

        public IList<CataleyaSourceListViewModel> ReadCataleyaSourceList()
        {
            IList<CataleyaSourceListViewModel> obj = this.cataleyaSourceListRepository.Get();
            return new List<CataleyaSourceListViewModel>(obj);
        }

        public CataleyaSourceListViewModel CreateCataleyaSourceList(CataleyaSourceListViewModel viewModel)
        {
            CataleyaSourceListViewModel obj = new CataleyaSourceListViewModel();
            obj.id = Convert.ToInt32(viewModel.id);
            obj.addressType = viewModel.addressType;
            obj.ipAddress = viewModel.ipAddress;
            obj.subnetMask = viewModel.subnetMask;
            obj.zoneId = viewModel.zoneId;
            obj.nodeId = viewModel.nodeId;
            obj.operatorId = viewModel.operatorId;
            viewModel.id = this.cataleyaSourceListRepository.Add(obj);
            return viewModel;
        }

        public bool CheckIfCataleyaSourceListAlreadyExists(int id, int zoneId, int nodeId)
        {
            return this.cataleyaSourceListRepository.CheckIfCataleyaSourceListAlreadyExists(id, zoneId, nodeId);
        }

        public void DeleteAllCataleyaSourceList() {
            this.cataleyaSourceListRepository.DeleteAllCataleyaSourceList();
        }
    }
}
