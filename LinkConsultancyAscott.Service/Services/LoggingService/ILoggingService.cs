﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILoggingService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Logging Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.LoggingService
{
    using System.Linq;
    
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    /// <summary>
    /// Interface ILoggingService
    /// </summary>
    public interface ILoggingService
    {
        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <returns>
        /// The list of view models.
        /// </returns>
        IQueryable<LogViewModel> Read();

        /// <summary>
        /// Uniques the specified field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns>IQueryable&lt;LogViewModel&gt;.</returns>
        IQueryable<LogViewModel> Unique(string field);
    }
}
