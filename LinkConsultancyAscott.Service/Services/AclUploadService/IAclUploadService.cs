﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclUploadService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Static Data Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.AclUploadService
{
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Models.Cataleya.Response;

    /// <summary>
    /// Interface IAclUploadService
    /// </summary>
    public interface IAclUploadService
    {
        /// <summary>
        /// Uploads the acl file.
        /// </summary>
        /// <param name="aclFileUploadRequest">The acl file upload request.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<ProcessIpAddressResponse> UploadAclFileAsync(AclFileUploadRequest aclFileUploadRequest);

        /// <summary>
        /// Creates the specified acl create request.
        /// </summary>
        /// <param name="aclCreateRequest">The acl create request.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<ProcessIpAddressResponse> CreateAsync(AclCreateRequest aclCreateRequest);

        /// <summary>
        /// Processes the ip address.
        /// </summary>
        /// <param name="entityModel">The entity model.</param>
        /// <returns>Task&lt;ProcessIpAddressResponse&gt;.</returns>
        Task<ProcessIpAddressResponse> ProcessIpAddressAsync(AclEntityModel entityModel);
    }
}
