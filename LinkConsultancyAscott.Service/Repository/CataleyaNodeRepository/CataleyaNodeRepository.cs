﻿using LinkConsultancyAscott.Data;
using LinkConsultancyAscott.Service.Infrastructure;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;
using System.Linq;

namespace LinkConsultancyAscott.Service.Repository.CataleyaNodeRepository
{
    public class CataleyaNodeRepository : RepositoryBase, ICataleyaNodeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ICataleyaNodeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public CataleyaNodeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IList<CataleyaNodeViewModel> Get()
        {
            List<CataleyaNodeViewModel> obj = new List<CataleyaNodeViewModel>();
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                foreach (var item in db.CataleyaNodes.ToList())
                {
                    CataleyaNodeViewModel node = new CataleyaNodeViewModel();
                    node.id = item.Id;
                    node.name = item.Name;
                    node.centralMgmtIp = item.CentralMgmtIP;
                    node.description = item.Description;
                    node.location = item.Location;
                    obj.Add(node);
                }
            }
            return new List<CataleyaNodeViewModel>(obj);
        }

        public int Add(CataleyaNodeViewModel node)
        {
            int id = 0;
            CataleyaNode obj = new CataleyaNode();
            obj.Id = node.id;
            obj.Name = node.name;
            obj.Location = node.location;
            obj.CentralMgmtIP = node.centralMgmtIp;
            obj.Description = node.description;
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                context.CataleyaNodes.Add(obj);
                context.SaveChanges();
                id = obj.Id;
            }
            return id;
        }

        public bool CheckIfCataleyaNodeAlreadyExists(int id)
        {
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var response = context.CataleyaNodes.Find(id);
                if (response != null && response.Id > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public void DeleteAllCataleyaNodes()
        {
            try
            {
                using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
                {
                    var response = context.CataleyaNodes.ToList();
                    foreach (var item in response)
                    {
                        context.CataleyaNodes.Remove(item);
                        context.SaveChanges();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
