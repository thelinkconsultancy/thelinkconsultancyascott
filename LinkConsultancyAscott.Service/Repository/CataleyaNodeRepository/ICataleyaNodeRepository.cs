﻿using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Repository.CataleyaNodeRepository
{
    public interface ICataleyaNodeRepository : IRepositoryBase
    {
        IList<CataleyaNodeViewModel> Get();
        int Add(CataleyaNodeViewModel node);
        bool CheckIfCataleyaNodeAlreadyExists(int id);
        void DeleteAllCataleyaNodes();
    }
}
