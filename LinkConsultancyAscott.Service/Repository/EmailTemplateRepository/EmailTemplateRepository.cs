﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTemplateRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The EmailTemplate repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.EmailTemplateRepository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Data;
    using Infrastructure;
    
    /// <summary>
    /// The EmailTemplate Repository.
    /// </summary>
    public class EmailTemplateRepository : LookUpRepository<int, EmailTemplate>, IEmailTemplateRepository
    {
        /// <summary>
        /// The email wrapper top
        /// </summary>
        private const string EmailWrapperTop = "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head><body><!--[if mso]><style type='text/css'>body, p, table, td {font-family: Arial, Helvetica, sans-serif !important;}</style><![endif]-->";

        /// <summary>
        /// The email wrapper tail
        /// </summary>
        private const string EmailWrapperTail = "</body><html>";

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTemplateRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public EmailTemplateRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>The list.</returns>
        public override IQueryable<EmailTemplate> GetAll()
        {
            try
            {
                var resultList = this.Dbset.Include("EmailType").ToList();
                IQueryable<EmailTemplate> retVal = resultList.AsQueryable();
                return retVal;
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                throw;
            }
        }

        /// <summary>
        /// Gets the email template.
        /// </summary>
        /// <param name="emailType">Type of the requirement.</param>
        /// <returns>The email template.</returns>
        public string GetEmailTemplate(Models.EmailType emailType)
        {
            var retVal = string.Empty;

            var template = this.Dbset.FirstOrDefault(t => t.EmailTypeId == (int)emailType);

            if (template != null)
            {
                retVal = template.EmailText;
            }

            retVal = EmailWrapperTop + retVal + EmailWrapperTail;
            return retVal;
        }

        /// <summary>
        /// Gets the email subject.
        /// </summary>
        /// <param name="emailType">Type of the requirement.</param>
        /// <returns>Returns System.String.</returns>
        public string GetEmailSubject(Models.EmailType emailType)
        {
            var retVal = string.Empty;

            var template = this.Dbset.FirstOrDefault(t => t.EmailTypeId == (int)emailType);

            if (template != null)
            {
                retVal = template.EmailSubject;
            }

            return retVal;
        }
    }
}