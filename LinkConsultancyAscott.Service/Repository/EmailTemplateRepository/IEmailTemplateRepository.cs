﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailTemplateRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The EmailTemplate Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.EmailTemplateRepository
{
    using System;
    using System.Collections.Generic;

    using Data;

    /// <summary>
    /// The EmailTemplate Repository interface.
    /// </summary>
    public interface IEmailTemplateRepository : ILookUpRepository<int, EmailTemplate>
    {
        /// <summary>
        /// Gets the email template.
        /// </summary>
        /// <param name="emailType">Type of the requirement.</param>
        /// <returns>The email template.</returns>
        string GetEmailTemplate(Models.EmailType emailType);

        /// <summary>
        /// Gets the email subject.
        /// </summary>
        /// <param name="emailType">Type of the requirement.</param>
        /// <returns>System.String.</returns>
        string GetEmailSubject(Models.EmailType emailType);
    }
}
