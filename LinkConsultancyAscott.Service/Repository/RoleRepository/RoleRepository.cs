﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Role repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.RoleRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// The User Repository.
    /// </summary>
    public class RoleRepository : RepositoryBase, IRoleRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public RoleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IEnumerable<AspNetRole> GetAll()
        {
            return this.Database.AspNetRoles.ToList();
        }

        public IQueryable<AspNetRole> GetAllQueryable()
        {
            return this.Database.AspNetRoles;
        }

        /// <summary>
        /// Gets the specified user identifier.
        /// </summary>
        /// <param name="roleId">The user identifier.</param>
        /// <returns>aspnet_Roles.</returns>
        public AspNetRole Get(string roleId)
        {
            return this.Database.AspNetRoles.FirstOrDefault(u => u.Id.Equals(roleId, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Gets the name of the role identifier from.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns>Guid.</returns>
        public string GetRoleIdFromRoleName(string roleName)
        {
            var role = this.Database.AspNetRoles.FirstOrDefault(u => u.Name.Equals(roleName, StringComparison.InvariantCultureIgnoreCase));

            if (role != null)
            {
                return role.Id;
            }

            return string.Empty;
        }

        /// <summary>
        /// Adds the specified p.
        /// </summary>
        /// <param name="p">The p.</param>
        public void Add(AspNetRole p)
        {
            this.Database.AspNetRoles.Add(p);
        }

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        public void Edit(AspNetRole p)
        {
        }

        public void Delete(AspNetRole p)
        {
            this.Database.AspNetRoles.Remove(p);
        }
    }
}
