﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILookUpRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The LookUpRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;

    using Data;

    /// <summary>
    /// Interface ILookUpRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TM">The type of the tm.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.IAuditedEntityRepositoryBase{T, TM}" />
    public interface ILookUpRepository<T, TM> : IBasicLookUpRepository<T, TM> where TM : class, ILookUp<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The s name.
        /// </param>
        /// <returns>
        /// The <see cref="TM"/>.
        /// </returns>
        TM GetByName(string name);
    }
}
