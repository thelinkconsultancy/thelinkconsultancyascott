﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The EmailType repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.EmailTypeRepository
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// The EmailType Repository.
    /// </summary>
    public class EmailTypeRepository : LookUpRepository<int, EmailType>, IEmailTypeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTypeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public EmailTypeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}