﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LookUpRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The look up repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Data;

    using Infrastructure;

    /// <summary>
    /// Class LookUpRepository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TM">The type of the tm.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.AuditedEntityRepositoryBase{T, TM}" />
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.ILookUpRepository{T, TM}" />
    public abstract class BasicLookUpRepository<T, TM> : AuditedEntityRepositoryBase<T, TM>, IBasicLookUpRepository<T, TM>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where TM : class, IBasicLookUp<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LookUpRepository{T, TM}"/> class.
        /// </summary>
        /// <param name="databaseFactory">The database factory.</param>
        protected BasicLookUpRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// The get all visible.
        /// </summary>
        /// <returns>
        /// The Model List.
        /// </returns>
        public virtual IQueryable<TM> GetAllVisible()
        {
            var filtered = from i in this.GetAll()
                           where i.Visible && !i.DeletedAt.HasValue
                           orderby i.DisplayOrder
                           select i;

            return filtered;
        }

        /// <summary>
        /// Gets all visible list.
        /// </summary>
        /// <returns>IEnumerable&lt;TM&gt;.</returns>
        public IEnumerable<TM> GetAllVisibleList()
        {
            var filtered = (from i in this.GetAll()
                           where i.Visible && !i.DeletedAt.HasValue
                           orderby i.DisplayOrder
                           select i).ToList();

            return filtered;
        }
    }
}