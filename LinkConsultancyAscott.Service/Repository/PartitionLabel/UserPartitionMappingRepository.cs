﻿namespace LinkConsultancyAscott.Service.Repository.PartitionLabel
{

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserPartitionMappingRepository : RepositoryBase, IUserPartitionMappingRepository
    {
        IPartitionLabelRepository objj;
        ICataleyaService cataleyaService;
        List<TLCRoleViewModel> objTLCRole = new List<TLCRoleViewModel>();
        public UserPartitionMappingRepository(IDatabaseFactory databaseFactory, IPartitionLabelRepository objj, ICataleyaService cataleyaService)
           : base(databaseFactory)
        {
            this.objj = objj;
            this.cataleyaService = cataleyaService;
            objTLCRole.Add(new TLCRoleViewModel { Id = 1, Name = "Customer" });
            objTLCRole.Add(new TLCRoleViewModel { Id = 2, Name = "Super Admin" });
        }

        public IList<UserPartitionMappingViewModel> Get()
        {
            List<UserPartitionMappingViewModel> obj = new List<UserPartitionMappingViewModel>();

            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                foreach (var item in db.CataleyaInformations.ToList())
                {
                    UserPartitionMappingViewModel user = new UserPartitionMappingViewModel();
                    List<PartitionLabelViewModel> ob = (List<PartitionLabelViewModel>)objj.Get();
                    user.CataleyaId = item.CataleyaId;
                    user.TLCId = item.TLCId;
                    user.Partition = new PartitionLabelViewModel { PartitionId = item.PartitionId };
                    if (item.TLCRole == 0) {
                        user.TLCRole.Id = item.TLCRole;
                        user.TLCRole.Name = "Customer";
                    }
                    if (item.TLCRole == 1)
                    {
                        user.TLCRole.Id = item.TLCRole;
                        user.TLCRole.Name = "Super Admin";
                    }
                    // user.PartitionId = item.PartitionId;
                    user.CataleyaUserName = item.CataleyaUserName;
                    user.LastAccountVerificationDate = item.LastAccountVerificationDate;
                    user.TLCEmail = item.TLCEmail;
                    user.Status = item.Status;
                    obj.Add(user);
                }
            }

            return new List<UserPartitionMappingViewModel>(obj);

        }

        public async Task<UserPartitionMappingViewModel> Edit(UserPartitionMappingViewModel user)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var result = db.CataleyaInformations.Find(user.CataleyaId);
                if (result != null)
                {
                    result.TLCId = user.TLCId;
                    result.TLCRole = user.TLCRole.Id;// user.TLCRole;
                    result.PartitionId = user.PartitionId;
                    result.CataleyaUserName = user.CataleyaUserName;
                    result.LastAccountVerificationDate = user.LastAccountVerificationDate;
                    result.TLCEmail = user.TLCEmail;
                    result.Status = user.Status;
                    db.SaveChanges();
                    if (user.PartitionId > 0)
                    {
                        this.cataleyaService.SetCataleyaUserNamePassword(result.CataleyaId, result.CataleyaUserName, result.CataleyaPassword, result.TLCEmail);
                        await this.cataleyaService.GetAccessTokenForCurrentUser();
                    }
                }
            }
            return user;
        }

        public UserPartitionMappingViewModel Delete(UserPartitionMappingViewModel user)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                CataleyaInformation _user = db.CataleyaInformations.Find(user.CataleyaId);
                if (user != null && user.CataleyaId > 0) {
                    AspNetUser aspNetUser = db.AspNetUsers.Find(user.TLCId);
                    if (aspNetUser != null && aspNetUser.Id != null) {
                        db.CataleyaInformations.Remove(_user);
                        db.AspNetUsers.Remove(aspNetUser);
                        db.SaveChanges();
                    }
                }
            }
            return user;
        }
    }
}
