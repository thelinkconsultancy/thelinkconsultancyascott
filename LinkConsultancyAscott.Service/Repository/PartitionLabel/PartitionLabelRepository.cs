﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The site activity log repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.PartitionLabel
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;

    /// <summary>
    /// The User Repository.
    /// </summary>
    public class PartitionLabelRepository : RepositoryBase, IPartitionLabelRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public PartitionLabelRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="user">The user.</param>
        public int? Adds(PartitionLabelViewModel user)
        {
            int? id = 0;
            PartitionLabel obj = new PartitionLabel();
            obj.PartitionId = (int)user.PartitionId;
            obj.PartitionLabelId = user.PartitionLabelId;
            obj.PartitionLabels = user.PartitionLabels;
            obj.DirectoryPath = user.DirectoryPath;
            obj.IstraIp = user.IstraIp;
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                context.PartitionLabels.Add(obj);
                context.SaveChanges();
                string path = obj.DirectoryPath + obj.IstraIp;
                CreateDirectoryForIstra(path);
                id = obj.PartitionId;
            }
            return id;
        }

        public void Delete(PartitionLabelViewModel user)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                PartitionLabel obj = new PartitionLabel();
                obj.PartitionId = (int)user.PartitionId;
                obj.PartitionLabelId = user.PartitionLabelId;
                obj.PartitionLabels = user.PartitionLabels;
                obj.DirectoryPath = user.DirectoryPath;
                obj.IstraIp = user.IstraIp;
                db.PartitionLabels.Attach(obj);
                db.PartitionLabels.Remove(obj);
                db.SaveChanges();
            }
        }

        public IList<PartitionLabelViewModel> Get()
        {
            List<PartitionLabelViewModel> obj = new List<PartitionLabelViewModel>();

            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                foreach (var item in db.PartitionLabels.ToList())
                {
                    PartitionLabelViewModel user = new PartitionLabelViewModel();
                    user.PartitionId = item.PartitionId;
                    user.PartitionLabelId = item.PartitionLabelId;
                    user.PartitionLabels = item.PartitionLabels;
                    user.DirectoryPath = item.DirectoryPath;
                    user.IstraIp = item.IstraIp;
                    obj.Add(user);
                }
            }
            return new List<PartitionLabelViewModel>(obj);
        }

        public PartitionLabelViewModel Edit(PartitionLabelViewModel user)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var result = db.PartitionLabels.Find(user.PartitionId);
                if (result != null)
                {
                    result.PartitionLabelId = user.PartitionLabelId;// = "Some new value";
                    result.PartitionLabels = user.PartitionLabels;
                    result.IstraIp = user.IstraIp;
                    result.DirectoryPath = user.DirectoryPath;
                    db.SaveChanges();
                    var path = result.DirectoryPath + result.IstraIp + @"\";
                    CreateDirectoryForIstra(path);
                }
            }
            return user;
        }

        private void CreateDirectoryForIstra(string directoryPath)
        {
            try
            {
                if (!Directory.Exists(directoryPath))
                {
                    DirectoryInfo di = Directory.CreateDirectory(directoryPath);
                }
            }
            catch (System.Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}

