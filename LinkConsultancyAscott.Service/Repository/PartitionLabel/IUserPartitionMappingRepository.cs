﻿

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The User Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.PartitionLabel
{

    using Data;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using Repository;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The User Repository interface.
    /// </summary>
    public interface IUserPartitionMappingRepository : IRepositoryBase
    {
        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="user">
        /// The user.

        UserPartitionMappingViewModel Delete(UserPartitionMappingViewModel user);
        Task<UserPartitionMappingViewModel> Edit(UserPartitionMappingViewModel user);
        IList<UserPartitionMappingViewModel> Get();
    }
}

