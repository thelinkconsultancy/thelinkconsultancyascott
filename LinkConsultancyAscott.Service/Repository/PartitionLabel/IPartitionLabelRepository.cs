﻿
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The User Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.PartitionLabel
{

    using Data;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using Repository;
    using System.Collections.Generic;

    /// <summary>
    /// The User Repository interface.
    /// </summary>
    public interface IPartitionLabelRepository : IRepositoryBase 
    {
        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="user">
        /// The user.

        int? Adds(PartitionLabelViewModel user);
        PartitionLabelViewModel Edit(PartitionLabelViewModel user);
        void Delete(PartitionLabelViewModel user);
        IList<PartitionLabelViewModel> Get();
        //   int Get(PartitionLabelViewModel user);
        //   new PartitionLabel Get(int id);


    }
}

