﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILogRepository.cs" company="The Link Consultancy - Ascott">
//    The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the ILogRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.LogRepository
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Repository;

    /// <summary>
    /// Interface ILogRepository
    /// </summary>
    public interface ILogRepository : IDataEntityRepositoryBase<long, Log>
    {
    }
}
