﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The RequirementType Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclRepository
{
    using LinkConsultancyAscott.Data;

    using AclListType = LinkConsultancyAscott.Service.Models.AclListType;

    /// <summary>
    /// The Acl Repository interface.
    /// </summary>
    public interface IAclRepository : IAuditedEntityRepositoryBase<int, Acl>
    {
        bool IpAddressExistsInList(string ipAddress, AclListType aclListType);
    }
}
