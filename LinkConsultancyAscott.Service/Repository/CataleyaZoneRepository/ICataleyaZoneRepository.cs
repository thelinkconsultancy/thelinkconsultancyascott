﻿using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Repository.CataleyaNodeRepository
{
    public interface ICataleyaZoneRepository : IRepositoryBase
    {
        IList<CataleyaZoneViewModel> Get();
        int Add(Datum zone);
        bool CheckIfCataleyaZoneAlreadyExists(int zoneId, int nodeId, int operatorId);
        void DeleteAllCataleyaZones();
    }
}
