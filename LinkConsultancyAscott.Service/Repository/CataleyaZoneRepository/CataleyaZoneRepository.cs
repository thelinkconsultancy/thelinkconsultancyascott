﻿using LinkConsultancyAscott.Data;
using LinkConsultancyAscott.Service.Infrastructure;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;
using System.Linq;

namespace LinkConsultancyAscott.Service.Repository.CataleyaNodeRepository
{
    public class CataleyaZoneRepository : RepositoryBase, ICataleyaZoneRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ICataleyaNodeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public CataleyaZoneRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IList<CataleyaZoneViewModel> Get()
        {
            List<CataleyaZoneViewModel> obj = new List<CataleyaZoneViewModel>();

            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                //foreach (var item in db.CataleyaZones.ToList())
                //{
                //    CataleyaZoneViewModel zone = new CataleyaZoneViewModel();
                //    zone.ZoneId = item.ZoneId;
                //    zone.NodeId = item.NodeId;
                //    zone.OperatorId = item.OperatorId;
                //    obj.Add(zone);
                //}
            }
            return new List<CataleyaZoneViewModel>(obj);
        }

        public int Add(Datum zone)
        {
            int id = 0;
            CataleyaZone obj = new CataleyaZone();
            obj.ZoneId = zone.compoundKey.id;
            obj.NodeId = zone.compoundKey.nodeid;
            obj.OperatorId = zone.operatorId;
            obj.ZoneName = zone.name;
            obj.ZoneType = zone.zoneType;
            obj.ZoneDescription = zone.description;
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                context.CataleyaZones.Add(obj);
                context.SaveChanges();
                id = obj.ZoneId;
            }
            return id;
        }

        public bool CheckIfCataleyaZoneAlreadyExists(int zoneId, int nodeId, int operatorId)
        {
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var response = context.CataleyaZones.Find(zoneId, nodeId, operatorId);
                if (response != null && response.ZoneId > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public void DeleteAllCataleyaZones()
        {
            try
            {
                using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
                {
                    var response = context.CataleyaZones.ToList();
                    foreach (var item in response)
                    {
                        context.CataleyaZones.Remove(item);
                        context.SaveChanges();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
