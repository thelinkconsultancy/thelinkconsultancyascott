﻿using LinkConsultancyAscott.Service.ViewModels.Snitch;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Repository.SnitchRepository
{
    public interface ISnitchRepository : IRepositoryBase
    {
        IList<IpDataDateViewModel> Get();
    }
}
