﻿using LinkConsultancyAscott.Data;
using LinkConsultancyAscott.Service.Infrastructure;
using LinkConsultancyAscott.Service.ViewModels.Snitch;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinkConsultancyAscott.Service.Repository.SnitchRepository
{
    public class SnitchRepository : RepositoryBase, ISnitchRepository
    {
        public SnitchRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IList<IpDataDateViewModel> Get()
        {
            List<IpDataDateViewModel> obj = new List<IpDataDateViewModel>();

            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                foreach (var item in db.IpDataDates.ToList())
                {
                    IpDataDateViewModel ipDataDate = new IpDataDateViewModel();
                    ipDataDate.LastUpdate = Convert.ToString(UnixTimeStampToDateTime(item.LastUpdate));
                    ipDataDate.DataCount = Convert.ToString(item.DataCount);
                    obj.Add(ipDataDate);
                }
            }
            return new List<IpDataDateViewModel>(obj);
        }

        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
