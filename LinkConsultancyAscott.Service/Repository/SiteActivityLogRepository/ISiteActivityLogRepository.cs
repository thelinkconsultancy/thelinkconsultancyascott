﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISiteActivityLogRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The SiteActivityLogRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.SiteActivityLogRepository
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Repository;

    /// <summary>
    /// The SiteActivityLogRepository interface.
    /// </summary>
    public interface ISiteActivityLogRepository : IDataEntityRepositoryBase<long, SiteActivityLog>
    {
        void AddViaTransaction(SiteActivityLog log);
    }
}
