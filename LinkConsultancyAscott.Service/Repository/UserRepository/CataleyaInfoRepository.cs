﻿


// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The site activity log repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.UserRepository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Models.User;
    using LinkConsultancyAscott.Service.Repository;
    using LinkConsultancyAscott.Service.Repository.PartitionLabel;
    using LinkConsultancyAscott.Service.ViewModels.Acl;

    /// <summary>
    /// The User Repository.
    /// </summary>
    public class CataleyaInfoRepository : RepositoryBase, ICalaleyaInfoRepository
    {
        //IUserPartitionMappingRepository userPartitionMappingRepository;
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public CataleyaInfoRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

      

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="userRoles">The user roles.</param>
        public void Add(CataleyaInformation user)
        {
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                context.CataleyaInformations.Add(user);
                context.SaveChanges();
            }
          //  transaction.Complete();

           // this.Database.CataleyaInformations.Add(user);
            
        }

        public UserPartitionMappingViewModel Edit(UserPartitionMappingViewModel viewModel)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var result = db.CataleyaInformations.Find(viewModel.CataleyaId);
                if (result != null)
                {
                    result.LastAccountVerificationDate = viewModel.LastAccountVerificationDate;
                    result.Status = viewModel.Status;
                    db.SaveChanges();
                }
            }
            return viewModel;
        }
    }
}

