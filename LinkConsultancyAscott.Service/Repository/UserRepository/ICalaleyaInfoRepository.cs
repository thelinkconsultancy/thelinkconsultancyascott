﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The User Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.UserRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Data;

    using LinkConsultancyAscott.Service.Models.User;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using Repository;

    /// <summary>
    /// The User Repository interface.
    /// </summary>
    public interface ICalaleyaInfoRepository : IRepositoryBase
    {
        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="user">
        /// The user.

        void Add(CataleyaInformation user);
        UserPartitionMappingViewModel Edit(UserPartitionMappingViewModel viewModel);
    }
}
