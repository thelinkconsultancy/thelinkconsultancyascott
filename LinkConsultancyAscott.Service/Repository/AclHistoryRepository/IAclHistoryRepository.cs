﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclHistoryRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The ACl History Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclHistoryRepository
{
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using LinkConsultancyAscott.Service.ViewModels.Cataleya;

    /// <summary>
    /// The AclHistory Repository interface.
    /// </summary>
    public interface IAclHistoryRepository : IAuditedEntityRepositoryBase<int, AclHistory>
    {
        CataleyaAclHistoryViewModel DeleteCataleyaAclHistory(CataleyaAclHistoryViewModel cataleyaAclHistory);
        CataleyaAclHistoryViewModel GetCataleyaAclHistoryByNode(int nodeId, int zoneId, string ipAddress);
        IList<CataleyaAclHistoryViewModel> GetAllCataleyaAclHistory(int batchNo);
        IList<CataleyaAclHistoryViewModel> GetAllCataleyaAclHistory();
        CataleyaAclHistoryViewModel GetCataleyaAclHistoryById(int id);
        bool CheckIfTheIpAddressIsBlacklisted(string IpAddress);
        IQueryable<vw_AclHistory> GetAllQueryableExpanded();
        int GetLastBatchNo();
    }
}
