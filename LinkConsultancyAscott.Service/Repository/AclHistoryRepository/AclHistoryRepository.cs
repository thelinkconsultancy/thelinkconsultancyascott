﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclHistoryRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The ACL History repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclHistoryRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Data;
    using Infrastructure;
    using LinkConsultancyAscott.Service.ViewModels.Cataleya;

    /// <summary>
    /// The ACL History Repository.
    /// </summary>
    public class AclHistoryRepository : AuditedEntityRepositoryBase<int, AclHistory>, IAclHistoryRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AclHistoryRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AclHistoryRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IQueryable<vw_AclHistory> GetAllQueryableExpanded()
        {
            return this.Database.vw_AclHistory;
        }

        public IList<CataleyaAclHistoryViewModel> GetAllCataleyaAclHistory()
        {
            List<CataleyaAclHistoryViewModel> obj = new List<CataleyaAclHistoryViewModel>();
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                foreach (var item in db.Database.SqlQuery<CataleyaAclHistoryViewModel>("exec USP_GetCataleyaAclHistory").ToList())
                {
                    CataleyaAclHistoryViewModel cataleyaAclHistory = new CataleyaAclHistoryViewModel();
                    cataleyaAclHistory.Id = item.Id;
                    cataleyaAclHistory.Action = item.Action;
                    cataleyaAclHistory.AppType = item.AppType;
                    cataleyaAclHistory.BatchNo = item.BatchNo;
                    cataleyaAclHistory.CurrentDate = (DateTime)item.CurrentDate;
                    cataleyaAclHistory.isDeleted = item.isDeleted;
                    cataleyaAclHistory.LocalPort = item.LocalPort;
                    cataleyaAclHistory.NodeId = (int)item.NodeId;
                    cataleyaAclHistory.NodeName = item.NodeName;
                    cataleyaAclHistory.RemoteIpAddress = item.RemoteIpAddress;
                    cataleyaAclHistory.RemotePort = item.RemotePort;
                    cataleyaAclHistory.RemotePrefix = item.RemotePrefix;
                    cataleyaAclHistory.SipInterfaceAddressType = item.SipInterfaceAddressType;
                    cataleyaAclHistory.SipInterfaceId = (int)item.SipInterfaceId;
                    cataleyaAclHistory.SipInterfaceIpAddress = item.SipInterfaceIpAddress;
                    cataleyaAclHistory.Transport = item.Transport;
                    cataleyaAclHistory.Ver = (int)item.Ver;
                    obj.Add(cataleyaAclHistory);
                }
            }
            return obj;
        }

        public IList<CataleyaAclHistoryViewModel> GetAllCataleyaAclHistory(int batchNo)
        {
            List<CataleyaAclHistoryViewModel> obj = new List<CataleyaAclHistoryViewModel>();
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                foreach (var item in db.Database.SqlQuery<CataleyaAclHistoryViewModel>("exec USP_GetCataleyaAclHistory").Where(x => x.BatchNo == batchNo).ToList())
                {
                    CataleyaAclHistoryViewModel cataleyaAclHistory = new CataleyaAclHistoryViewModel();
                    cataleyaAclHistory.Id = item.Id;
                    cataleyaAclHistory.Action = item.Action;
                    cataleyaAclHistory.AppType = item.AppType;
                    cataleyaAclHistory.BatchNo = item.BatchNo;
                    cataleyaAclHistory.CurrentDate = (DateTime)item.CurrentDate;
                    cataleyaAclHistory.isDeleted = item.isDeleted;
                    cataleyaAclHistory.LocalPort = item.LocalPort;
                    cataleyaAclHistory.NodeId = (int)item.NodeId;
                    cataleyaAclHistory.RemoteIpAddress = item.RemoteIpAddress;
                    cataleyaAclHistory.RemotePort = item.RemotePort;
                    cataleyaAclHistory.RemotePrefix = item.RemotePrefix;
                    cataleyaAclHistory.SipInterfaceAddressType = item.SipInterfaceAddressType;
                    cataleyaAclHistory.SipInterfaceId = (int)item.SipInterfaceId;
                    cataleyaAclHistory.SipInterfaceIpAddress = item.SipInterfaceIpAddress;
                    cataleyaAclHistory.Transport = item.Transport;
                    cataleyaAclHistory.Ver = (int)item.Ver;
                    if (item.CataleyaSecurityAclId != null || item.CataleyaSecurityAclId > 0)
                    {
                        cataleyaAclHistory.CataleyaSecurityAclId = (int)item.CataleyaSecurityAclId;
                    }
                    else
                    {
                        cataleyaAclHistory.CataleyaSecurityAclId = 0;
                    }
                    obj.Add(cataleyaAclHistory);
                }
            }
            return obj;
        }

        public CataleyaAclHistoryViewModel DeleteCataleyaAclHistory(CataleyaAclHistoryViewModel cataleyaAclHistory)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var _aclHistory = new CataleyaAclHistory();
                _aclHistory.Action = cataleyaAclHistory.Action;
                _aclHistory.AppType = cataleyaAclHistory.AppType;
                _aclHistory.BatchNo = cataleyaAclHistory.BatchNo;
                _aclHistory.CurrentDate = DateTime.Now;
                _aclHistory.isDeleted = true;
                _aclHistory.LocalPort = cataleyaAclHistory.LocalPort;
                _aclHistory.NodeId = cataleyaAclHistory.NodeId;
                _aclHistory.RemoteIpAddress = cataleyaAclHistory.RemoteIpAddress;
                _aclHistory.RemotePort = cataleyaAclHistory.RemotePort;
                _aclHistory.RemotePrefix = cataleyaAclHistory.RemotePrefix;
                _aclHistory.SipInterfaceAddressType = cataleyaAclHistory.SipInterfaceAddressType;
                _aclHistory.SipInterfaceId = cataleyaAclHistory.SipInterfaceId;
                _aclHistory.SipInterfaceIpAddress = cataleyaAclHistory.SipInterfaceIpAddress;
                _aclHistory.Transport = cataleyaAclHistory.Transport;
                _aclHistory.Ver = cataleyaAclHistory.Ver;
                db.CataleyaAclHistories.Add(_aclHistory);
                db.SaveChanges();
            }
            return cataleyaAclHistory;
        }

        public CataleyaAclHistoryViewModel GetCataleyaAclHistoryById(int id)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var _aclHistory = db.CataleyaAclHistories.Find(id);
                if (_aclHistory != null && _aclHistory.Id > 0)
                {
                    var cataleyaAclHistory = new CataleyaAclHistoryViewModel();
                    cataleyaAclHistory.Action = _aclHistory.Action;
                    cataleyaAclHistory.AppType = _aclHistory.AppType;
                    cataleyaAclHistory.BatchNo = _aclHistory.BatchNo;
                    cataleyaAclHistory.CurrentDate = (DateTime)_aclHistory.CurrentDate;
                    cataleyaAclHistory.Id = _aclHistory.Id;
                    cataleyaAclHistory.isDeleted = _aclHistory.isDeleted;
                    cataleyaAclHistory.LocalPort = _aclHistory.LocalPort;
                    cataleyaAclHistory.NodeId = (int)_aclHistory.NodeId;
                    cataleyaAclHistory.RemoteIpAddress = _aclHistory.RemoteIpAddress;
                    cataleyaAclHistory.RemotePort = _aclHistory.RemotePort;
                    cataleyaAclHistory.RemotePrefix = _aclHistory.RemotePrefix;
                    cataleyaAclHistory.SipInterfaceAddressType = _aclHistory.SipInterfaceAddressType;
                    cataleyaAclHistory.SipInterfaceId = (int)_aclHistory.SipInterfaceId;
                    cataleyaAclHistory.SipInterfaceIpAddress = _aclHistory.SipInterfaceIpAddress;
                    cataleyaAclHistory.Transport = _aclHistory.Transport;
                    cataleyaAclHistory.Ver = (int)_aclHistory.Ver;
                    return cataleyaAclHistory;
                }
                return new CataleyaAclHistoryViewModel();
            }
        }

        public CataleyaAclHistoryViewModel GetCataleyaAclHistoryByNode(int nodeId, int zoneId, string ipAddress)
        {
            try
            {
                using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
                {
                    var sipInterface = (from sl in db.CataleyaSourceLists
                                        from si in db.CataleyaSipInterfaces
                                        select new SipInterfaceViewModel
                                        {
                                            NodeId = (int)si.NodeId,
                                            ZoneId = si.ZoneId,
                                            IpInterfaceId = (int)si.IpInterfaceId
                                        }).FirstOrDefault(x => x.NodeId == nodeId &&  x.ZoneId == zoneId);
                    var _aclHistory = db.CataleyaAclHistories.FirstOrDefault(x => x.NodeId == nodeId && x.SipInterfaceId == sipInterface.IpInterfaceId && x.RemoteIpAddress == ipAddress);
                    if (_aclHistory != null && _aclHistory.Id > 0)
                    {
                        var cataleyaAclHistory = new CataleyaAclHistoryViewModel();
                        cataleyaAclHistory.Id = _aclHistory.Id;
                        cataleyaAclHistory.Action = _aclHistory.Action;
                        cataleyaAclHistory.AppType = _aclHistory.AppType;
                        cataleyaAclHistory.BatchNo = _aclHistory.BatchNo;
                        cataleyaAclHistory.NodeId = (int)_aclHistory.NodeId;
                        cataleyaAclHistory.isDeleted = _aclHistory.isDeleted;
                        cataleyaAclHistory.LocalPort = _aclHistory.LocalPort;
                        cataleyaAclHistory.Transport = _aclHistory.Transport;
                        cataleyaAclHistory.RemotePort = _aclHistory.RemotePort;
                        cataleyaAclHistory.RemotePrefix = _aclHistory.RemotePrefix;
                        cataleyaAclHistory.RemoteIpAddress = _aclHistory.RemoteIpAddress;
                        cataleyaAclHistory.CurrentDate = (DateTime)_aclHistory.CurrentDate;
                        cataleyaAclHistory.SipInterfaceId = (int)_aclHistory.SipInterfaceId;
                        cataleyaAclHistory.SipInterfaceIpAddress = _aclHistory.SipInterfaceIpAddress;
                        cataleyaAclHistory.CataleyaSecurityAclId = _aclHistory.CataleyaSecurityAclId;
                        cataleyaAclHistory.SipInterfaceAddressType = _aclHistory.SipInterfaceAddressType;
                        return cataleyaAclHistory;
                    }
                    return new CataleyaAclHistoryViewModel();
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public bool CheckIfTheIpAddressIsBlacklisted(string IpAddress)
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var _blacklist = db.IPDatas.FirstOrDefault(x => x.Ipaddress == IpAddress);
                if (_blacklist != null && _blacklist.Id > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public int GetLastBatchNo()
        {
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                CataleyaAclHistory _aclHistory = db.CataleyaAclHistories.OrderByDescending(a => a.Id).FirstOrDefault();
                return _aclHistory.BatchNo;
            }
        }
    }
}