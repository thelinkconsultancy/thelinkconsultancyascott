﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataEntityRepositoryBase.cs" company="The Link Consultancy - Ascott">
//  The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The DataEntityRepositoryBase interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;

    using Data;

    public interface IAuditedEntityRepositoryBase<T, DTO> : IDataEntityRepositoryBase<T, DTO>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where DTO : class, IAuditedEntity<T>
    {
    }
}
