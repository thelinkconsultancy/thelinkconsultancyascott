﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAppConfigRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The AppConfig Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AppConfigRepository
{
    using System.Collections.Generic;

    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The AppConfig Repository interface.
    /// </summary>
    public interface IAppConfigRepository : ILookUpRepository<int, AppConfig>
    {
        /// <summary>
        /// Gets the default claim types.
        /// </summary>
        /// <returns>IEnumerable&lt;System.String&gt;.</returns>
        IEnumerable<string> GetDefaultClaimTypes();

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        string GetValue(string key);
    }
}
