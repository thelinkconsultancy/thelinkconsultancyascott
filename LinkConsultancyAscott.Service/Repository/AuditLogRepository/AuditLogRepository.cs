﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditLogRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Log Repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AuditLogRepository
{
    using System.Linq;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// The user data repository.
    /// </summary>
    public class AuditLogRepository : DataEntityRepositoryBase<long, AuditLog>, IAuditLogRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuditLogRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AuditLogRepository(IDatabaseFactory databaseFactory) 
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// Gets the summary.
        /// </summary>
        /// <returns> List of Audit Logs. </returns>
        public IQueryable<vw_AuditLog> GetSummary()
        {
            return this.Database.vw_AuditLog;
        }
    }
}