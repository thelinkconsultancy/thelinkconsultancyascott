﻿using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;

namespace LinkConsultancyAscott.Service.Repository.CataleyaSipInterfaceRepository
{
    public interface ICataleyaSipInterfaceRepository
    {
        int Add(int nodeId, int zoneId, SipInterface sipInterface);
        bool CheckIfSipInterfaceAlreadyExists(int interfaceId, int zoneId);
        void DeleteAllCataleyaSipInterfaces();
    }
}
