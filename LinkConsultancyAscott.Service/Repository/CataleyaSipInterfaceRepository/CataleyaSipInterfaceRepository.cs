﻿using LinkConsultancyAscott.Data;
using LinkConsultancyAscott.Service.Infrastructure;
using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkConsultancyAscott.Service.Repository.CataleyaSipInterfaceRepository
{
    public class CataleyaSipInterfaceRepository : RepositoryBase, ICataleyaSipInterfaceRepository
    {
        public CataleyaSipInterfaceRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public int Add(int nodeId, int zoneId, SipInterface sipInterface)
        {
            int id = 0;
            CataleyaSipInterface obj = new CataleyaSipInterface();
            obj.InterfaceId = sipInterface.compoundKey.id;
            obj.ZoneId = zoneId;
            obj.NodeId = nodeId;
            obj.InterfaceName = sipInterface.name;
            obj.InterfaceDescription = sipInterface.description;
            obj.Port = sipInterface.port;
            obj.IpInterfaceId = sipInterface.ipInterfaceId;
            obj.IpSubnetId = sipInterface.ipInterface.ipSubnetId.ToString();
            obj.IpAdress = sipInterface.ipInterface.ipaddress;
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                context.CataleyaSipInterfaces.Add(obj);
                context.SaveChanges();
                id = obj.InterfaceId;
            }
            return id;
        }

        public bool CheckIfSipInterfaceAlreadyExists(int interfaceId, int zoneId)
        {
            try
            {
                using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
                {
                    var response = context.CataleyaSipInterfaces.FirstOrDefault(x => x.InterfaceId == interfaceId && x.ZoneId == zoneId);
                    if (response != null && response.InterfaceId > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public void DeleteAllCataleyaSipInterfaces()
        {
            try
            {
                using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
                {
                    var response = context.CataleyaSipInterfaces.ToList();
                    foreach (var item in response)
                    {
                        context.CataleyaSipInterfaces.Remove(item);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
