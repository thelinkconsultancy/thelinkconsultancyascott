﻿using LinkConsultancyAscott.Data;
using LinkConsultancyAscott.Service.Infrastructure;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;
using System.Linq;

namespace LinkConsultancyAscott.Service.Repository.CataleyaSourceListRepository
{
    public class CataleyaSourceListRepository : RepositoryBase, ICataleyaSourceListRepository
    {
        public CataleyaSourceListRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IList<CataleyaSourceListViewModel> Get()
        {
            List<CataleyaSourceListViewModel> obj = new List<CataleyaSourceListViewModel>();
            using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                foreach (var item in db.CataleyaSourceLists.ToList())
                {
                    CataleyaSourceListViewModel cataleyaSourceList = new CataleyaSourceListViewModel();
                    cataleyaSourceList.id = item.id;
                    cataleyaSourceList.addressType = item.addressType;
                    cataleyaSourceList.ipAddress = item.ipAddress;
                    cataleyaSourceList.nodeId = (int)item.nodeId;
                    cataleyaSourceList.operatorId = item.operatorId;
                    cataleyaSourceList.subnetMask = item.subnetMask;
                    cataleyaSourceList.zoneId = (int)item.zoneId;
                    obj.Add(cataleyaSourceList);
                }
            }
            return new List<CataleyaSourceListViewModel>(obj);
        }

        public int Add(CataleyaSourceListViewModel cataleyaSourceList)
        {
            int id = 0;
            CataleyaSourceList obj = new CataleyaSourceList();
            obj.id = cataleyaSourceList.id;
            obj.addressType = cataleyaSourceList.addressType;
            obj.ipAddress = cataleyaSourceList.ipAddress;
            obj.subnetMask = cataleyaSourceList.subnetMask;
            obj.zoneId = cataleyaSourceList.zoneId;
            obj.nodeId = cataleyaSourceList.nodeId;
            obj.operatorId = cataleyaSourceList.operatorId;
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                context.CataleyaSourceLists.Add(obj);
                context.SaveChanges();
                id = obj.id;
            }
            return id;
        }

        public bool CheckIfCataleyaSourceListAlreadyExists(int id, int zoneId, int nodeId)
        {
            using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            {
                var response = context.CataleyaSourceLists.FirstOrDefault(x => x.id == id && x.zoneId == zoneId && x.nodeId == nodeId);
                if (response != null && response.id > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public void DeleteAllCataleyaSourceList()
        {
            try
            {
                using (var context = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
                {
                    var response = context.CataleyaSourceLists.ToList();
                    foreach (var item in response)
                    {
                        context.CataleyaSourceLists.Remove(item);
                        context.SaveChanges();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
