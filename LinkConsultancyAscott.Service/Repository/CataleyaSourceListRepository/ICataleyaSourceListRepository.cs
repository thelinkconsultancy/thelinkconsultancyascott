﻿using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using System.Collections.Generic;

namespace LinkConsultancyAscott.Service.Repository.CataleyaSourceListRepository
{
    public interface ICataleyaSourceListRepository
    {
        int Add(CataleyaSourceListViewModel cataleyaSourceList);
        bool CheckIfCataleyaSourceListAlreadyExists(int id, int zoneId, int nodeId);
        IList<CataleyaSourceListViewModel> Get();
        void DeleteAllCataleyaSourceList();
    }
}
