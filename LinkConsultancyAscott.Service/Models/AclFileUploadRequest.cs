﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclFileUploadRequest.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Class AclFileUploadRequest Request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    using System;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Class AclFileUploadRequest Request.
    /// </summary>
    public class AclFileUploadRequest : AclRequestBase
    {
        /// <summary>
        /// Gets or sets the filename.
        /// </summary>
        /// <value>The filename.</value>
        public string Filename { get; set; }
        
        /// <summary>
        /// Gets or sets the file data.
        /// </summary>
        /// <value>The file Data.</value>
        public MemoryStream FileData { get; set; }
    }
}
