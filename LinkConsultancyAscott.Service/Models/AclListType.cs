﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclListType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum EmailType
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    using System.ComponentModel;

    /// <summary>
    /// Enum AclListType
    /// </summary>
    public enum AclListType
    {
        /// <summary>
        /// The white list
        /// </summary>
        [Description("White")]
        WhiteList = 1,

        /// <summary>
        /// The black list
        /// </summary>
        [Description("Black")]
        BlackList = 2
    }
}
