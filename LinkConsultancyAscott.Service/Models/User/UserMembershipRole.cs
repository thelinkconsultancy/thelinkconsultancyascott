﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserMembershipRole.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum UserMembershipRole
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.User
{
    using System.Collections.Generic;

    /// <summary>
    /// Enum UserMembershipRole
    /// </summary>
    public class UserMembershipRole
    {
        /// <summary>
        /// Gets or sets the name of the contact.
        /// </summary>
        /// <value>The name of the contact.</value>
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or sets the contact email address.
        /// </summary>
        /// <value>The contact email address.</value>
        public string ContactEmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the role to documents map.
        /// </summary>
        /// <value>The role to documents map.</value>
        public Dictionary<string, List<string>> RoleToDocumentsMap { get; set; }
    }
}
