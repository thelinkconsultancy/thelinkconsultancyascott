﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataEntityInterfaceBase.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the DataEntityInterfaceBase.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{

    /// <summary>
    /// Class DataEntityBase.
    /// </summary>
    public class DataEntityInterfaceBase : DataEntityBase
    {
        public string name { get; set; }

        public string description { get; set; }

        public int ipInterfaceId { get; set; }

        public int? chmod { get; set; }
        public IpInterface ipInterface { get; set; }
        public Operator @operator { get; set; }

    }
}
