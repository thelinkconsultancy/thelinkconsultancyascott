﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataWrapperEntityBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the DataWrapperEntityBase.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{

    /// <summary>
    /// Class DataWrapperEntityBase.
    /// </summary>
    public class DataWrapperEntityBase<T> : DataWrapperBase<T>
    {
    }
}
