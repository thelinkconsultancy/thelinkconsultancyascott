﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompoundKey.cs" company="">
//   
// </copyright>
// <summary>
//   Class CompoundKey.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{
    /// <summary>
    /// Class CompoundKey.
    /// </summary>
    public class CompoundKey
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int id { get; set; }

        /// <summary>
        /// Gets or sets the nodeid.
        /// </summary>
        /// <value>The nodeid.</value>
        public int nodeid { get; set; }
    }
}