﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    public class RemoteEndPoint : DataEntityBase
    {
        public string name { get; set; }

        public string addressType { get; set; }

        public string ipAddress { get; set; }

        public int port { get; set; }

        public string transport { get; set; }

        public int priority { get; set; }

        public int weight { get; set; }

        public string uriScheme { get; set; }

        public string reuseConnection { get; set; }

        public string domain { get; set; }

        public int zoneId { get; set; }

        public int zoneNodeid { get; set; }
    }
}