﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    public class SourceList
    {
        public int id { get; set; }

        public string addressType { get; set; }

        public string ipAddress { get; set; }

        public int subnetMask { get; set; }

        public int zoneId { get; set; }

        public int nodeId { get; set; }

        public object operatorId { get; set; }
    }
}

