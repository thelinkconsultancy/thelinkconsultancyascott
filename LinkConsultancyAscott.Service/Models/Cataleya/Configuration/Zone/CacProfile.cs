﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    public class CacProfile
    {
        public CompoundKey compoundKey { get; set;
        }

        public int ver { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public int maxSessions { get; set; }

        public int maxSessionsIn { get; set; }

        public int maxSessionsOut { get; set; }

        public int rateSessionsIn { get; set; }

        public int rateSessionsOut { get; set; }

        public int rateXcodSessions { get; set; }

        public int maxXcodSessions { get; set; }

        public int burstRate { get; set; }

        public int burstDuration { get; set; }

        public int nonInvTxnRate { get; set; }

        public int pktRate { get; set; }

        public int mediaBandwidth { get; set; }

        public int regTxnRate { get; set; }

        public int maxRegistrations { get; set; }

        public int operatorId { get; set; }

        public int? chmod { get; set; }

        public object cacProfileType { get; set; }

        public Operator @operator { get; set; }

        public int nodeId { get; set; }

        public int id { get; set; }
    }
}