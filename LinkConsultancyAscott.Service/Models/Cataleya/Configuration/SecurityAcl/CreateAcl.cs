﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Acl.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the EmailTemplate View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// The Acl.
    /// </summary>
    public class CreateAcl
    {
        public CompoundKey compoundKey { get; set; }

        public int ver { get; set; }

        public int? operatorId { get; set; }

        public int nodeId { get; set; }

        /// <summary>
        /// Gets or sets the type of the application.
        /// </summary>
        /// <value>The type of the application.</value>
        public string appType { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>The action.</value>
        public string action { get; set; }
        public int ipinterfaceId { get; set; }
        public int localPort { get; set; }
        public string remoteIpAddress { get; set; }
        public int remotePrefix { get; set; }
        public int remotePort { get; set; }
        public string transport { get; set; }

        /// <summary>
        /// Gets or sets the ip interface.
        /// </summary>
        /// <value>The ip interface.</value>
        public IpInterface ipInterface { get; set; }
    }
}