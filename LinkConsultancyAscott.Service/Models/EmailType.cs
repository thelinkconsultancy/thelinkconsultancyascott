﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum EmailType
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    /// <summary>
    /// Enum EmailType
    /// </summary>
    public enum EmailType
    {
        /// <summary>
        /// The RID Open notification
        /// </summary>
        DataPackUploadedNotification = 1,

        /// <summary>
        /// The RID Open notification
        /// </summary>
        RidReviewOpenNotification = 2,

        /// <summary>
        /// The RID Closed notification
        /// </summary>
        RidReviewClosedNotification = 3,

        /// <summary>
        /// The rid with supplier notification
        /// </summary>
        RidWithSupplierNotification = 4,

        /// <summary>
        /// The data pack closed notification
        /// </summary>
        DataPackClosedNotification = 5
    }
}
