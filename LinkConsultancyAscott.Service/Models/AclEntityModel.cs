﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclEntityModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Class AclEntityModel Request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// Class AclEntityModel Request.
    /// </summary>
    public class AclEntityModel
    {
        /// <summary>
        /// Gets or sets the IpAddress.
        /// </summary>
        /// <value>The IpAddress.</value>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        /// <value>The prefix.</value>
        public int Prefix { get; set; }

        /// <summary>
        /// Gets or sets the type of the ACL.
        /// </summary>
        /// <value>The type of the ACL.</value>
        public AclListType AclType { get; set; }

        /// <summary>
        /// Gets or sets the ACL source.
        /// </summary>
        /// <value>The ACL source.</value>
        public AclSourceType AclSource { get; set; }

        /// <summary>
        /// Gets or sets the user data.
        /// </summary>
        /// <value>The user data.</value>
        public AspNetUser UserData { get; set; }

        /// <summary>
        /// Gets or sets the sip interface identifier.
        /// </summary>
        /// <value>The sip interface identifier.</value>
        public int InterfaceId { get; set; }

        /// <summary>
        /// Gets or sets the media interface identifier.
        /// </summary>
        /// <value>The media interface identifier.</value>
        public string InterfaceName { get; set; }

        /// <summary>
        /// Gets or sets the type of the interface.
        /// </summary>
        /// <value>The type of the interface.</value>
        public InterfaceType @InterfaceType { get; set; }

        /// <summary>
        /// Gets or sets the ip interface.
        /// </summary>
        /// <value>The ip interface.</value>
        public IpInterface @IpInterface { get; set; }
    }
}
