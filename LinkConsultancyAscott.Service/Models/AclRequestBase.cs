﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclRequestBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Class AclRequestBase Request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    using System;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Class AclRequestBase Request.
    /// </summary>
    public abstract class AclRequestBase
    {
        /// <summary>
        /// Gets or sets the type of the ACL.
        /// </summary>
        /// <value>The type of the ACL.</value>
        public AclListType AclType { get; set; }

        /// <summary>
        /// Gets or sets the ACL source.
        /// </summary>
        /// <value>The ACL source.</value>
        public AclSourceType AclSource { get; set; }

        /// <summary>
        /// Gets or sets the web token.
        /// </summary>
        /// <value>The web token.</value>
        public Guid WebToken { get; set; }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>The access token.</value>
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the name of the interface.
        /// </summary>
        /// <value>The name of the interface.</value>
        public string InterfaceName { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }
    }
}
