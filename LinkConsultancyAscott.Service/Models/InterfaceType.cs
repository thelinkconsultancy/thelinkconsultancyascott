﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InterfaceType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum InterfaceType
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    using System.ComponentModel;

    /// <summary>
    /// Enum InterfaceType
    /// </summary>
    public enum InterfaceType
    {
        [Description("SIP")]
        Sip = 1,

        [Description("Media")]
        Media = 2,

        /// <summary>
        /// The ip
        /// </summary>
        [Description("IP")]
        Ip = 3
    }
}
