﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TripStatus.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum RequirementType
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    /// <summary>
    /// Enum ApplicationRoles
    /// </summary>
    public static class ApplicationRoles
    {
        /// <summary>
        /// The administrators
        /// </summary>
        public const string Administrators = "Administrators";
        
        /// <summary>
        /// The developers
        /// </summary>
        public const string Developers = "Developers";

        /// <summary>
        /// The travellers
        /// </summary>
        public const string Users = "Users";
        
    }
}
