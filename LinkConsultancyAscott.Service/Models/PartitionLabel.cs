﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;

namespace LinkConsultancyAscott.Service.Models
{
    public class PartitionLabelModel
    {
        [Display(Name = "Partition Id")]
        public int PartitionId { get; set; }

        [Required]
        [Display(Name = "Partition Label")]
        public string PartitionLabel { get; set; }

        [Display(Name = "Partition Label Id")]
        public string PartitionLabelId { get; set; }

        [Display(Name = "Istra Ip")]
        public string IstraIp { get; set; }

    }
}
