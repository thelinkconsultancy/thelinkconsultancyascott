﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclHistoryType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum AclHistoryType
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    /// <summary>
    /// Enum AclHistoryType
    /// </summary>
    public enum AclHistoryType
    {
        /// <summary>
        /// The created
        /// </summary>
        Created = 1,


        /// <summary>
        /// The rollback
        /// </summary>
        Rollback = 2
    }
}
