﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDatabase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IDatabase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Threading.Tasks;

    using Data;

    /// <summary>
    /// Interface IDatabase
    /// </summary>
    public interface IDatabase
    {
        /// <summary>
        /// Gets or sets the logs.
        /// </summary>
        /// <value>The logs.</value>
        DbSet<Log> Logs { get; set; }
        
        /// <summary>
        /// Gets or sets the site activity logs.
        /// </summary>
        DbSet<SiteActivityLog> SiteActivityLogs { get; set; }

        /// <summary>
        /// Gets or sets the vw audit log.
        /// </summary>
        /// <value>The vw audit log.</value>
        DbSet<vw_AuditLog> vw_AuditLog { get; set; }

        DbSet<vw_Acls> vw_Acls { get; set; }
        DbSet<vw_AclHistory> vw_AclHistory { get; set; }

        DbSet<AspNetRole> AspNetRoles { get; set; }
        DbSet<AspNetUser> AspNetUsers { get; set; }
        DbSet<vw_UsersInRoles> vw_UsersInRoles { get; set; }
        DbSet<CataleyaInformation> CataleyaInformations { get; set; }


        /// <summary>
        /// Commits this instance.
        /// </summary>
        void Commit();

        /// <summary>
        /// Commits the asynchronous.
        /// </summary>
        /// <returns>Task.</returns>
        Task CommitAsync();

        /// <summary>
        /// Nons the tracking commit.
        /// </summary>
        void NonTrackingCommit();

        /// <summary>
        /// Nons the tracking commit asynchronous.
        /// </summary>
        /// <returns>Task.</returns>
        Task NonTrackingCommitAsync();

        /// <summary>
        /// Sets this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <returns> Result set.</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Sets the specified entity type.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>Database Set.</returns>
        DbSet Set(Type entityType);

        /// <summary>
        /// Entries the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>Database Entity Entry.</returns>
        DbEntityEntry Entry(object entity);

        /// <summary>
        /// Entries the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns> Entity list </returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
