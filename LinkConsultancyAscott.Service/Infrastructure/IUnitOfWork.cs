﻿namespace LinkConsultancyAscott.Service.Infrastructure
{
    using System.Threading.Tasks;

    /// <summary>
    /// The UnitOfWork interface.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// The commit.
        /// </summary>
        void Commit();

        Task CommitAsync();

        void NonTrackingCommit();

        Task NonTrackingCommitAsync();
    }
}
