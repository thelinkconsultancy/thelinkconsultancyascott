﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Database.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Database type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Data;

    /// <summary>
    /// The database.
    /// </summary>
    public class Database : LinkConsultancyAscottEntities, IDatabase
    {
        /// <summary>
        /// The log.
        /// </summary>
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Initializes a new instance of the <see cref="Database"/> class.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        public Database(string connectionString) 
            : base(connectionString)
        {
        }

        /// <summary>
        /// The commit.
        /// </summary>
        public virtual void Commit()
        {
            try
            {
                this.SaveChanges();
            }
            catch (Exception ex)
            {
               this.ProcessException(ex);
               throw;
            }
        }

        /// <summary>
        /// commit as an asynchronous operation.
        /// </summary>
        /// <returns>Returns Task.</returns>
        public async Task CommitAsync()
        {
            try
            {
                await this.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                this.ProcessException(ex);
                throw;
            }
        }

        /// <summary>
        /// The commit.
        /// </summary>
        public virtual void NonTrackingCommit()
        {
            try
            {
                this.NonTrackingSaveChanges();
            }
            catch (Exception ex)
            {
                this.ProcessException(ex);
                throw;
            }
        }

        public async Task NonTrackingCommitAsync()
        {
            try
            {
                await this.NonTrackingSaveChangesAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                this.ProcessException(ex);
                throw;
            }
        }

        /// <summary>
        /// The save changes.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public int NonTrackingSaveChanges()
        {
            return base.SaveChanges();
        }

        /// <summary>
        /// non tracking save changes as an asynchronous operation.
        /// </summary>
        /// <returns>Returns Task&lt;System.Int32&gt;.</returns>
        public async Task<int> NonTrackingSaveChangesAsync()
        {
            return await base.SaveChangesAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// The get event detail.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetEventDetail(object obj)
        {
            string description;
            try
            {
                description = "Auditing disabled" + obj.ToString();
            }
            catch (Exception ex)
            {
                description = "Error serialising object" + ex.Message;
            }

            return description;
        }

        /// <summary>
        /// Processes the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        private void ProcessException(Exception ex)
        {
            var exception = ex as DbEntityValidationException;
            if (exception != null)
            {
                var error = exception;
                Log.ErrorFormat("DbEntityValidationException {0}-{1} {2}", error.GetType(), error.Message, error.StackTrace);

                foreach (var r in error.EntityValidationErrors)
                {
                    var stringBuilder = new StringBuilder();
                    foreach (var validationError in r.ValidationErrors)
                    {
                        stringBuilder.AppendFormat("Property: {0} Error Message: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        stringBuilder.AppendLine();
                    }

                    Log.ErrorFormat("Entity: {0} IsValid: {1} Error: {2}", r.Entry.Entity, r.IsValid, stringBuilder);
                }
            }
            else
            {
                var updateException = ex as DbUpdateException;
                if (updateException != null)
                {
                    var error = updateException;
                    Log.ErrorFormat("DbUpdateException {0}-{1} {2}", error.GetType(), error.Message, error.StackTrace);
                }
                else
                {
                    Log.ErrorFormat("Error Type not handled [{0}] {1} {2}", ex.GetType(), ex.Message, ex.StackTrace);
                }
            }
        }
    }
}
