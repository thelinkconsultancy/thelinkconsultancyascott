﻿namespace LinkConsultancyAscott.Service.Infrastructure
{
    using System;

    /// <summary>
    /// The DatabaseFactory interface.
    /// </summary>
    public interface IDatabaseFactory : IDisposable
    {
        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="IDatabase"/>.
        /// </returns>
        IDatabase Get();

        string ConnectionString();
    }
}
