﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitOfWork.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the UnitOfWork type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Infrastructure
{
    using System.Threading.Tasks;

    /// <summary>
    /// The unit of work.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// The _database factory.
        /// </summary>
        private readonly IDatabaseFactory databaseFactory;

        /// <summary>
        /// The _database.
        /// </summary>
        private IDatabase database;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        /// <summary>
        /// Gets the database.
        /// </summary>
        protected IDatabase Database => this.database ?? (this.database = this.databaseFactory.Get());

        /// <summary>
        /// The commit.
        /// </summary>
        public void Commit()
        { 
            this.Database.Commit();
        }

        /// <summary>
        /// Commits the asynchronous.
        /// </summary>
        /// <returns>Task.</returns>
        public Task CommitAsync()
        {
            return this.Database.CommitAsync();
        }

        /// <summary>
        /// Nons the tracking commit.
        /// </summary>
        public void NonTrackingCommit()
        {
            this.Database.NonTrackingCommit();
        }

        /// <summary>
        /// Nons the tracking commit asynchronous.
        /// </summary>
        /// <returns>Task.</returns>
        public Task NonTrackingCommitAsync()
        {
            return this.Database.NonTrackingCommitAsync();
        }
    }
}
