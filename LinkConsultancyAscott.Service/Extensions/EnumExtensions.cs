﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumExtensions.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2015
// </copyright>
// <summary>
//   The enum extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Extensions
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The enumeration type extensions.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// The get description.
        /// </summary>
        /// <param name="enumValue">
        /// The enumeration type value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetDescription(this Enum enumValue)
        {
            Type type = enumValue.GetType();
            var memberInfo = type.GetMember(enumValue.ToString());

            if (memberInfo.Length == 0)
            {
                return enumValue.ToString();
            }

            object[] attrinutes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attrinutes.Length == 0)
            {
                return enumValue.ToString();
            }

            return ((DescriptionAttribute)attrinutes[0]).Description;
        }
    }
}
