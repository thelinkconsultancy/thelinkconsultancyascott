﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileExtensionConverter.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The file extension converter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Utilities
{
    using System.Collections.Generic;

    /// <summary>
    /// The file extension converter.
    /// </summary>
    public static class FileExtensionConverter
    {
        /// <summary>
        /// The extension mime type mapping.
        /// </summary>
        private static readonly IDictionary<string, string> ExtensionMimeTypeMapping;

        /// <summary>
        /// The default mime type.
        /// </summary>
        private static string defaultMIMEType = "application/octet-stream";

        /// <summary>
        /// Initializes static members of the <see cref="FileExtensionConverter"/> class.
        /// </summary>
        static FileExtensionConverter()
        {
            ExtensionMimeTypeMapping = new Dictionary<string, string>()
                                           {
                                               { "txt", "text/plain" },
                                               { "rtf", "text/richtext" },
                                               { "wav", "audio/wav" },
                                               { "gif", "image/gif" },
                                               { "jpeg", "image/jpeg" },
                                               { "png", "image/png" },
                                               { "tiff", "image/tiff" },
                                               { "bmp", "image/bmp" },
                                               { "avi", "video/avi" },
                                               { "mpeg", "video/mpeg" },
                                               { "pdf", "application/pdf" },
                                               { "doc", "application/msword" },
                                               { "dot", "application/msword" },
                                               {
                                                   "docx",
                                                   "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                                                   },

                                               {
                                                   "dotx",
                                                   "application/vnd.openxmlformats-officedocument.wordprocessingml.template"
                                                   },
                                               { "xls", "application/vnd.ms-excel" },
                                               { "xlt", "application/vnd.ms-excel" },
                                               { "csv", "application/vnd.ms-excel" },
                                               {
                                                   "xlsx",
                                                   "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                                   },

                                               {
                                                   "xltx",
                                                   "application/vnd.openxmlformats-officedocument.spreadsheetml.template"
                                                   },

                                               { "ppt", "application/vnd.ms-powerpoint" },
                                               { "pot", "application/vnd.ms-powerpoint" },
                                               {
                                                   "pptx",
                                                   "application/vnd.openxmlformats-officedocument.presentationml.presentation"
                                                   },

                                               {
                                                   "potx",
                                                   "application/vnd.openxmlformats-officedocument.presentationml.template"
                                                   }
                                           };
        }

        /// <summary>
        /// The to mime type.
        /// </summary>
        /// <param name="extension">
        /// The extension.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ToMimeType(string extension)
        {
            if (string.IsNullOrEmpty(extension))
            {
                return defaultMIMEType;
            }

            string lowerExtension = extension.ToLower();
            string mime;
            if (!ExtensionMimeTypeMapping.TryGetValue(lowerExtension, out mime))
            {
                mime = defaultMIMEType;
            }

            return mime;
        }
    }
}
