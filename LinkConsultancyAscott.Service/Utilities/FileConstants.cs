﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileHelpers.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The File Helpers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Utilities
{
    using System.Collections.Generic;

    /// <summary>
    /// The file extension converter.
    /// </summary>
    public static class FileHelpers
    {
        /// <summary>
        /// The image extensions.
        /// </summary>
        public static readonly List<string> ImageExtensions = new List<string> { ".JPG", ".JPE", ".JPEG", ".BMP", ".GIF", ".PNG" };

        /// <summary>
        /// The document extensions.
        /// </summary>
        public static readonly List<string> DocumentExtensions = new List<string> { ".TXT", ".DOC", ".DOCX", ".XLS", ".XLSX", ".PDF" };
    }
}
