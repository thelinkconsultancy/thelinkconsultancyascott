﻿namespace LinkConsultancyAscott.Service.Utilities
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    /// <summary>
    /// Class UnitTestDetector.
    /// </summary>
    public static class UnitTestDetector
    {
        /// <summary>
        /// The unit test attributes
        /// </summary>
        private static readonly HashSet<string> UnitTestAttributes = new HashSet<string>
                {
                    "Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute",
                    "NUnit.Framework.TestFixtureAttribute",
                };

        /// <summary>
        /// Gets a value indicating whether this instance is running in unit test.
        /// </summary>
        /// <value><c>true</c> if this instance is running in unit test; otherwise, <c>false</c>.</value>
        public static bool IsRunningInUnitTest
        {
            get
            {
                var stackTrace = new StackTrace();

                if (stackTrace != null)
                {
                    var applicationFrames = stackTrace.GetFrames();

                    if (applicationFrames != null && applicationFrames.Any())
                    {
                        foreach (var f in applicationFrames)
                        {
                            var type = f.GetMethod().DeclaringType;
                            if (type != null)
                            {
                                if (type.GetCustomAttributes(false).Any(x => UnitTestAttributes.Contains(x.GetType().FullName)))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }

                return false;
            }
        }
    }
}
