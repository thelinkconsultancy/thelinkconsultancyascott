var Errors;
(function (Errors) {
    var ErrorManager = /** @class */ (function () {
        function ErrorManager() {
            this.activeWindowCount = 0;
        }
        ErrorManager.prototype.load = function () {
            //$('#PopupErrorWindow').html('<div style="text-align: center; position: absolute; top: 50%; left: 0px; width: 100%; height: 1px; overflow: visible; display: block;"><i class="far fa-spinner fa-pulse fa-5x"></i></div>');
            //$("#PopupErrorWindow").parent().addClass("error-popup");
        };
        ErrorManager.prototype.openErrorWindow = function (errorJson) {
            var _this = this;
            var newPopupDiv = $("<div></div>").appendTo("body");
            var errorWindow = $(newPopupDiv).kendoWindow({
                iframe: true,
                scrollable: true,
                width: "500px",
                height: "500px",
                draggable: true,
                modal: false,
                resizable: true,
                visible: false,
                actions: ["Pin", "Minimize", "Maximize", "Close"],
                close: function (e) {
                    _this.activeWindowCount--;
                    // Remove all the HTML for the window!
                    errorWindow.destroy();
                }
            }).data("kendoWindow");
            newPopupDiv.html("<div style='text- align: center; position: absolute; top: 50 %; left: 0px; width: 100 %; height: 1px; overflow: visible; display: block;'><i class='far fa- spinner fa- pulse fa- 5x'></i></div>");
            newPopupDiv.parent().addClass("error-popup");
            // Show the window
            if (this.activeWindowCount === 0) {
                errorWindow.center().open();
            }
            else {
                // Offset the position of the new window so it is 
                var newWindowPositionX = this.lastPopupPosition.left + 10;
                var newWindowPositionY = this.lastPopupPosition.top + 10;
                newPopupDiv.closest(".k-window").css({ left: newWindowPositionX, top: newWindowPositionY });
                errorWindow.open();
            }
            errorJson.split("\n").forEach(function (line) { return console.log(line); });
            var formattedJson = errorJson.replace(/\\+r\\+n/g, "\r\n").replace(/\\+"/g, "\"").replace(/\\+/g, "\\");
            formattedJson.split("\n").forEach(function (line) { return console.log(line); });
            // We either have an exception serialised as JSON (if the controller threw an exception,
            // or just some text with errors if the controller returned normally but with the ModelState
            // populated with errors.
            // Handle both these cases...
            var shortMessage = "Error";
            if (formattedJson.match(/"ActionName": "(.*)"/)) {
                var action = formattedJson.match(/"ActionName": "(.*)"/)[1];
                var controller = formattedJson.match(/"ControllerName": "(.*)"/)[1];
                var mainMessage = formattedJson.match(/"Message": "((.|\r\n)*)"/)[1].split(/",/)[0];
                shortMessage = "Error from " + controller + "." + action + ": " + mainMessage.substr(0, 50) + "...";
            }
            errorWindow.title(shortMessage);
            errorWindow.content("<pre><code>" + formattedJson + "</code></pre>");
            this.activeWindowCount++;
            this.lastPopupPosition = { left: errorWindow.wrapper[0].offsetLeft, top: errorWindow.wrapper[0].offsetTop };
        };
        ErrorManager.prototype.dataSourceErrorHandler = function (e) {
            var message;
            if (e.errors) {
                message = "Errors:\n";
                $.each(e.errors, function (key, value) {
                    if ('errors' in value) {
                        $.each(value.errors, function () {
                            message += this + "\n";
                        });
                    }
                });
            }
            if (e.xhr) {
                message = e.xhr.responseText;
            }
            // Show the popup error window...
            Errors.manager.openErrorWindow(message);
            // Write message to message area...
            // OutputMessageServiceManager.outputMessageService.writeMessageLine(message, MessageType.Error, false);
            // The sender is the Kendo DataSource which had the error - we need to cancel and changes so the grid is returned to its pre-error state.
            e.sender.cancelChanges();
        };
        return ErrorManager;
    }());
    Errors.ErrorManager = ErrorManager;
    Errors.manager = new ErrorManager();
})(Errors || (Errors = {}));
//# sourceMappingURL=LinkConsultancyAscott.errors.js.map