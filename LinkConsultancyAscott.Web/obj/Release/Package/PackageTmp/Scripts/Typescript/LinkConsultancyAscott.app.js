"use strict";
var LinkConsultancyAscott;
(function (LinkConsultancyAscott) {
    var Application = /** @class */ (function () {
        function Application() {
            // Array of Grids to be updated
            this.updateGridList = ["AclListGrid", "AclHistoryGrid", "CataleyaAclGrid"];
        }
        Application.prototype.setActiveNavLink = function (linkName) {
        };
        Application.prototype.refreshGrids = function () {
            for (var _i = 0, _a = this.updateGridList; _i < _a.length; _i++) {
                var gridName = _a[_i];
                infoLog("Refreshing Grid [" + gridName + "]");
                LinkConsultancyAscott.application.refreshAjaxGrid("#" + gridName);
            }
        };
        Application.prototype.refreshAjaxGrid = function (gridName) {
            // Ensure the incoming grid name is a valid jQuery Identifier
            if (gridName.length > 0) {
                if (gridName[0] !== "#") {
                    gridName = "#" + gridName;
                }
            }
            // Also need to update any other dependent grids
            var grid = $(gridName).data('kendoGrid');
            if (grid) {
                var dataSource = grid.dataSource;
                dataSource.read();
            }
        };
        Application.prototype.loadDataUrl = function (element) {
            var jelement = $(element);
            var url = jelement.data("request-url") + "?_=" + new Date().getTime();
            var anchor = jelement.data("anchor").toString();
            this.loadUrl(url, null);
            window.location.hash = anchor;
        };
        Application.prototype.loadUrl = function (url, onLoaded) {
            $('#mainBodyContainer').hide("fast", function () {
                $('#mainBodyContainer').load(url, '', function () {
                    $('#mainBodyContainer').show("fast", function () {
                        $('#load').fadeOut("fast");
                        // After all this is complete call any spec
                        if (onLoaded) {
                            onLoaded();
                        }
                    });
                });
            });
            $('#load').remove();
            $('#wrapper').append('<span id="load">LOADING...</span>');
            $('#load').fadeIn('normal');
        };
        Application.prototype.listViewModeItemTemplate = function (e) {
            if (e.field === "all") {
                //handle the check-all checkbox template
                return "<li class='k-item'><label class='k-label'><input class='k-check-all' type='checkbox' value='Select All'>Select All</label></li>";
            }
            else {
                //handle the other checkboxes
                return "<li class='k-item'><label class='k-label'><input type='checkbox' value='#=RequirementType.Name#'>#=RequirementType.Name#</label></li>";
            }
        };
        Application.prototype.gridResizeHandler = function (gridName) {
            infoLog("Entered gridResizeHandler");
            var gridElement = $(gridName);
            function resizeGrid() {
                var windowHeight = $(window).height();
                var headerHeight = $('#header-wrapper').outerHeight(true);
                var contentHeight = windowHeight - headerHeight - 80;
                gridElement.css("height", contentHeight + 'px');
                var grid = gridElement.data("kendoGrid");
                if (grid) {
                    grid.resize();
                }
            }
            $(window).resize(function () {
                resizeGrid();
            });
            resizeGrid();
        };
        Application.prototype.onUploadClick = function (e) {
            infoLog("onUploadClick");
            var uploadWindow = $("#UploadIpAddressFileWindow").data("kendoWindow");
            uploadWindow.refresh(null);
            uploadWindow.open();
            uploadWindow.center();
        };
        Application.prototype.onIpAddressFileSelect = function (e) {
            // Array with information about the uploaded files
            var files = e.files;
            // Check the extension of each file and abort the upload if it is not .jpg
            $.each(files, function (index, file) {
                if (!(file.extension.toLowerCase() === ".txt")) {
                    alert("Only .txt files can be uploaded");
                    e.preventDefault();
                    return false;
                }
                else {
                    return false;
                }
            });
        };
        Application.prototype.onIpAddressFileUpload = function (e) {
            var listType = $("#ListType").val();
            if (listType) {
                e.data = {
                    ListType: listType
                };
            }
            else {
                e.preventDefault();
            }
        };
        Application.prototype.onIpAddressFileCancel = function (e) {
            infoLog("Entered onIpAddressFileCancel");
        };
        Application.prototype.onIpAddressFileClear = function (e) {
            infoLog("Entered onIpAddressFileClear");
        };
        Application.prototype.onIpAddressFileComplete = function (e) {
            infoLog("Entered onIpAddressFileComplete");
            // Close the window and redraw the grid
            var uploadWindow = $("#UploadIpAddressFileWindow").data("kendoWindow");
            refreshAjaxGrid("#AclListGrid");
            uploadWindow.close();
        };
        Application.prototype.onIpAddressFileError = function (e) {
            infoLog("Entered onIpAddressFileError");
        };
        Application.prototype.onIpAddressFileProgress = function (e) {
            infoLog("Entered onIpAddressFileProgress");
        };
        Application.prototype.onIpAddressFileRemove = function (e) {
            infoLog("Entered onIpAddressFileRemove");
        };
        Application.prototype.onIpAddressFileSuccess = function (e) {
            infoLog("Entered onIpAddressFileSuccess");
        };
        return Application;
    }());
    LinkConsultancyAscott.Application = Application;
    // Instantiate an instance of the class
    LinkConsultancyAscott.application = new Application();
})(LinkConsultancyAscott || (LinkConsultancyAscott = {}));
//# sourceMappingURL=LinkConsultancyAscott.app.js.map