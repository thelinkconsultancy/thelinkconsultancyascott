"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var LinkConsultancyAscott;
(function (LinkConsultancyAscott) {
    var Base = /** @class */ (function () {
        function Base() {
        }
        Base.prototype.refreshAjaxGrid = function (gridName) {
            // Ensure the incoming grid name is a valid jQuery Identifier
            if (gridName.length > 0) {
                if (gridName[0] !== "#") {
                    gridName = "#" + gridName;
                }
            }
            // Also need to update any other dependent grids
            var grid = $(gridName).data('kendoGrid');
            if (grid) {
                var dataSource = grid.dataSource;
                dataSource.read();
            }
        };
        Base.prototype.dataSourceErrorHandler = function (e) {
            if (e.errors) {
                var message = "Errors:\n";
                $.each(e.errors, function (key, value) {
                    if ('errors' in value) {
                        $.each(value.errors, function () {
                            message += this + "\n";
                        });
                    }
                });
                alert(message);
            }
        };
        Base.prototype.onRequestEndRefreshGrid = function (e) {
            if (e.type === "update" || e.type === "destroy" || e.type === "create") {
                e.sender.read();
            }
        };
        return Base;
    }());
    LinkConsultancyAscott.Base = Base;
    var PartialBase = /** @class */ (function (_super) {
        __extends(PartialBase, _super);
        function PartialBase() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        // Ideally the next 3 methods would be abstract but Typescript doesn't support that yet
        PartialBase.prototype.loadPartial = function () { };
        PartialBase.prototype.onPartialLoaded = function (response, status, xhr) { };
        PartialBase.prototype.deleteKendoControls = function () { };
        return PartialBase;
    }(Base));
    LinkConsultancyAscott.PartialBase = PartialBase;
})(LinkConsultancyAscott || (LinkConsultancyAscott = {}));
//# sourceMappingURL=LinkConsultancyAscott.partial.base.js.map