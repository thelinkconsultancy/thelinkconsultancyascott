/// <reference path="../../typings/kendo/kendo.all.d.ts"/>
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var AdministrationUser;
(function (AdministrationUser) {
    var AdministrationUserApplication = /** @class */ (function (_super) {
        __extends(AdministrationUserApplication, _super);
        function AdministrationUserApplication() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.onUserGridEdit = function (e) {
                // Change the appearance of the default buttons
                // setUpdateAndCancelButtons();
            };
            return _this;
        }
        /**
         * Called when the user grid is data bound, and sets the icon of the custom toolbar button(s).
         * @param {any} e The event args.
         */
        AdministrationUserApplication.prototype.onAdministrationUserRowBound = function (e) {
            // Customise the toolbar button icon!
            var span = $(".k-grid-administrationUserClearUserCacheButton").find("span");
            span.append("<i class='far fa-users'></i>");
            span.append("<i class='far fa-refresh text-danger'></i>");
        };
        /**
         * Calls the server to clear the cache of user identities.
         * Displays a success or failure notification to the user when the call returns.
         */
        AdministrationUserApplication.prototype.onClearUserCache = function () {
            var ajaxUrl = appRootUrl + "/Administration/AdministrationUser/ClearUserCache";
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                url: ajaxUrl,
                success: function (data) {
                    // Tell user it worked! :-)
                    // OutputMessageServiceManager.outputMessageService.writeMessageLine(data, MessageType.Success, true);
                },
                error: function (jqXhr, error, errorThrown) {
                    // Tell user it didn't work! :-(
                    //  OutputMessageServiceManager.outputMessageService.writeMessageLine(jqXhr.responseText, MessageType.Error);
                }
            });
        };
        return AdministrationUserApplication;
    }(AdministrationBase
        .AdministrationBaseApplication));
    AdministrationUser.AdministrationUserApplication = AdministrationUserApplication;
    // Instantiate an instance of the class
    AdministrationUser.administrationUserApplication = new AdministrationUserApplication();
})(AdministrationUser || (AdministrationUser = {}));
//# sourceMappingURL=AdministrationUser.js.map