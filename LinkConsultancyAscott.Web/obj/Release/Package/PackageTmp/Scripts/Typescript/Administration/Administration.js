/// <reference path="../RootUrlHelper.ts"/>
/// <reference path="../LinkConsultancyAscott.common.ts"/>
/// <reference path="AdministrationBase.ts"/>
/// <reference path="../../typings/jquery/jquery.d.ts"/>
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Administration;
(function (Administration) {
    var AdministrationApplication = /** @class */ (function (_super) {
        __extends(AdministrationApplication, _super);
        function AdministrationApplication() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        AdministrationApplication.prototype.onAdministrationTabStripShow = function (e) {
            infoLog("Entered administrationApplication.onAdministrationTabStripShow: " + $(e.item).find("> .k-link").text());
            infoLog("Exited  administrationApplication.onAdministrationTabStripShow: " + $(e.item).find("> .k-link").text());
        };
        AdministrationApplication.prototype.onAdministrationTabStripSelect = function (e) {
            infoLog("Entered administrationApplication.onAdministrationTabStripSelect: " + $(e.item).find("> .k-link").text());
            infoLog("Exited  administrationApplication.onAdministrationTabStripSelect: " + $(e.item).find("> .k-link").text());
        };
        AdministrationApplication.prototype.onAdministrationTabStripActivate = function (e) {
            var tabName = $(e.item).find("> .k-link").text();
            infoLog("Entered administrationApplication.onAdministrationTabStripActivate: " + tabName);
            // Update any busy grids
            if (tabName === "Site Activity Log") {
                Administration.administrationApplication.refreshAjaxGrid("SiteActivityLogGrid");
            }
            else if (tabName === "Logging") {
                Administration.administrationApplication.refreshAjaxGrid("LoggingGrid");
            }
            else if (tabName === "Audit Log") {
                Administration.administrationApplication.refreshAjaxGrid("AuditLogGrid");
            }
            infoLog("Exited  administrationApplication.onAdministrationTabStripActivate: " + tabName);
        };
        AdministrationApplication.prototype.onAdministrationTabStripContentLoad = function (e) {
            infoLog("Entered administrationApplication.onAdministrationTabStripContentLoad: " + $(e.item).find("> .k-link").text());
            infoLog("Exited  administrationApplication.onAdministrationTabStripContentLoad: " + $(e.item).find("> .k-link").text());
        };
        AdministrationApplication.prototype.onAdministrationTabStripError = function (e) {
            errorLog("Entered administrationApplication.onAdministrationTabStripError " + e.xhr.statusText + " " + e.xhr.status);
            errorLog("Exited  administrationApplication.onAdministrationTabStripError " + e.xhr.statusText + " " + e.xhr.status);
        };
        AdministrationApplication.prototype.getColourFromState = function (state) {
            if (state === "Add") {
                return "#009900";
            }
            else if (state === "Modified") {
                return "#FF9900";
            }
            else {
                return "#FF0000";
            }
        };
        AdministrationApplication.prototype.getTimeSpan = function (timeSpan) {
            return Number(Number(timeSpan.TotalSeconds).toFixed(2));
        };
        AdministrationApplication.prototype.getParentProjectModel = function () {
            var src = event.srcElement;
            var row = $(src).closest("tr");
            var gridObj = $(event.srcElement).closest("[data-role=grid]");
            var grid = gridObj.data("kendoGrid");
            var dataItem = grid.dataItem(row);
            //where the OrderID is the model ID
            return { projectModelId: dataItem.Id };
        };
        AdministrationApplication.prototype.buildFilters = function (dataItems) {
            var filters = [];
            var length = dataItems.length;
            var name;
            var idx = 0;
            for (; idx < length; idx++) {
                name = dataItems[idx].Name;
                filters.push({
                    field: "ProjectModels",
                    operator: "contains",
                    value: name
                });
            }
            return filters;
        };
        AdministrationApplication.prototype.onAdminProjectModelDropDownListSelect = function (e) {
            var filters = Administration.administrationApplication.buildFilters([e.sender.dataItem()]);
            var rolesEditor = $("#UserRoles").data("kendoMultiSelect");
            rolesEditor.enable(true);
            var rolesEditorDataSource = rolesEditor.dataSource;
            rolesEditorDataSource.filter(filters);
        };
        AdministrationApplication.prototype.onAdminProjectModelDropDownListChange = function (e) {
            var filters = Administration.administrationApplication.buildFilters([e.sender.dataItem()]);
            var rolesEditor = $("#UserRoles").data("kendoMultiSelect");
            rolesEditor.enable(true);
            var rolesEditorDataSource = rolesEditor.dataSource;
            rolesEditorDataSource.filter(filters);
        };
        AdministrationApplication.prototype.onAdminProjectModelDropDownListDataBound = function (e) {
            var filters = Administration.administrationApplication.buildFilters([e.sender.dataItem()]);
            var rolesEditor = $("#UserRoles").data("kendoMultiSelect");
            rolesEditor.enable(true);
            var rolesEditorDataSource = rolesEditor.dataSource;
            rolesEditorDataSource.filter(filters);
        };
        AdministrationApplication.prototype.convertLevel = function (level) {
            var retIcon = "<i class='far fa-question fa-lg text-warning' title='Unknown'></i>";
            if (level) {
                switch (level) {
                    case 1:
                        retIcon = "<i class='far fa-exclamation fa-lg text-danger' title='1'></i>";
                        break;
                    case 2:
                        retIcon = "<i class='far fa-exclamation fa-lg text-danger' title='2'></i>";
                        break;
                    case 3:
                        retIcon = "<i class='far fa-exclamation fa-lg text-danger' title='3'></i>";
                        break;
                    case 4:
                        retIcon = "<i class='far fa-check fa-lg text-success' title='4'></i>";
                        break;
                }
            }
            return retIcon;
        };
        AdministrationApplication.prototype.setWebsitePageConfigGridPopupDimensions = function () {
            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(750).height(700).data("kendoWindow").center().open().center();
                $(".k-edit-form-container").width("100%");
            }, 100);
        };
        AdministrationApplication.prototype.setRequirementEmailGridPopupDimensions = function () {
            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(750).height(700).data("kendoWindow").center().open().center();
                $(".k-edit-form-container").width("100%");
            }, 100);
        };
        AdministrationApplication.prototype.onWebsitePageConfigGridDataBound = function (arg) {
            var grid = '#' + arg.sender.element[0].id;
            //Selects all edit buttons
            $(grid + " tbody tr .k-grid-RevertToDefault").each(function () {
                var currentDataItem = $(grid).data("kendoGrid").dataItem($(this).closest("tr"));
                //Check in the current dataItem if the row is editable
                if (currentDataItem.IsDefault === true) {
                    $(this).remove();
                }
            });
        };
        AdministrationApplication.prototype.onRequestEndRefreshGrid = function (e) {
            _super.prototype.onRequestEndRefreshGrid.call(this, e);
            // Refresh dependent grids
            if (e.type === "update" || e.type === "destroy" || e.type === "create") {
                // Refresh all admin grids with drop downs
                Administration.administrationApplication.refreshAjaxGrid("UserGrid");
                Administration.administrationApplication.refreshAjaxGrid("ContactGrid");
                Administration.administrationApplication.refreshAjaxGrid("DestinationGrid");
                Administration.administrationApplication.refreshAjaxGrid("RequirementGrid");
                Administration.administrationApplication.refreshAjaxGrid("RequirementEmailGrid");
            }
        };
        AdministrationApplication.prototype.onDestinationRequestEnd = function (e) {
            infoLog("Entered administrationApplication.onDestinationRequestEnd");
            if (e.type === "update" && !e.response.Errors) {
                var dataItem = e.response.Data[0];
                if (dataItem) {
                    if (dataItem.AlertAffectedUsers === true) {
                        // Display a popup with a grid of future trips to the updated country
                        // Send an email on Ok button click
                        if (dataItem.DestinationId) {
                            // This is a destination region
                            Administration.administrationApplication.destinationRegionUpdatedPopup(dataItem.DestinationId, dataItem.Id);
                        }
                        else {
                            // This is a destination
                            Administration.administrationApplication.destinationUpdatedPopup(dataItem.Id);
                        }
                    }
                }
            }
            infoLog("Exited  administrationApplication.onDestinationRequestEnd");
        };
        AdministrationApplication.prototype.fileDialogLoader = function () {
            $('#FileUploadWindow').html('<div style="text-align: center; position: absolute; top: 40%; left: 0px; width: 100%; height: 1px; overflow: visible; display: block;"><div style="margin-left: -125px; position: absolute; top: -35px; left: 50%; width: 250px; height: 70px;"><img src="/Content/kendo/2018.1.117/BlueOpal/loading_2x.gif" /></div></div>');
        };
        AdministrationApplication.prototype.onFileUploadClick = function (e) {
            var parentEmailId = +($(e.sender.element[0]).attr("parentemailid"));
            var kendoWindow = $("#FileUploadWindow_" + parentEmailId);
            var window = kendoWindow.data("kendoWindow");
            // Redraw the window
            window.center().open().refresh({
                url: "/Administration/RequirementEmailAttachmentManagement/UploadFiles?parentEmailId=" + parentEmailId,
                type: "GET",
                iframe: true
            });
        };
        AdministrationApplication.prototype.onFileSelect = function (e) {
            // Array with information about the uploaded files
            /* var files = e.files;
 
             // Check the extension of each file and abort the upload if it is not .jpg
             $.each(files, function (item) {
                 if (!(this.extension.toLowerCase() === ".jpg" ||
                     this.extension.toLowerCase() === ".bmp" ||
                     this.extension.toLowerCase() === ".gif" ||
                     this.extension.toLowerCase() === ".jpeg" ||
                     this.extension.toLowerCase() === ".txt" ||
                     this.extension.toLowerCase() === ".doc" ||
                     this.extension.toLowerCase() === ".docx" ||
                     this.extension.toLowerCase() === ".xls" ||
                     this.extension.toLowerCase() === ".xlsx" ||
                     this.extension.toLowerCase() === ".pdf" ||
                     this.extension.toLowerCase() === ".png")) {
                     alert("Only .gif, .jpg, .jpeg, .png, .txt, .doc, .docx, .xls, .xlsx and .pdf files can be uploaded");
                     e.preventDefault();
                     return false;
                 } else {
                     return false;
                 }
             });*/
            return false;
        };
        AdministrationApplication.prototype.destinationUpdatedPopup = function (destinationId) {
            var id = destinationId.toString();
            Administration.administrationApplication.displayDestinationUpdatedPopup(id);
        };
        AdministrationApplication.prototype.destinationRegionUpdatedPopup = function (destinationId, destinationRegionId) {
            var id = destinationId.toString() + "-" + destinationRegionId.toString();
            Administration.administrationApplication.displayDestinationUpdatedPopup(id);
        };
        AdministrationApplication.prototype.displayDestinationUpdatedPopup = function (id) {
            var wnd = $("#DestinationUpdatedPopupForm").data("kendoWindow");
            wnd.refresh({
                url: "/Administration/DestinationManagement/DestinationUpdated/" + id
            });
            wnd.center().open();
        };
        AdministrationApplication.prototype.gridAdministrationResizeHandler = function (gridName) {
            infoLog("Entered Admin.gridResizeHandler");
            var gridElement = $(gridName);
            function resizeGrid() {
                var windowHeight = $(window).height();
                var headerHeight = $('#header-wrapper').outerHeight(true);
                var contentHeight = windowHeight - headerHeight - 80;
                gridElement.css("height", contentHeight + 'px');
                var grid = gridElement.data("kendoGrid");
                if (grid) {
                    grid.resize();
                }
            }
            $(window).resize(function () {
                resizeGrid();
            });
            resizeGrid();
        };
        return AdministrationApplication;
    }(AdministrationBase.AdministrationBaseApplication));
    Administration.AdministrationApplication = AdministrationApplication;
    // Instantiate an instance of the class
    Administration.administrationApplication = new AdministrationApplication();
})(Administration || (Administration = {}));
//# sourceMappingURL=Administration.js.map