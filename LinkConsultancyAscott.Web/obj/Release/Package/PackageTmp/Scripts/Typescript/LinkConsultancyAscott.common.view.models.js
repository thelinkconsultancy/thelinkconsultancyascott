"use strict";
var CommonViewModels;
(function (CommonViewModels) {
    var ZippedDataPackType;
    (function (ZippedDataPackType) {
        ZippedDataPackType[ZippedDataPackType["CustomerRidDataPack"] = 1] = "CustomerRidDataPack";
        ZippedDataPackType[ZippedDataPackType["SupplierResponseZippedDataPack"] = 2] = "SupplierResponseZippedDataPack";
        ZippedDataPackType[ZippedDataPackType["WindChillPackZippedDataPack"] = 3] = "WindChillPackZippedDataPack";
    })(ZippedDataPackType = CommonViewModels.ZippedDataPackType || (CommonViewModels.ZippedDataPackType = {}));
    var DataPackStatusType;
    (function (DataPackStatusType) {
        DataPackStatusType[DataPackStatusType["DataPackUploaded"] = 1] = "DataPackUploaded";
        DataPackStatusType[DataPackStatusType["RidReviewOpen"] = 2] = "RidReviewOpen";
        DataPackStatusType[DataPackStatusType["RidReviewClosed"] = 3] = "RidReviewClosed";
        DataPackStatusType[DataPackStatusType["RidWithSupplier"] = 4] = "RidWithSupplier";
        DataPackStatusType[DataPackStatusType["DataPackClosed"] = 5] = "DataPackClosed";
    })(DataPackStatusType = CommonViewModels.DataPackStatusType || (CommonViewModels.DataPackStatusType = {}));
    var CommentItemType;
    (function (CommentItemType) {
        CommentItemType[CommentItemType["DataPack"] = 1] = "DataPack";
        CommentItemType[CommentItemType["DocumentationType"] = 2] = "DocumentationType";
        CommentItemType[CommentItemType["Document"] = 3] = "Document";
    })(CommentItemType = CommonViewModels.CommentItemType || (CommonViewModels.CommentItemType = {}));
    var UploadResponseType;
    (function (UploadResponseType) {
        UploadResponseType[UploadResponseType["CustomerResponse"] = 1] = "CustomerResponse";
        UploadResponseType[UploadResponseType["SupplierResponse"] = 2] = "SupplierResponse";
    })(UploadResponseType = CommonViewModels.UploadResponseType || (CommonViewModels.UploadResponseType = {}));
    var FilterListViewModel = /** @class */ (function () {
        function FilterListViewModel() {
        }
        return FilterListViewModel;
    }());
    CommonViewModels.FilterListViewModel = FilterListViewModel;
    var NotificationResponse = /** @class */ (function () {
        function NotificationResponse() {
        }
        return NotificationResponse;
    }());
    CommonViewModels.NotificationResponse = NotificationResponse;
    var NotificationResponseWithModel = /** @class */ (function () {
        function NotificationResponseWithModel() {
        }
        return NotificationResponseWithModel;
    }());
    CommonViewModels.NotificationResponseWithModel = NotificationResponseWithModel;
})(CommonViewModels || (CommonViewModels = {}));
//# sourceMappingURL=LinkConsultancyAscott.common.view.models.js.map