"use strict";
var ReportViewer;
(function (ReportViewer) {
    var ReportViewerApplication = /** @class */ (function () {
        function ReportViewerApplication() {
        }
        ReportViewerApplication.prototype.displayCustomerRidDataPack = function (dataPackId) {
            var href = "/ReportViewer/ViewerFull?reportName=CustomerRidDataPackReport.trdp&dataPackId=" + dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        ReportViewerApplication.prototype.displayDataPackSupplierResponseReport = function (dataPackId) {
            var href = "/ReportViewer/ViewerFull?reportName=DataPackSupplierResponseReport.trdp&dataPackId=" + dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        ReportViewerApplication.prototype.displayDataPackWindchillReport = function (dataPackId) {
            var href = "/ReportViewer/ViewerFull?reportName=DataPackWindchillReport.trdp&dataPackId=" + dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        ReportViewerApplication.prototype.displayDataPackReviewSummaryReport = function (dataPackId) {
            var href = "/ReportViewer/ViewerFull?reportName=DataPackReviewSummary.trdp&dataPackId=" + dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        ReportViewerApplication.prototype.displayDataPackRidsGridSummaryReport = function (dataPackId) {
            var href = "/ReportViewer/ViewerFull?reportName=DataPackRidsGridSummary.trdp&dataPackId=" + dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        ReportViewerApplication.prototype.displayDataPackRidsPackReportFlatReport = function (dataPackId) {
            var href = "/ReportViewer/ViewerFull?reportName=DataPackRidsPackReportFlat.trdp&dataPackId=" + dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        ReportViewerApplication.prototype.displayDataPackRidsPackReportHierarchicalReport = function (dataPackId) {
            var href = "/ReportViewer/ViewerFull?reportName=DataPackRidsPackReportHierarchical.trdp&dataPackId=" + dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        ReportViewerApplication.prototype.displayDocumentRidRoleLinkRidsReport = function (documentRidRoleLinkId) {
            var href = "/ReportViewer/ViewerFull?reportName=DocumentRidRoleLinkRidsReport.trdp&documentRidRoleLinkId=" + documentRidRoleLinkId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        };
        return ReportViewerApplication;
    }());
    ReportViewer.ReportViewerApplication = ReportViewerApplication;
    // Instantiate an instance of the class
    ReportViewer.application = new ReportViewerApplication();
})(ReportViewer || (ReportViewer = {}));
//# sourceMappingURL=LinkConsultancyAscott.report.viewer.js.map