"use strict";
module ReportViewer {
    export interface IReportViewerApplication {
        displayDocumentRidRoleLinkRidsReport: (documentRidRoleLinkId: number) => void;

        displayDataPackReviewSummaryReport: (dataPackId: number) => void;
        displayDataPackRidsGridSummaryReport: (dataPackId: number) => void;
        displayDataPackRidsPackReportFlatReport: (dataPackId: number) => void;
        displayDataPackRidsPackReportHierarchicalReport: (dataPackId: number) => void;

        displayCustomerRidDataPack: (dataPackId: number) => void;
        displayDataPackSupplierResponseReport: (dataPackId: number) => void;
        displayDataPackWindchillReport: (dataPackId: number) => void;
    }

    export class ReportViewerApplication implements IReportViewerApplication {
        displayCustomerRidDataPack(dataPackId: number): void {
             var href: string = "/ReportViewer/ViewerFull?reportName=CustomerRidDataPackReport.trdp&dataPackId="+ dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }

        displayDataPackSupplierResponseReport(dataPackId: number): void {
             var href: string = "/ReportViewer/ViewerFull?reportName=DataPackSupplierResponseReport.trdp&dataPackId="+ dataPackId;
             window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }

        displayDataPackWindchillReport(dataPackId: number): void {
             var href: string = "/ReportViewer/ViewerFull?reportName=DataPackWindchillReport.trdp&dataPackId="+ dataPackId;
             window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }

        displayDataPackReviewSummaryReport(dataPackId: number): void {
            var href: string = "/ReportViewer/ViewerFull?reportName=DataPackReviewSummary.trdp&dataPackId="+ dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }

        displayDataPackRidsGridSummaryReport(dataPackId: number): void {
            
            var href: string = "/ReportViewer/ViewerFull?reportName=DataPackRidsGridSummary.trdp&dataPackId="+ dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }

        displayDataPackRidsPackReportFlatReport(dataPackId: number): void {
            var href: string = "/ReportViewer/ViewerFull?reportName=DataPackRidsPackReportFlat.trdp&dataPackId="+ dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }

        displayDataPackRidsPackReportHierarchicalReport(dataPackId: number): void {
            var href: string = "/ReportViewer/ViewerFull?reportName=DataPackRidsPackReportHierarchical.trdp&dataPackId="+ dataPackId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }


        displayDocumentRidRoleLinkRidsReport(documentRidRoleLinkId: number): void {
             var href: string = "/ReportViewer/ViewerFull?reportName=DocumentRidRoleLinkRidsReport.trdp&documentRidRoleLinkId="+ documentRidRoleLinkId;
            window.open(href, '_blank', 'location=yes,height=600,width=700,scrollbars=yes,resizable=yes,status=yes');
        }
    }

    // Instantiate an instance of the class

    export var application: IReportViewerApplication = new ReportViewerApplication(); 
}
