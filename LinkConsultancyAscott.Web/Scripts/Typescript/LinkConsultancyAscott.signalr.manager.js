/// <reference path="LinkConsultancyAscott.signalr.ts"/>
"use strict";
var LinkConsultancyAscottSignalrManager;
(function (LinkConsultancyAscottSignalrManager) {
    var Application = /** @class */ (function () {
        function Application() {
            try {
                infoLog("Entered SignalRInitialisation");
                this.kickOff();
                infoLog("Exited  SignalRInitialisation");
            }
            catch (exception) {
                console.log(exception);
            }
        }
        Application.prototype.kickOff = function () {
            infoLog("Entered SignalRInitialisation.kickOff");
            // Trip Hub
            var tlcHub = $.connection.linkConsultancyAscottHub;
            this.tlcClient = new LinkConsultancyAscottClient();
            tlcHub.client.aclUpdated = this.tlcClient.aclUpdated;
            $.connection.hub.start()
                .done(function () {
                infoLog('Now connected, connection ');
            })
                .fail(function () { infoLog('Could not connect'); });
            infoLog("Exited SignalRInitialisation.kickOff");
        };
        return Application;
    }());
    LinkConsultancyAscottSignalrManager.Application = Application;
    LinkConsultancyAscottSignalrManager.instance = null;
})(LinkConsultancyAscottSignalrManager || (LinkConsultancyAscottSignalrManager = {}));
//# sourceMappingURL=LinkConsultancyAscott.signalr.manager.js.map