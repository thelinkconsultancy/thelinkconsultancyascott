﻿"use strict";
module AdministrationBase {
    export interface IAdministrationBase  extends LinkConsultancyAscott.IPartialBase  {
    }

    export class AdministrationBaseApplication extends LinkConsultancyAscott.PartialBase implements IAdministrationBase{
    }
}