﻿/// <reference path="../RootUrlHelper.ts"/>
/// <reference path="../LinkConsultancyAscott.common.ts"/>
/// <reference path="AdministrationBase.ts"/>
/// <reference path="../../typings/jquery/jquery.d.ts"/>

"use strict";
module Administration {
    export interface IAdministrationApplication extends AdministrationBase.IAdministrationBase  {
        onAdministrationTabStripShow: (e: kendo.ui.TabStripShowEvent) => void;
        onAdministrationTabStripSelect: (e: kendo.ui.TabStripSelectEvent) => void;
        onAdministrationTabStripActivate: (e: kendo.ui.TabStripActivateEvent) => void;
        onAdministrationTabStripContentLoad: (e: kendo.ui.TabStripContentLoadEvent) => void;
        onAdministrationTabStripError: (e: kendo.ui.TabStripErrorEvent) => void;

        getColourFromState: (state: string) => string;

        getTimeSpan: (timeSpan: any) => number;

        buildFilters: (dataItems: any) => any;
        onAdminProjectModelDropDownListSelect: (e: kendo.ui.DropDownListSelectEvent) => any;
        onAdminProjectModelDropDownListChange: (e: kendo.ui.DropDownListChangeEvent) => any;
        onAdminProjectModelDropDownListDataBound: (e: kendo.ui.DropDownListDataBoundEvent) => any;

        convertLevel: (level: number) => string;

        setWebsitePageConfigGridPopupDimensions: () => void;
        onWebsitePageConfigGridDataBound: (arg: any) => void;

        setRequirementEmailGridPopupDimensions: () => void;

        fileDialogLoader: () => void;
        onFileUploadClick: (e: kendo.ui.ButtonClickEvent) => void;

        onFileSelect: (e: kendo.ui.UploadSelectEvent) => boolean;

        onDestinationRequestEnd: (e: kendo.data.DataSourceRequestEndEvent) => void;

        destinationUpdatedPopup: (destinationId: number) => void;

        destinationRegionUpdatedPopup: (destinationId: number, destinationRegionId: number) => void;

        displayDestinationUpdatedPopup: (id: string) => void;

        gridAdministrationResizeHandler: (gridName: string) => void;
    }

    export class AdministrationApplication extends AdministrationBase.AdministrationBaseApplication implements IAdministrationApplication {

        onAdministrationTabStripShow(e: kendo.ui.TabStripShowEvent): void {
            infoLog("Entered administrationApplication.onAdministrationTabStripShow: " + $(e.item).find("> .k-link").text());

            infoLog("Exited  administrationApplication.onAdministrationTabStripShow: " + $(e.item).find("> .k-link").text());
        }

        onAdministrationTabStripSelect(e: kendo.ui.TabStripSelectEvent): void {
            infoLog("Entered administrationApplication.onAdministrationTabStripSelect: " + $(e.item).find("> .k-link").text());
            infoLog("Exited  administrationApplication.onAdministrationTabStripSelect: " + $(e.item).find("> .k-link").text());
        }

        onAdministrationTabStripActivate(e: kendo.ui.TabStripActivateEvent): void {
            var tabName = $(e.item).find("> .k-link").text();
            infoLog("Entered administrationApplication.onAdministrationTabStripActivate: " + tabName);

            // Update any busy grids
            if (tabName === "Site Activity Log") {
                Administration.administrationApplication.refreshAjaxGrid("SiteActivityLogGrid");
            }
            else if (tabName === "Logging") {
                Administration.administrationApplication.refreshAjaxGrid("LoggingGrid");
            }
            else if (tabName === "Audit Log") {
                Administration.administrationApplication.refreshAjaxGrid("AuditLogGrid");
            }
            infoLog("Exited  administrationApplication.onAdministrationTabStripActivate: " + tabName);
        }

        onAdministrationTabStripContentLoad(e: kendo.ui.TabStripContentLoadEvent): void {
            infoLog("Entered administrationApplication.onAdministrationTabStripContentLoad: " + $(e.item).find("> .k-link").text());
            infoLog("Exited  administrationApplication.onAdministrationTabStripContentLoad: " + $(e.item).find("> .k-link").text());
        }

        onAdministrationTabStripError(e: kendo.ui.TabStripErrorEvent): void {
            errorLog("Entered administrationApplication.onAdministrationTabStripError " + e.xhr.statusText + " " + e.xhr.status);

            errorLog("Exited  administrationApplication.onAdministrationTabStripError " + e.xhr.statusText + " " + e.xhr.status);
        }

        getColourFromState(state: string): string {
            if (state === "Add") {
                return "#009900";
            } else if (state === "Modified") {
                return "#FF9900";
            } else {
                return "#FF0000";
            }
        }

        getTimeSpan(timeSpan: any) : number {
            return Number(Number(timeSpan.TotalSeconds).toFixed(2));
        }

        getParentProjectModel(): any {
          var src = event.srcElement;
          var row = $(src).closest("tr");
          var gridObj = $(event.srcElement).closest("[data-role=grid]");

            var grid : kendo.ui.Grid = gridObj.data("kendoGrid");

            var dataItem : any = grid.dataItem(row);

            //where the OrderID is the model ID

            return { projectModelId: dataItem.Id }
        }

        buildFilters(dataItems: any) : any {
            var filters = [];
            var length = dataItems.length;
            var name;
            var idx = 0;

            for (; idx < length; idx++) {
                name = dataItems[idx].Name;

                filters.push({
                    field: "ProjectModels",
                    operator: "contains",
                    value: name
                });
            }
            return filters;
        }

        onAdminProjectModelDropDownListSelect(e: kendo.ui.DropDownListSelectEvent) {
            var filters = Administration.administrationApplication.buildFilters([e.sender.dataItem()]);
            var rolesEditor: kendo.ui.MultiSelect = $("#UserRoles").data("kendoMultiSelect");
            rolesEditor.enable(true);
            var rolesEditorDataSource = rolesEditor.dataSource;
            rolesEditorDataSource.filter(filters);
        }

        onAdminProjectModelDropDownListChange(e: kendo.ui.DropDownListChangeEvent) {
            var filters = Administration.administrationApplication.buildFilters([e.sender.dataItem()]);
            var rolesEditor: kendo.ui.MultiSelect = $("#UserRoles").data("kendoMultiSelect");
            rolesEditor.enable(true);
            var rolesEditorDataSource = rolesEditor.dataSource;
            rolesEditorDataSource.filter(filters);
        }

        onAdminProjectModelDropDownListDataBound(e: kendo.ui.DropDownListDataBoundEvent) {
            var filters = Administration.administrationApplication.buildFilters([e.sender.dataItem()]);
            var rolesEditor: kendo.ui.MultiSelect = $("#UserRoles").data("kendoMultiSelect");
            rolesEditor.enable(true);
            var rolesEditorDataSource = rolesEditor.dataSource;
            rolesEditorDataSource.filter(filters);
        }

        convertLevel(level: number): string {
            var retIcon = "<i class='far fa-question fa-lg text-warning' title='Unknown'></i>";

            if (level) {
                switch (level) {

                    case 1:
                        retIcon = "<i class='far fa-exclamation fa-lg text-danger' title='1'></i>";
                        break;

                    case 2:
                        retIcon = "<i class='far fa-exclamation fa-lg text-danger' title='2'></i>";
                        break;

                    case 3:
                         retIcon = "<i class='far fa-exclamation fa-lg text-danger' title='3'></i>";
                        break;

                    case 4:
                        retIcon = "<i class='far fa-check fa-lg text-success' title='4'></i>";
                        break;

                }
            }
            return retIcon;
        }

        setWebsitePageConfigGridPopupDimensions(): void {
            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(750).height(700).data("kendoWindow").center().open().center();
                $(".k-edit-form-container").width("100%");
            }, 100);
        }

        setRequirementEmailGridPopupDimensions(): void {
            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(750).height(700).data("kendoWindow").center().open().center();
                $(".k-edit-form-container").width("100%");
            }, 100);
        }

        onWebsitePageConfigGridDataBound(arg): void {
            var grid = '#' + arg.sender.element[0].id;

            //Selects all edit buttons
            $(grid + " tbody tr .k-grid-RevertToDefault").each(function() {
                var currentDataItem: any = $(grid).data("kendoGrid").dataItem($(this).closest("tr"));

                //Check in the current dataItem if the row is editable
                if (currentDataItem.IsDefault === true) {
                    $(this).remove();
                }
            });
        }

        onRequestEndRefreshGrid(e: kendo.data.DataSourceRequestEndEvent): void {

            super.onRequestEndRefreshGrid(e);

            // Refresh dependent grids
            if (e.type === "update" || e.type === "destroy" || e.type === "create") {
                // Refresh all admin grids with drop downs
                Administration.administrationApplication.refreshAjaxGrid("UserGrid");
                Administration.administrationApplication.refreshAjaxGrid("ContactGrid");
                Administration.administrationApplication.refreshAjaxGrid("DestinationGrid");
                Administration.administrationApplication.refreshAjaxGrid("RequirementGrid");
                Administration.administrationApplication.refreshAjaxGrid("RequirementEmailGrid");
            }

        }

        onDestinationRequestEnd(e: kendo.data.DataSourceRequestEndEvent): void {
            infoLog("Entered administrationApplication.onDestinationRequestEnd");

            if (e.type === "update" && !e.response.Errors) {
                var dataItem: any = e.response.Data[0];

                if (dataItem) {
                    if (dataItem.AlertAffectedUsers === true) {
                        // Display a popup with a grid of future trips to the updated country
                        // Send an email on Ok button click
                        if (dataItem.DestinationId) {
                            // This is a destination region
                            Administration.administrationApplication.destinationRegionUpdatedPopup(dataItem.DestinationId, dataItem.Id);
                        } else {
                            // This is a destination
                            Administration.administrationApplication.destinationUpdatedPopup(dataItem.Id);
                        }

                    }
                }

            }
            infoLog("Exited  administrationApplication.onDestinationRequestEnd");
        }

        fileDialogLoader() : void {
            $('#FileUploadWindow').html('<div style="text-align: center; position: absolute; top: 40%; left: 0px; width: 100%; height: 1px; overflow: visible; display: block;"><div style="margin-left: -125px; position: absolute; top: -35px; left: 50%; width: 250px; height: 70px;"><img src="/Content/kendo/2018.1.117/BlueOpal/loading_2x.gif" /></div></div>');
        }

        onFileUploadClick(e: kendo.ui.ButtonClickEvent): void {
            var parentEmailId: number = +($(e.sender.element[0]).attr("parentemailid"));

             var kendoWindow: JQuery = $("#FileUploadWindow_" + parentEmailId);
             var window: kendo.ui.Window = kendoWindow.data("kendoWindow");

             // Redraw the window
             window.center().open().refresh({
                  url: "/Administration/RequirementEmailAttachmentManagement/UploadFiles?parentEmailId=" + parentEmailId,
                  type: "GET",
                  iframe: true
             });
        }

        onFileSelect(e : kendo.ui.UploadSelectEvent) : boolean {
            // Array with information about the uploaded files
           /* var files = e.files;

            // Check the extension of each file and abort the upload if it is not .jpg
            $.each(files, function (item) {
                if (!(this.extension.toLowerCase() === ".jpg" ||
                    this.extension.toLowerCase() === ".bmp" ||
                    this.extension.toLowerCase() === ".gif" ||
                    this.extension.toLowerCase() === ".jpeg" ||
                    this.extension.toLowerCase() === ".txt" ||
                    this.extension.toLowerCase() === ".doc" ||
                    this.extension.toLowerCase() === ".docx" ||
                    this.extension.toLowerCase() === ".xls" ||
                    this.extension.toLowerCase() === ".xlsx" ||
                    this.extension.toLowerCase() === ".pdf" ||
                    this.extension.toLowerCase() === ".png")) {
                    alert("Only .gif, .jpg, .jpeg, .png, .txt, .doc, .docx, .xls, .xlsx and .pdf files can be uploaded");
                    e.preventDefault();
                    return false;
                } else {
                    return false;
                }
            });*/

            return false;
        }

        destinationUpdatedPopup(destinationId: number): void {

            var id: string = destinationId.toString();
            Administration.administrationApplication.displayDestinationUpdatedPopup(id);
        }

        destinationRegionUpdatedPopup(destinationId: number, destinationRegionId: number): void {

            var id: string = destinationId.toString() + "-" + destinationRegionId.toString();

            Administration.administrationApplication.displayDestinationUpdatedPopup(id);
        }

        displayDestinationUpdatedPopup(id: string): void {
            var wnd = $("#DestinationUpdatedPopupForm").data("kendoWindow");

            wnd.refresh({
                url: `/Administration/DestinationManagement/DestinationUpdated/${id}`
            });
            wnd.center().open();
        }

        gridAdministrationResizeHandler(gridName: string): void {
          infoLog("Entered Admin.gridResizeHandler");
          var gridElement = $(gridName);

          function resizeGrid() {
            var windowHeight = $(window).height();
            var headerHeight = $('#header-wrapper').outerHeight(true);
            var contentHeight = windowHeight - headerHeight - 80;

            
            gridElement.css("height", contentHeight + 'px');

              var grid = gridElement.data("kendoGrid");
              if (grid) {
                  grid.resize();
              }
            
          }

          $(window).resize(() => {
              resizeGrid();
          });

          resizeGrid();
        }
    }

    // Instantiate an instance of the class
    export var administrationApplication: IAdministrationApplication = new AdministrationApplication();
}