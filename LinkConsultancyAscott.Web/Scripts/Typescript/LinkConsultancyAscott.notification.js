/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/kendo/kendo.all.d.ts"/>
/// <reference path="LinkConsultancyAscott.common.view.models.ts"/>
var LinkConsultancyAscottNotification;
(function (LinkConsultancyAscottNotification) {
    var LinkConsultancyAscottNotificationManager = /** @class */ (function () {
        function LinkConsultancyAscottNotificationManager() {
        }
        LinkConsultancyAscottNotificationManager.prototype.getNotification = function () {
            var element = window.parent.document.getElementById('notification');
            var jelm = $(element);
            var notification = jelm.data("kendoNotification");
            return notification;
        };
        LinkConsultancyAscottNotificationManager.prototype.processNotificationResponse = function (notificationResponse) {
            var notification = LinkConsultancyAscottNotification.manager.getNotification();
            LinkConsultancyAscottNotification.manager.displayNotificationResponse(notification, notificationResponse);
        };
        LinkConsultancyAscottNotificationManager.prototype.displayNotificationResponse = function (notification, notificationResponse) {
            switch (notificationResponse.ResponseStatus) {
                case "Info":
                    this.showInfoNotification(notification, notificationResponse.ResponseMessage);
                    break;
                case "Error":
                    this.showErrorNotification(notification, notificationResponse.ResponseMessage);
                    break;
                case "Warning":
                    this.showWarningNotification(notification, notificationResponse.ResponseMessage);
                    break;
                case "Success":
                    this.showSuccessNotification(notification, notificationResponse.ResponseMessage);
                    break;
                default:
                    this.showErrorNotification(notification, notificationResponse.ResponseMessage);
                    break;
            }
        };
        LinkConsultancyAscottNotificationManager.prototype.showInfo = function (message) {
            var _this = this;
            (function () {
                _this.showInfoNotification(_this.getNotification(), message);
            });
        };
        LinkConsultancyAscottNotificationManager.prototype.showError = function (message) {
            var _this = this;
            (function () {
                _this.showErrorNotification(_this.getNotification(), message);
            });
        };
        LinkConsultancyAscottNotificationManager.prototype.showSuccess = function (message) {
            var _this = this;
            (function () {
                _this.showSuccessNotification(_this.getNotification(), message);
            });
        };
        LinkConsultancyAscottNotificationManager.prototype.showWarning = function (message) {
            var _this = this;
            (function () {
                _this.showWarningNotification(_this.getNotification(), message);
            });
        };
        LinkConsultancyAscottNotificationManager.prototype.showInfoNotification = function (notification, message) {
            notification.info(message);
        };
        LinkConsultancyAscottNotificationManager.prototype.showErrorNotification = function (notification, message) {
            notification.error(message);
        };
        LinkConsultancyAscottNotificationManager.prototype.showSuccessNotification = function (notification, message) {
            notification.success(message);
        };
        LinkConsultancyAscottNotificationManager.prototype.showWarningNotification = function (notification, message) {
            notification.warning(message);
        };
        return LinkConsultancyAscottNotificationManager;
    }());
    LinkConsultancyAscottNotification.LinkConsultancyAscottNotificationManager = LinkConsultancyAscottNotificationManager;
    LinkConsultancyAscottNotification.manager = new LinkConsultancyAscottNotificationManager();
})(LinkConsultancyAscottNotification || (LinkConsultancyAscottNotification = {}));
//# sourceMappingURL=LinkConsultancyAscott.notification.js.map