"use strict";
var LinkConsultancyAscottClient = /** @class */ (function () {
    function LinkConsultancyAscottClient() {
    }
    LinkConsultancyAscottClient.prototype.aclUpdated = function () {
        infoLog("Entered SignalR: LinkConsultancyAscott.aclUpdated");
        LinkConsultancyAscott.application.refreshGrids();
        infoLog("Exited  SignalR: LinkConsultancyAscott.aclUpdated");
    };
    return LinkConsultancyAscottClient;
}());
//# sourceMappingURL=LinkConsultancyAscott.signalr.js.map