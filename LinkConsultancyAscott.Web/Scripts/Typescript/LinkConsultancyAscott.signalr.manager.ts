﻿/// <reference path="LinkConsultancyAscott.signalr.ts"/>

"use strict";
module LinkConsultancyAscottSignalrManager {
    export interface IApplication {
        tlcClient: ILinkConsultancyAscottClient;
    }

    export class Application implements IApplication {

        tlcClient: ILinkConsultancyAscottClient;

        constructor()
        {
            try {
                infoLog("Entered SignalRInitialisation");
                this.kickOff();
                infoLog("Exited  SignalRInitialisation");
            } catch (exception) {
                console.log(exception);
            }
        }

        kickOff() {
            infoLog("Entered SignalRInitialisation.kickOff");
            // Trip Hub
            var tlcHub = $.connection.linkConsultancyAscottHub;

            this.tlcClient = new LinkConsultancyAscottClient();

            tlcHub.client.aclUpdated = this.tlcClient.aclUpdated;

            $.connection.hub.start()
                .done(() => {
                    infoLog('Now connected, connection ');
                })
                .fail(() => { infoLog('Could not connect'); });

            infoLog("Exited SignalRInitialisation.kickOff");
        }
    }

    export var instance: IApplication =  null;
}
