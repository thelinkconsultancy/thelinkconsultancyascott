﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Log type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The log.
    /// </summary>
    public partial class Log : IDataEntityBase<long>
    {
    }
}
