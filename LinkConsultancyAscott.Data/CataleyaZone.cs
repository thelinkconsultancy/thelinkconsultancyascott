//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LinkConsultancyAscott.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CataleyaZone
    {
        public int ZoneId { get; set; }
        public int NodeId { get; set; }
        public int OperatorId { get; set; }
        public string ZoneName { get; set; }
        public string ZoneIp { get; set; }
        public string ZoneType { get; set; }
        public string ZoneDescription { get; set; }
    
        public virtual CataleyaNode CataleyaNode { get; set; }
    }
}
