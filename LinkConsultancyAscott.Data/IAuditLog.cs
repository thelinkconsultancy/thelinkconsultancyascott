// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuditLog.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IAuditLog type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IAuditLog LookUp. 
    /// </summary>
    public interface IAuditLog : IDataEntityBase<long>
    {
    } 
}
