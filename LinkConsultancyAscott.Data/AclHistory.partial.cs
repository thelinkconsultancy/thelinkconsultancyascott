﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclHistory.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AclHistory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The AclHistory.
    /// </summary>
    public partial class AclHistory : IAclHistory
    {
    }
}
