// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IEmailType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IDocument LookUp. 
    /// </summary>
    public interface IEmailType : ILookUp<int>
    {
    } 
}
