﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuditedEntity.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Interface IAuditedEntity
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    using System;

    /// <summary>
    /// Interface IAuditedEntity
    /// </summary>
    /// <typeparam name="T"> The id Type </typeparam>
    /// <seealso cref="LinkConsultancyAscott.Data.IDataEntityBase{T}" />
    public interface IAuditedEntity<T> : IDataEntityBase<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>The created by.</value>
        string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created at.
        /// </summary>
        /// <value>The created at.</value>
        DateTime CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the last modified by.
        /// </summary>
        /// <value>The last modified by.</value>
        string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the last modified at.
        /// </summary>
        /// <value>The last modified at.</value>
        DateTime? LastModifiedAt { get; set; }

        /// <summary>
        /// Gets or sets the deleted by.
        /// </summary>
        /// <value>The deleted by.</value>
        string DeletedBy { get; set; }

        /// <summary>
        /// Gets or sets the deleted at.
        /// </summary>
        /// <value>The deleted at.</value>
        DateTime? DeletedAt { get; set; }
    }
}
