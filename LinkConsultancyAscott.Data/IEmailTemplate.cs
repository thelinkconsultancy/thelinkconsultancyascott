// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailTemplate.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IEmailTemplate type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IEmailTemplate LookUp. 
    /// </summary>
    public interface IEmailTemplate : ILookUp<int>
    {
    } 
}
