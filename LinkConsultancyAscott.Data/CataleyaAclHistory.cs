//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LinkConsultancyAscott.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CataleyaAclHistory
    {
        public int Id { get; set; }
        public Nullable<int> NodeId { get; set; }
        public Nullable<int> SipInterfaceId { get; set; }
        public string Action { get; set; }
        public string AppType { get; set; }
        public string Transport { get; set; }
        public string SipInterfaceAddressType { get; set; }
        public string RemoteIpAddress { get; set; }
        public string SipInterfaceIpAddress { get; set; }
        public string RemotePort { get; set; }
        public string LocalPort { get; set; }
        public string RemotePrefix { get; set; }
        public Nullable<int> Ver { get; set; }
        public Nullable<System.DateTime> CurrentDate { get; set; }
        public bool isDeleted { get; set; }
        public int BatchNo { get; set; }
        public Nullable<int> CataleyaSecurityAclId { get; set; }
    }
}
