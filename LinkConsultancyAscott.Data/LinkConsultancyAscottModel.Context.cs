﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LinkConsultancyAscott.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class LinkConsultancyAscottEntities : DbContext
    {
        public LinkConsultancyAscottEntities()
            : base("name=LinkConsultancyAscottEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<EmailTemplateRole> EmailTemplateRoles { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual DbSet<EmailType> EmailTypes { get; set; }
        public virtual DbSet<AuditLog> AuditLogs { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<SiteActivityLog> SiteActivityLogs { get; set; }
        public virtual DbSet<vw_AuditLog> vw_AuditLog { get; set; }
        public virtual DbSet<AclEntityType> AclEntityTypes { get; set; }
        public virtual DbSet<AclListType> AclListTypes { get; set; }
        public virtual DbSet<AclSourceType> AclSourceTypes { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<vw_UsersInRoles> vw_UsersInRoles { get; set; }
        public virtual DbSet<AclHistory> AclHistories { get; set; }
        public virtual DbSet<AclHistoryType> AclHistoryTypes { get; set; }
        public virtual DbSet<Acl> Acls { get; set; }
        public virtual DbSet<InterfaceType> InterfaceTypes { get; set; }
        public virtual DbSet<C__RefactorLog> C__RefactorLog { get; set; }
        public virtual DbSet<vw_AclHistory> vw_AclHistory { get; set; }
        public virtual DbSet<vw_Acls> vw_Acls { get; set; }
        public virtual DbSet<AppConfig> AppConfigs { get; set; }
        public virtual DbSet<PartitionLabel> PartitionLabels { get; set; }
        public virtual DbSet<CataleyaInformation> CataleyaInformations { get; set; }
        public virtual DbSet<IpDataDate> IpDataDates { get; set; }
        public virtual DbSet<IpErrorLog> IpErrorLogs { get; set; }
        public virtual DbSet<CataleyaNode> CataleyaNodes { get; set; }
        public virtual DbSet<CataleyaZone> CataleyaZones { get; set; }
        public virtual DbSet<CataleyaSipInterface> CataleyaSipInterfaces { get; set; }
        public virtual DbSet<CataleyaSourceList> CataleyaSourceLists { get; set; }
        public virtual DbSet<CataleyaAclHistory> CataleyaAclHistories { get; set; }
        public virtual DbSet<CataleyaSubnet> CataleyaSubnets { get; set; }
        public virtual DbSet<IPData> IPDatas { get; set; }
    
        public virtual ObjectResult<USP_GetCataleyaAclHistory_Result> USP_GetCataleyaAclHistory()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<USP_GetCataleyaAclHistory_Result>("USP_GetCataleyaAclHistory");
        }
    }
}
