﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinkConsultancyAscottEntities.Constructor.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the The Link Consultancy - Ascott Diary Management Entities type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// Class LinkConsultancyAscottEntities.
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    public partial class LinkConsultancyAscottEntities
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LinkConsultancyAscottEntities"/> class.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        public LinkConsultancyAscottEntities(string connectionString)
            : base(connectionString)
        {
        }
    }
}
