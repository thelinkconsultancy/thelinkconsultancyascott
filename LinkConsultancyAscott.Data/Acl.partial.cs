﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Acl.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Acl type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The Acl Type.
    /// </summary>
    public partial class Acl : IAcl
    {
    }
}
