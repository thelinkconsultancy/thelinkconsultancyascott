/***************************************
*** Static data management script ***
***************************************/
-- This script will manage the static data from
-- your Team Database project for [dbo].[aspnet_Roles].

PRINT 'Updating static data table [dbo].[aspnet_Roles]'

-- Set to your region's date format to ensure dates are updated correctly
SET DATEFORMAT dmy

-- Turn off affected rows being returned
SET NOCOUNT ON

-- Change this to 1 to delete missing records in the target
-- WARNING: Setting this to 1 can cause damage to your database
-- and cause failed deployment if there are any rows referencing
-- a record which has been deleted.
DECLARE @DeleteMissingRecords BIT
SET @DeleteMissingRecords = 0 

-- 1: Define table variable
DECLARE @tblTempTable TABLE (
[ApplicationId] uniqueidentifier,
[RoleId] uniqueidentifier,
[RoleName] nvarchar(256),
[LoweredRoleName] nvarchar(256),
[Description] nvarchar(256)
)

-- 2: Populate the table variable with data
-- This is where you manage your data in source control. You
-- can add and modify entries, but because of potential foreign
-- key contraint violations this script will not delete any
-- removed entries. If you remove an entry then it will no longer
-- be added to new databases based on your schema, but the entry
-- will not be deleted from databases in which the value already exists.
INSERT INTO @tblTempTable ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description]) VALUES ('4116e90d-22d9-42ff-8446-23315a1fec3f', '613b6825-7309-4c97-888c-10210394b07a', 'Administrators', 'administrators', NULL)
INSERT INTO @tblTempTable ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description]) VALUES ('4116e90d-22d9-42ff-8446-23315a1fec3f', '1b74ce92-63dd-4e77-85c8-1e788af3f50a', 'Editors', 'editors', NULL)
INSERT INTO @tblTempTable ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description]) VALUES ('4116e90d-22d9-42ff-8446-23315a1fec3f', '39e3e17b-99de-4175-9d3c-3359a8a028d3', 'Users', 'users', NULL)


-- 3: Update existing records that have changed, add missing and delete missing in source
-- SET IDENTITY_INSERT [dbo].[aspnet_Roles] ON -- PD -- There is no identity field in the table
MERGE INTO [dbo].[aspnet_Roles] AS T
USING @tblTempTable AS S
  ON T.ApplicationId = S.ApplicationId
WHEN MATCHED
AND (T.RoleId <> S.RoleId
OR T.RoleName <> S.RoleName
OR T.LoweredRoleName <> S.LoweredRoleName
OR T.Description <> S.Description
) THEN
UPDATE SET T.RoleId = S.RoleId,
T.RoleName = S.RoleName,
T.LoweredRoleName = S.LoweredRoleName,
T.Description = S.Description
WHEN NOT MATCHED THEN
    INSERT ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description])
    VALUES (S.[ApplicationId], S.[RoleId], S.[RoleName], S.[LoweredRoleName], S.[Description])
WHEN NOT MATCHED BY SOURCE THEN
  DELETE;
-- SET IDENTITY_INSERT [dbo].[aspnet_Roles] OFF -- PD -- There is no identity field in the table


PRINT 'Finished updating static data table '

-- Note: If you are not using the new GDR version of DBPro -- then remove this go command.

GO