﻿CREATE TABLE [Configuration].[EmailTypes]
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Name] [VARCHAR](500) NOT NULL,
	[DisplayOrder] [INT] NOT NULL CONSTRAINT [DF_EmailTypes_DisplayOrder]  DEFAULT ((10)),
	[Visible] [BIT] NOT NULL CONSTRAINT [DF_EmailTypes_Visible]  DEFAULT ((1)),
	[CreatedAt] [DATETIME] NOT NULL CONSTRAINT [DF_EmailTypes_CreatedAt]  DEFAULT (GETDATE()),
	[LastModifiedAt] [DATETIME] NULL,
	[DeletedAt] [DATETIME] NULL,
	[CreatedBy] [VARCHAR](250) NOT NULL CONSTRAINT [DF_EmailTypes_CreatedBy]  DEFAULT ('N/A'),
	[LastModifiedBy] [VARCHAR](250) NULL,
	[DeletedBy] [VARCHAR](250) NULL
	CONSTRAINT [PK_EmailTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
)
