﻿CREATE TABLE [Configuration].[EmailTemplates]
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Name] [VARCHAR](500) NOT NULL,
	[DisplayOrder] [INT] NOT NULL CONSTRAINT [DF_EmailTemplates_DisplayOrder]  DEFAULT ((10)),
	[Visible] [BIT] NOT NULL CONSTRAINT [DF_EmailTemplates_Visible]  DEFAULT ((1)),
	[CreatedAt] [DATETIME] NOT NULL CONSTRAINT [DF_EmailTemplates_CreatedAt]  DEFAULT (GETDATE()),
	[LastModifiedAt] [DATETIME] NULL,
	[DeletedAt] [DATETIME] NULL,
	[CreatedBy] [VARCHAR](250) NOT NULL CONSTRAINT [DF_EmailTemplates_CreatedBy]  DEFAULT ('N/A'),
	[LastModifiedBy] [VARCHAR](250) NULL,
	[DeletedBy] [VARCHAR](250) NULL,
	[EmailTypeId] [INT] NOT NULL,
	[EmailSubject] [VARCHAR](2500) NULL,
	[EmailText] [NVARCHAR](MAX) NULL,
	CONSTRAINT [PK_EmailTemplates] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_EmailTemplates_EmailType] FOREIGN KEY([EmailTypeId]) REFERENCES [Configuration].[EmailTypes] ([Id]) ON DELETE CASCADE
)
