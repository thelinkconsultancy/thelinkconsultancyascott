﻿CREATE VIEW [dbo].[vw_UsersInRoles]
	AS 
	select 
	u.Id AS UserId, u.UserName,
	
	STUFF((SELECT ','+ CONVERT(VARCHAR(128),a.Id) + ':' + a.Name
				   FROM [dbo].AspNetRoles a
				   JOIN [dbo].AspNetUserRoles ar ON ar.RoleId = a.Id
				  WHERE ar.UserId = u.Id
			   GROUP BY a.Id, a.Name
				FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '') as UserRoles,
				act.LastActivityDate,
    sc.ClaimValue AS SipInterfaces,
	sm.ClaimValue AS MediaInterfaces
	from [dbo].[AspNetUsers] u
    outer apply (select UserName, MAX(ActivityTimestamp) as LastActivityDate from [Logging].[SiteActivityLog] Where UserName = u.UserName group by UserName) act
	LEFT OUTER JOIN dbo.AspNetUserClaims sc ON sc.UserId = u.Id AND sc.ClaimType = 'SipInterfaces'
	LEFT OUTER JOIN dbo.AspNetUserClaims sm ON sm.UserId = u.Id AND sm.ClaimType = 'MediaInterfaces';
GO