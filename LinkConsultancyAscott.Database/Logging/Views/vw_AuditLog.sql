﻿CREATE VIEW [Logging].[vw_AuditLog]
AS
SELECT   [Id]   ,
    [EntityId]  ,
    [EntityName] ,
    [Timestamp]  ,
    [UserName] ,
	[EventType] ,
    [EventDescription] ,
    [Source]      
	FROM  [Logging].[AuditLog]
