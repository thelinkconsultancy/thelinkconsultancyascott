﻿CREATE TABLE [Logging].[Log] (
    [Id]        BIGINT            IDENTITY (1, 1) NOT NULL,
    [Date]      DATETIME2       NOT NULL CONSTRAINT [DF_Log_CreatedAt]  DEFAULT (SYSUTCDATETIME()),
    [Thread]    VARCHAR (255)  NOT NULL,
    [Level]     VARCHAR (50)   NOT NULL,
    [Logger]    VARCHAR (255)  NOT NULL,
    [Message]   VARCHAR (4000) NOT NULL,
    [Exception] VARCHAR (2000) NULL,
    CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED ([Id] ASC)
);

