﻿namespace LinkConsultancyAscott.WebCore.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
using System;

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        //[Required]
        [Display(Name = "UserRoles")]
        public string UserRoles { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class CataleyaInfoViewModel
    {
        [Required]
       // [EmailAddress]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }

    //public class PartitionLabelViewModel
    //{
       
    //    [Display(Name = "Partition Id")]
    //    public int PartitionId { get; set; }

    //    [Required]
    //    [Display(Name = "Partition Label")]
    //    public string PartitionLabel { get; set; }

    //    [Display(Name = "Partition Labl Id")]
    //    public string PartitionLabelId { get; set; }

    //    [Display(Name = "Istra Ip")]
    //    public string IstraIp { get; set; }

    //}



    public class UserPartitionMappingViewModel
    {
        [Display(Name = "Cataleya Id")]
        public int CataleyaId { get; set; }

        [Display(Name = "TLC Id")]
        public string TLCId { get; set; }

        [Display(Name = "TLC Role")]
        public int TLCRole { get; set; }

        [Display(Name = "Cataleya UserName")]
        public string CataleyaUserName { get; set; }
        
        [Display(Name = "Partition Label")]
        public int PartitionId { get; set; }

        [Display(Name = "Last Account Verification Date")]
        public DateTime LastAccountVerificationDate { get; set; }

        [Display(Name = "Status")]
        public bool Status { get; set; }


        [Display(Name = "TLC Email")]
        public string TLCEmail { get; set; }
        
    }


    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
