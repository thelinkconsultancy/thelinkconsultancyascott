﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HtmlExtensions.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The html helper extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace System.Web.Mvc
{
    /// <summary>
    /// The html helper extensions.
    /// </summary>
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// The current view name.
        /// </summary>
        /// <param name="html">
        /// The html.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CurrentViewName(this HtmlHelper html)
        {
            return System.IO.Path.GetFileNameWithoutExtension(((RazorView)html.ViewContext.View).ViewPath);
        }
    }
}