﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractApiControllerBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AbstractApiControllerBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System.Web.Http;

    /// <summary>
    /// Class AbstractControllerBase.
    /// </summary>
    public class AbstractApiControllerBase : ApiController
    {
        /// <summary>
        /// The log.
        /// </summary>
        protected static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
