﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicLookUpControllerBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The basic look up controller base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.Linq;

    using System.Web.Mvc;

    using Data;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Service.Infrastructure;
    using Service.Repository;
    using Service.ViewModels;

    /// <summary>
    /// Class BasicLookUpControllerBase.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TM">The type of the TM.</typeparam>
    /// <typeparam name="TVm">The type of the TVM.</typeparam>
    /// <typeparam name="TR">The type of the TR.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.WebCore.Controllers.AbstractControllerBase" />
    [HandleError]
    public abstract class BasicLookUpControllerBase<T, TM, TVm, TR> : AbstractControllerBase
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
                                   where TM : class, IBasicLookUp<T>
                                   where TVm : BasicLookUpViewModelBase<T, TM>, new()
                                   where TR : IBasicLookUpRepository<T, TM>
    {
        /// <summary>
        /// The _repository
        /// </summary>
        protected readonly TR Repository;

        /// <summary>
        /// The _unit of work
        /// </summary>
        protected readonly IUnitOfWork UnitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicLookUpControllerBase{T, TM,TVm,TR}"/> class. 
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        protected BasicLookUpControllerBase(
            TR repository,
            IUnitOfWork unitOfWork)
        {
            this.Repository = repository;
            this.UnitOfWork = unitOfWork;
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        /// <returns>Action Result.</returns>
        public virtual ActionResult List()
        {
            this.ViewBag.Message = this.Message;
            return this.View();
        }

        /// <summary>
        /// Lists the partial.
        /// </summary>
        /// <returns>Action Result.</returns>
        public virtual ActionResult ListPartial()
        {
            this.ViewBag.Message = this.Message;
            return this.PartialView();
        }

        /// <summary>
        /// Gets the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Action Result.</returns>
        public virtual ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var dtoList = this.Repository.GetAll().ToList();

            // TODO Need to look at speeding this up and removing the ToList
            var list = dtoList.Select(dto => new TVm { DataObject = dto }).ToList();

            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the look up for drop down.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Action Result.</returns>
        public ActionResult GetLookUpForDropDown([DataSourceRequest] DataSourceRequest request)
        {
            var dtoList = this.Repository.GetAllVisibleList();
            var list = dtoList.Select(dto => new TVm { DataObject = dto }).ToList();
            return this.Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the vm.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>Returns TVM.</returns>
        public TVm GetVM(TM dto)
        {
            var vm = this.GetNewViewModel();
            vm.DataObject = dto;
            return vm;
        }

        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult Create([DataSourceRequest] DataSourceRequest request, TVm viewModel)
        {
            TVm newViewModel = null;

            // Perform model binding (fill the product properties and validate it).           
            if (this.ServerValidation(viewModel))
            {
                // The model is valid - insert the product. 
                var dto = this.Repository.GetNewDataObject();
                viewModel.UpdateDataObject(dto);
                this.Repository.Add(dto);
                this.UnitOfWork.Commit();

                var newDto = this.Repository.Get(dto.Id);
                newViewModel = this.GetVM(newDto);
            }
            else
            {
                newViewModel = viewModel;
            }

            // Rebind the grid       
            return this.Json(new[] { newViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Updates the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult Update([DataSourceRequest] DataSourceRequest request, TVm viewModel) 
        {
            TVm updatedViewModel = null;

            if (this.ServerValidation(viewModel))
            {
                TM dto = this.Repository.Get(viewModel.Id);

                viewModel.Details = dto;

                viewModel.UpdateDataObject(dto);

                this.Repository.Edit(dto);
                this.UnitOfWork.Commit();

                var updatedDto = this.Repository.Get(dto.Id);
                updatedViewModel = this.GetVM(updatedDto);
            }
            else
            {
                updatedViewModel = viewModel;
            }

            return this.Json(new[] { updatedViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Deletes the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, TVm viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                TM dto = this.Repository.Get(viewModel.Id);

                viewModel.Details = dto;

                viewModel.UpdateDataObject(dto);
      
                this.Repository.Edit(dto);
                this.UnitOfWork.Commit();
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Deletes the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult DestroyDeleted([DataSourceRequest] DataSourceRequest request, TVm viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                var dto = this.Repository.Get(viewModel.Id);

                this.Repository.Delete(dto);
                this.UnitOfWork.Commit();
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            var viewModelList = this.Repository.GetAll().Select(log =>
              new TVm
              {
                  Id = log.Id,
                  CreatedAt = log.CreatedAt,
                  CreatedBy = log.CreatedBy,
                  DeletedAt = log.DeletedAt,
                  DeletedBy = log.DeletedBy,
                  DisplayOrder = log.DisplayOrder,
                  LastModifiedAt = log.LastModifiedAt,
                  LastModifiedBy = log.LastModifiedBy,
                  Visible = log.Visible
              });

            return this.Json(viewModelList.ToDataSourceResult(request));
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult GetVisible([DataSourceRequest] DataSourceRequest request)
        {
            var viewModelList = this.Repository.GetAllVisible().Select(log =>
              new TVm
              {
                  Id = log.Id,
                  CreatedAt = log.CreatedAt,
                  CreatedBy = log.CreatedBy,
                  DeletedAt = log.DeletedAt,
                  DeletedBy = log.DeletedBy,
                  DisplayOrder = log.DisplayOrder,
                  LastModifiedAt = log.LastModifiedAt,
                  LastModifiedBy = log.LastModifiedBy,
                  Visible = log.Visible
              });

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// The get new view model.
        /// </summary>
        /// <returns>
        /// The <see cref="TVm"/>.
        /// </returns>
        protected TVm GetNewViewModel()
        {
            return (TVm)Activator.CreateInstance(typeof(TVm));
        }

        /// <summary>
        /// Servers the validation.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        protected virtual bool ServerValidation(TVm viewModel)
        {
            return viewModel != null && this.ModelState.IsValid;
        }
    }
}

