﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LookUpControllerBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The look up controller base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Data;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Service.Infrastructure;
    using Service.Models;
    using Service.Repository;
    using Service.ViewModels;

    /// <summary>
    /// Class LookUpControllerBase.
    /// </summary>
    /// <typeparam name="T"> Index type </typeparam>
    /// <typeparam name="TM">The type of the tm.</typeparam>
    /// <typeparam name="TVm">The type of the t vm.</typeparam>
    /// <typeparam name="TR">The type of the tr.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.WebCore.Controllers.AbstractControllerBase" />
  //  [Authorize(Roles = ApplicationRoles.Users)]
    [HandleError]
    public abstract class LookUpControllerBase<T, TM, TVm, TR> : BasicLookUpControllerBase<T, TM, TVm, TR>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
                                   where TM : class, ILookUp<T>
                                   where TVm : LookUpViewModelBase<T, TM>, new()
                                   where TR : ILookUpRepository<T, TM>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LookUpControllerBase{T, TM, TVm, TR}"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        protected LookUpControllerBase(TR repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }

        /// <summary>
        /// Gets the visible drop down list.
        /// </summary>
        /// <returns>Returns JsonResult.</returns>
        [HttpPost]
        public abstract JsonResult GetVisibleDropDownList();

        /// <summary>
        /// Looks up value exists.
        /// </summary>
        /// <param name="name">Name of the s.</param>
        /// <returns> JSON Result.</returns>
        public JsonResult LookUpValueExists(string name)
        {
            var user = this.Repository.GetByName(name.Trim());
            return user == null ? this.Json(true, JsonRequestBehavior.AllowGet) :
                this.Json(
                    $"{name} is already defined.",
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public override ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            var viewModelList = this.Repository.GetAll().Select(log =>
              new TVm
              {
                  Id = log.Id,
                  Name = log.Name,
                  CreatedAt = log.CreatedAt,
                  CreatedBy = log.CreatedBy,
                  DeletedAt = log.DeletedAt,
                  DeletedBy = log.DeletedBy,
                  DisplayOrder = log.DisplayOrder,
                  LastModifiedAt = log.LastModifiedAt,
                  LastModifiedBy = log.LastModifiedBy,
                  Visible = log.Visible
              });

            return this.Json(viewModelList.ToDataSourceResult(request));
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public override ActionResult GetVisible([DataSourceRequest] DataSourceRequest request)
        {
            var viewModelList = this.Repository.GetAllVisible().Select(log =>
              new TVm
              {
                  Id = log.Id,
                  Name = log.Name,
                  CreatedAt = log.CreatedAt,
                  CreatedBy = log.CreatedBy,
                  DeletedAt = log.DeletedAt,
                  DeletedBy = log.DeletedBy,
                  DisplayOrder = log.DisplayOrder,
                  LastModifiedAt = log.LastModifiedAt,
                  LastModifiedBy = log.LastModifiedBy,
                  Visible = log.Visible
              });

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }
    }
}
