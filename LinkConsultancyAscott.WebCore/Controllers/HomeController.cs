﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Class HomeController.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using LinkConsultancyAscott.Service.Services.CataleyaNodeService;
using LinkConsultancyAscott.Service.Services.CataleyaService;
using LinkConsultancyAscott.Service.Services.CataleyaSourceListService;
using LinkConsultancyAscott.Service.Services.CataleyaZoneService;
using LinkConsultancyAscott.Service.Services.HomeService;
using LinkConsultancyAscott.Service.Services.PartitionLabel;
using LinkConsultancyAscott.Service.Services.SipInterfaceService;
using LinkConsultancyAscott.Service.Services.SnitchService;
using LinkConsultancyAscott.Service.Types;
using LinkConsultancyAscott.Service.ViewModels.Cataleya;
using LinkConsultancyAscott.Service.ViewModels.Snitch;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Mvc;

namespace LinkConsultancyAscott.WebCore.Controllers
{
    /// <summary>
    /// Class HomeController.
    /// </summary>
    /// <seealso cref="Controller" />
    //  [Authorize(Roles = ApplicationRoles.Users)]
    public class HomeController : AbstractControllerBase
    {
        /// <summary>
        /// The home service
        /// </summary>
        private readonly IHomeService homeService;
        private readonly ICataleyaService cataleyaService;
        private readonly ICataleyaNodeService cataleyaNodeService;
        private readonly ICataleyaZoneService cataleyaZoneService;
        private readonly ICataleyaSourceListService cataleyaSourceListService;
        private readonly ICataleyaSipInterfaceService cataleyaSipInterfaceService;
        private readonly IPartitionLabelService partitionLabelService;
        private readonly ISnitchService snitchService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="homeService">The home service.</param>
        public HomeController(IHomeService homeService, ICataleyaService cataleyaService,
            ICataleyaNodeService cataleyaNodeService, ICataleyaZoneService cataleyaZoneService,
            ICataleyaSipInterfaceService cataleyaSipInterfaceService, ISnitchService snitchService,
            ICataleyaSourceListService cataleyaSourceListService, IPartitionLabelService partitionLabelService)
        {
            this.homeService = homeService;
            this.cataleyaService = cataleyaService;
            this.snitchService = snitchService;
            this.cataleyaNodeService = cataleyaNodeService;
            this.cataleyaZoneService = cataleyaZoneService;
            this.partitionLabelService = partitionLabelService;
            this.cataleyaSourceListService = cataleyaSourceListService;
            this.cataleyaSipInterfaceService = cataleyaSipInterfaceService;
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        /// <returns>Action Result.</returns>
        public ActionResult Start()
        {
            try
            {
                this.ViewBag.Title = AppConstants.AppTitle + "Start";
                var response = this.snitchService.ReadIpDataDateList();
                this.ViewBag.SnitchDateStamp = response.LastUpdate;
                this.ViewBag.SnitchDataCount = response.DataCount;
                return this.View();
            }
            catch (Exception e)
            {
                Console.Write(e);
                throw;
            }
        }

        /// <summary>
        /// Robots this instance.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [AllowAnonymous]
        public ActionResult Robots()
        {
            Response.ContentType = "text/plain";
            return View();
        }

        /// <summary>
        /// Errors this instance.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        public ActionResult Error()
        {
            this.ViewBag.Title = AppConstants.AppTitle + "Error";
            Exception exception = this.Server.GetLastError();
            System.Diagnostics.Debug.WriteLine(exception);
            return this.View();

        }

        /// <summary>
        /// Sync all the Nodes from Cattaleya to Db
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> GetAllNodes()
        {
            try
            {
                var sourceListFromDbInitial = this.cataleyaSourceListService.ReadCataleyaSourceList();

                this.DeleteAllData();
                var response = await this.cataleyaService.CataleyaNodeListAsync();
                if (response != null && response.Count > 0)
                {
                    var partitionLabels = this.partitionLabelService.ReadPartitionLabelList();
                    foreach (var item in response)
                    {
                        if (!this.cataleyaNodeService.CheckIfCataleyaNodeAlreadyExists(item.id))
                        {
                            var result = this.cataleyaNodeService.CreateCataleyaNode(item);
                            foreach (var partitionLabel in partitionLabels)
                            {
                                if (!string.IsNullOrEmpty(partitionLabel.PartitionLabelId) || !partitionLabel.PartitionLabelId.Equals("0"))
                                {
                                    await this.GetAllZones(result.id, Convert.ToInt32(partitionLabel.PartitionLabelId));
                                }
                            }
                        }
                        else
                        {
                            foreach (var partitionLabel in partitionLabels)
                            {
                                await this.GetAllZones(item.id, Convert.ToInt32(partitionLabel.PartitionLabelId));
                            }
                        }
                    }
                }

                var sourceListFromDbFinal = this.cataleyaSourceListService.ReadCataleyaSourceList();

                if (sourceListFromDbInitial.Count == 0)
                {
                    return RedirectToAction("Start", "Home");
                }

                if (sourceListFromDbInitial.Count < sourceListFromDbFinal.Count)
                {
                    await this.cataleyaService.DestroyNewSourceListFromSecurityACL(sourceListFromDbInitial, sourceListFromDbFinal);
                }
                return RedirectToAction("Start", "Home");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Sync all the zones, sip interfaces, sourcelist from cataleya
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        private async Task GetAllZones(int nodeId, int operatorId)
        {
            try
            {
                var response = await this.cataleyaService.CataleyaZoneListAsync(nodeId, operatorId);

                if (response != null && response.data != null)
                {
                    foreach (var zone in response.data)
                    {
                        if (!this.cataleyaZoneService.CheckIfCataleyaZoneAlreadyExists(zone.compoundKey.id, nodeId, operatorId))
                        {
                            this.cataleyaZoneService.CreateCataleyaZone(zone);
                        }
                        foreach (var sipInterface in zone.sipInterfaces)
                        {
                            if (!this.cataleyaSipInterfaceService.CheckIfCataleyaSipInterfaceAlreadyExists(sipInterface.compoundKey.id, zone.compoundKey.id))
                            {
                                this.cataleyaSipInterfaceService.CreateCataleyaSipInterface(nodeId, zone.compoundKey.id, sipInterface);
                            }
                        }
                        foreach (var sourceList in zone.sourceLists)
                        {
                            var json = JsonConvert.SerializeObject(sourceList);
                            var typedSourceList = JsonConvert.DeserializeObject<CataleyaSourceListViewModel>(json);
                            if (!this.cataleyaSourceListService.CheckIfCataleyaSourceListAlreadyExists(typedSourceList.id, typedSourceList.zoneId, typedSourceList.nodeId))
                            {
                                this.cataleyaSourceListService.CreateCataleyaSourceList(typedSourceList);
                            }

                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Delete all the nodes, zones, sip interfaces and sourcelist from Db
        /// </summary>
        private void DeleteAllData()
        {
            this.cataleyaSourceListService.DeleteAllCataleyaSourceList();
            this.cataleyaSipInterfaceService.DeleteAllCataleyaSipInterfaces();
            this.cataleyaZoneService.DeleteAllCataleyaZones();
            this.cataleyaNodeService.DeleteAllCataleyaNodes();
        }
    }
}