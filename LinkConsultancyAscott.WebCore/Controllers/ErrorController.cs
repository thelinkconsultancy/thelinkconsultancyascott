﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorController.cs" company="">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Class ErrorController.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Class ErrorController.
    /// </summary>
    [HandleError]
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorController" /> class.
        /// </summary>
        public ErrorController()
        {
        }
        
        public ActionResult Index()
        {
            return this.View();
        }
    }
}
