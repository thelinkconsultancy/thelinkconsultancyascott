﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CurrentUserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The CurrentUserService Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Services.CurrentUserService
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Web;

    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;

    /// <summary>
    /// CurrentUserService class
    /// </summary>
    public class CurrentUserService : ICurrentUserService
    {
        /// <summary>
        /// Users the name.
        /// </summary>
        /// <returns>System.String.</returns>
        public string UserName()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return null;
            }

            return HttpContext.Current.User.Identity.Name; // When auth - id stored in cookie
        }

        /// <summary>
        /// Gets the cataleya API URL.
        /// </summary>
        /// <value>The cataleya API URL.</value>
        public string CataleyaApiUrl => this.GetValueClaimType(AppConstants.CataleyaApiUrlKey);

        /// <summary>
        /// Gets the cataleya username.
        /// </summary>
        /// <value>The cataleya username.</value>
        public string CataleyaUsername => this.GetValueClaimType(AppConstants.CataleyaUsernameKey);

        /// <summary>
        /// Gets the cataleya password.
        /// </summary>
        /// <value>The cataleya password.</value>
        public string CataleyaPassword => this.GetValueClaimType(AppConstants.CataleyaPasswordKey);

        /// <summary>
        /// Gets the cataleya default node identifier.
        /// </summary>
        /// <value>The cataleya default node identifier.</value>
        public int CataleyaDefaultNodeId => Convert.ToInt32("2"/*this.GetValueClaimType(AppConstants.CataleyaDefaultNodeId)*/);

        /// <summary>
        /// Gets the type of the value claim.
        /// </summary>
        /// <param name="claimType">Type of the claim.</param>
        /// <returns>System.String.</returns>
        private string GetValueClaimType(string claimType)
        {
            string retVal = null;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return null;
            }

            if (HttpContext.Current.User.Identity is ClaimsIdentity claimsIdentity)
            {
                var claim = claimsIdentity.Claims
                    .FirstOrDefault(c => c.Type.Equals(claimType, StringComparison.InvariantCultureIgnoreCase));

                if (claim != null)
                {
                    retVal = claim.Value;
                }
            }

            return retVal;
        }
    }
}
