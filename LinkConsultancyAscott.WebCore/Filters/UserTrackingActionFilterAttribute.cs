﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTrackingActionFilterAttribute.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the UserTrackingActionFilterAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Filters
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;

    using Data;

    using Service.Repository.SiteActivityLogRepository;

    /// <summary>
    /// The user tracking action filter attribute.
    /// </summary>
    [System.AttributeUsageAttribute(System.AttributeTargets.All, AllowMultiple = false)]
    public class UserTrackingActionFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// The _stopwatch
        /// </summary>
        private readonly Stopwatch stopwatch;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserTrackingActionFilterAttribute"/> class.
        /// </summary>
        public UserTrackingActionFilterAttribute()
        {
            this.stopwatch = new Stopwatch();
        }

        /// <summary>
        /// Called when [action executing].
        /// </summary>
        /// <param name="filterContext">The action context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            this.stopwatch.Start();
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Called when [action executed].
        /// </summary>
        /// <param name="filterContext">The action executed context.</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.stopwatch.Stop();

            System.Web.HttpContextBase httpContext = filterContext.HttpContext;
                var actionDescriptor = filterContext.ActionDescriptor;

                // Want to filter out some of the controllers like tooltips
                if (actionDescriptor.ControllerDescriptor.ControllerName != "ToolTips")
                {
                    var bc = httpContext.Request.Browser;

                    string routeIds =
                        string.Join(
                        " ",
                        filterContext.RouteData.Values.Select(pair => $"{pair.Key}={pair.Value}"));

                string area = this.GetArea(actionDescriptor.ControllerDescriptor);

                var data = new SiteActivityLog
                        {
                    ActivityTimestamp = httpContext.Timestamp,
                            ActionDuration = this.stopwatch.Elapsed,
                            Controller = actionDescriptor.ControllerDescriptor.ControllerName,
                            Action = actionDescriptor.ActionName,
                            Browser = bc.Browser + " " + bc.Version,
                            UserName = httpContext.User.Identity.Name,
                            IPAddress = httpContext.Request.UserHostAddress,
                            IsMobileDevice = bc.IsMobileDevice,
                            RouteInfo = routeIds,
                            Platform = httpContext.Request.UserAgent,
                            Domain = this.GetDomain(httpContext),
                            Area = area,
                            HttpVerb = httpContext.Request.HttpMethod,
                            ResponseStatus = httpContext.Response != null ? httpContext.Response.Status : string.Empty,
                            ResponseStatusDescription = httpContext.Response != null ? httpContext.Response.StatusDescription : string.Empty,
                            ResponseData = httpContext.Response != null ? httpContext.Error?.ToString() ?? string.Empty : string.Empty
                };
                    this.stopwatch.Reset();

                    // Store it somewhere
                    this.SaveData(data);
                }

            base.OnActionExecuted(filterContext);
        }

        /// <summary>
        /// Gets the domain.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>System String.</returns>
        private string GetDomain(System.Web.HttpContextBase context) 
        {
            // Return variable declaration
            var appPath = string.Empty;

            // Checking the current context content
            if (context?.Request != null && context.Request.Url != null) 
            {
                // Formatting the fully qualified website url/name
                appPath =
                    $"{context.Request.Url.Scheme}://{context.Request.Url.Host}{(context.Request.Url.Port == 80 ? string.Empty : ":" + context.Request.Url.Port)}{context.Request.ApplicationPath}";
            }

            if (!appPath.EndsWith("/"))
            {
                appPath += "/";
            }

            return appPath;
        }

        /// <summary>
        /// Saves the data.
        /// </summary>
        /// <param name="data">The data.</param>
        private void SaveData(SiteActivityLog data)
        {
            this.SaveDataToDatabase(data);
        }

        /// <summary>
        /// Saves the data to database.
        /// </summary>
        /// <param name="data">The data.</param>
        private void SaveDataToDatabase(SiteActivityLog data)
        {
            var repo = DependencyResolver.Current.GetService<ISiteActivityLogRepository>();
            repo.AddViaTransaction(data);
        }

        /// <summary>
        /// Gets the area.
        /// </summary>
        /// <param name="controllerDescriptor">The controller descriptor.</param>
        /// <returns>System String.</returns>
        private string GetArea(ControllerDescriptor controllerDescriptor)
        {
            var areaName = string.Empty;

            if (!string.IsNullOrEmpty(controllerDescriptor?.ControllerType?.Namespace))
            {
                var parts = controllerDescriptor.ControllerType.Namespace.Split('.').ToList();
                var areaIndex = parts.IndexOf("Areas");
                if (areaIndex > -1)
                {
                    areaName = parts[areaIndex + 1];
                }
            }

            return areaName;
        }
    }
}