﻿
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTypeManagementController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Requirement Type Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository.EmailTypeRepository;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using LinkConsultancyAscott.Service.ViewModels.Administration;
    using LinkConsultancyAscott.WebCore.Controllers;

    /// <summary>
    /// The Requirement Type controller.
    /// </summary>
    [HandleError]
    [Authorize(Roles = Service.Models.ApplicationRoles.Administrators + "," + Service.Models.ApplicationRoles.Developers)]
    public class PartitionLabelDrpController :
        LookUpControllerBase<int, EmailType, EmailTypeViewModel, IEmailTypeRepository>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTypeManagementController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        public PartitionLabelDrpController(IEmailTypeRepository repository, IUnitOfWork unitOfWork)
            : base(repository, unitOfWork)
        {
            this.Message = "Edit Email Type";
        }

        /// <summary>
        /// Gets the visible drop down list.
        /// </summary>
        /// <returns>Returns JsonResult.</returns>
        [HttpPost]
        public override JsonResult GetVisibleDropDownList()
        {
            var viewModelList = this.Repository.GetAllVisible().Select(log =>
              new PartitionLabelViewModel
              {
                  PartitionId = log.Id,
                  PartitionLabels = log.Name
                
              });

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }
    }
}


