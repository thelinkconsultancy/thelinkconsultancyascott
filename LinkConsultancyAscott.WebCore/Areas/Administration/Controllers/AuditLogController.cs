﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditLogController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Site Activity Log Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Services.AuditLogService;
    using LinkConsultancyAscott.WebCore.Controllers;

    /// <summary>
    /// Class AdministrationUserController.
    /// </summary>
    [Authorize(Roles = ApplicationRoles.Developers)]
    public class AuditLogController : AbstractControllerBase
    {
        /// <summary>
        /// The Site Activity Log service
        /// </summary>
        private readonly IAuditLogService AuditLogService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditLogController"/> class. 
        /// </summary>
        /// <param name="AuditLogService">
        /// The Site Activity Log Service.
        /// </param>
        public AuditLogController(IAuditLogService AuditLogService)
        {
            this.AuditLogService = AuditLogService;
        }

        /// <summary>
        /// The log list.
        /// </summary>
        /// <returns>The <see cref="ActionResult" />.</returns>
        public ActionResult Log()
        {
            return this.PartialView();
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Action Result.</returns>
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return this.Json(
                this.AuditLogService
                .Read().ToDataSourceResult(request), 
                JsonRequestBehavior.AllowGet);
        }
    }
}
