﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdministrationUserController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Administration User Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Service.Services.UserService;
    using Service.ViewModels.Administration;

    using LinkConsultancyAscott.Service.Models;

    using WebCore.Controllers;

    /// <summary>
    /// Class AdministrationUserController.
    /// </summary>
    [Authorize(Roles = ApplicationRoles.Administrators + "," + ApplicationRoles.Developers)]
    public class AdministrationUserController : AbstractControllerBase
    {
        /// <summary>
        /// The user service
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdministrationUserController"/> class.
        /// </summary>
        /// <param name="userService">
        /// The unit of work.
        /// </param>
        public AdministrationUserController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// The user list.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult List()
        {
            return this.PartialView();
        }

        /// <summary>
        /// Claimses the list.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ClaimsList(string userId)
        {
            return this.PartialView("ClaimsList", userId);
        }

        /// <summary>
        /// Reads the user claims.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ReadUserClaims([DataSourceRequest] DataSourceRequest request, string userId)
        {
            return this.Json(this.userService.GetUserClaims(userId).ToDataSourceResult(request));
        }

        /// <summary>
        /// Updates the user claim.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult UpdateUserClaim([DataSourceRequest] DataSourceRequest request, UserClaimViewModel viewModel)
        {
            if (viewModel != null && this.ModelState.IsValid)
            {
                viewModel = this.userService.UpdateUserClaim(viewModel);
            }

            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// The ajax log list.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return this.Json(this.userService.GetAllUsers().ToDataSourceResult(request));
        }

        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, UserViewModel viewModel)
        {
            // Perform model binding (fill the product properties and validate it).           
            if (viewModel != null && this.ModelState.IsValid)
            {
                viewModel = this.userService.CreateUser(viewModel);
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Updates the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, UserViewModel viewModel)
        {
            if (viewModel != null && this.ModelState.IsValid)
            {
                viewModel = this.userService.UpdateUser(viewModel);
            }

            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Deletes the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, UserViewModel viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                this.userService.DeleteUser(viewModel.UserName);
            }

            // Rebind the grid       
            return this.Json(this.ModelState.ToDataSourceResult());
        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <returns>Json Result.</returns>
        public JsonResult GetRoles()
        {
            var dtoList = this.userService.GetAllRoles();

            var data = dtoList.Select(e => new UserRoleViewModel
            {
                Id = e.Id,
                Name = e.Name
            }).ToList();

            return this.Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
