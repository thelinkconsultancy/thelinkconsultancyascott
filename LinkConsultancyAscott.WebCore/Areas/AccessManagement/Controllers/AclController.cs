﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the ACL Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.AccessManagement.Controllers
{
    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;
    using LinkConsultancyAscott.Service.Extensions;
    using LinkConsultancyAscott.Service.Services.AclService;
    using LinkConsultancyAscott.Service.Services.CataleyaNodeService;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.Services.PartitionLabel;
    using LinkConsultancyAscott.Service.Services.UserService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    using Microsoft.AspNet.Identity.Owin;
    using Service.Infrastructure;
    using Service.Models;
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using WebCore.Controllers;

    /// <summary>
    /// Class CDR Data Pack Upload Controller.
    /// </summary>
   // [Authorize(Roles = ApplicationRoles.Users)]
    public class AclController : AbstractControllerBase
    {
        public class Employee
        {
            public int EmployeeID { get; set; }

            public string EmployeeName { get; set; }
        }

        /// <summary>
        /// The CDR data pack upload service
        /// </summary>
        private readonly IAclService aclService;
        private readonly IPartitionLabelService partitionLabelService;
        private readonly IUserPartitionMappingService partitionMappingService;
        private readonly IUserService userService;
        private readonly ICataleyaService cataleyaService;
        private readonly ICataleyaNodeService cataleyaNodeService;
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return this._userManager ?? this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                this._userManager = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AclController"/> class.
        /// </summary>
        /// <param name="aclService">The service.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public AclController(IAclService aclService, IUnitOfWork unitOfWork,
            IPartitionLabelService partitionLabelService,
            IUserPartitionMappingService partitionMappingService,
            ICataleyaService cataleyaService, IUserService userService,
            ICataleyaNodeService cataleyaNodeService)
        {
            this.aclService = aclService;
            this.partitionLabelService = partitionLabelService;
            this.partitionMappingService = partitionMappingService;
            this.cataleyaNodeService = cataleyaNodeService;
            this.cataleyaService = cataleyaService;
            this.userService = userService;
        }

        /// <summary>
        /// Index Page
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [HttpGet]
        public ActionResult AclHistory()
        {
            this.ViewBag.Title = AppConstants.AppTitle + "ACL History";
            return this.View();
        }

        /// <summary>
        /// Lists the specified ACL list type.
        /// </summary>
        /// <param name="aclListType">Type of the ACL list.</param>
        /// <returns>Return ActionResult.</returns>
        public ActionResult AclList(AclListType aclListType)
        {
            this.ViewBag.Title = AppConstants.AppTitle + aclListType.GetDescription();
            return this.View(aclListType);
        }

        /// <summary>
        /// Cataleyas the ACL list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaAclList()
        {
            return this.View();
        }

        /// <summary>
        /// Cataleyas the sip interface list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaSipInterfaceList()
        {
            return this.View();
        }

        /// <summary>
        /// Cataleyas the IP interface list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaIpInterfaceList()
        {
            return this.View();
        }

        /// <summary>
        /// Cataleyas the media interface list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaMediaInterfaceList()
        {
            return this.View();
        }

        /// <summary>
        /// Reads the cataleya acl list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaAclList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaAclList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the cataleya sip interface list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns Task&lt;ActionResult&gt;.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaSipInterfaceList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaSipInterfaceList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the cataleya IP interface list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns Task&lt;ActionResult&gt;.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaIpInterfaceList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaIpInterfaceList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the cataleya media interface list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Task&lt;ActionResult&gt;.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaMediaInterfaceList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaMediaInterfaceList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns ActionResult.</returns>
        public virtual ActionResult ReadAclHistory([DataSourceRequest] DataSourceRequest request)
        {
            var showAllRows = this.User.IsInRole(ApplicationRoles.Administrators) || this.User.IsInRole(ApplicationRoles.Developers);

            var list = this.aclService.ReadAclHistory(this.UserName, showAllRows);
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Destroys the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Returns ActionResult.</returns>
        [HttpPost]
        public ActionResult DestroyAclHistory([DataSourceRequest] DataSourceRequest request, AclHistoryViewModel viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                this.aclService.DestroyAclHistory(viewModel);
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Reads the acl list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <returns>Returns ActionResult.</returns>
        public ActionResult ReadAclList([DataSourceRequest] DataSourceRequest request, AclListType aclListType)
        {
            var showAllRows = this.User.IsInRole(ApplicationRoles.Administrators) || this.User.IsInRole(ApplicationRoles.Developers);

            var list = this.aclService.ReadAclList(aclListType, this.UserName, showAllRows);
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual async Task<ActionResult> CreateAclList([DataSourceRequest] DataSourceRequest request, AclListViewModel viewModel)
        {
            AclListViewModel newViewModel;

            // Perform model binding (fill the product properties and validate it).           
            if (viewModel != null && this.ModelState.IsValid)
            {
                // The model is valid - insert the product. 
                newViewModel = await this.aclService.CreateAclList(viewModel, this.UserName).ConfigureAwait(false);
            }
            else
            {
                newViewModel = viewModel;
            }

            // Rebind the grid       
            return this.Json(new[] { newViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Updates the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult UpdateAclList([DataSourceRequest] DataSourceRequest request, AclListViewModel viewModel)
        {
            AclListViewModel updatedViewModel;

            if (viewModel != null && this.ModelState.IsValid)
            {
                updatedViewModel = this.aclService.UpdateAclList(viewModel);
            }
            else
            {
                updatedViewModel = viewModel;
            }

            return this.Json(new[] { updatedViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Deletes the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult DestroyAclList([DataSourceRequest] DataSourceRequest request, AclListViewModel viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                this.aclService.DestroyAclList(viewModel);
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        [HttpGet]
        public ActionResult UploadIpAddressFile(AclListType listType)
        {
            var viewModel = new UploadIpAddressFileViewModel
            {
                ListType = listType,
                ListTypeName = listType.GetDescription()
            };

            return this.PartialView(viewModel);
        }

        /// <summary>
        /// Saves the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveIpAddressFile(UploadIpAddressFileViewModel viewModel)
        {
            // The Name of the Upload component is "files"
            if (viewModel != null)
            {
                this.aclService.SaveIpAddressFile(viewModel, this.UserName, this.BaseUrl);
            }

            // Return an empty string to signify success
            return this.Content(string.Empty);
        }

        public ActionResult RemoveIpAddressFile(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {

                }
            }

            // Return an empty string to signify success
            return Content(string.Empty);
        }

        [HttpGet]
        public ActionResult PartitionLabel()
        {
            this.ViewBag.Title = "TLC - Partition Label";
            return this.View();//"AccessManagement/Acl/PartitionLabelView");
        }

        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult CreatePartionLabel([DataSourceRequest] DataSourceRequest request, PartitionLabelViewModel viewModel)
        {
            PartitionLabelViewModel addViewModel = this.partitionLabelService.CreatePartitionLabel(viewModel);
            return this.Json(new[] { addViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        public JsonResult GetPartitionLabelsForDropdown()
        {
            var data = this.partitionLabelService.ReadPartitionLabelList();
            return this.Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the acl list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <returns>Returns ActionResult.</returns>
        public ActionResult ReadPartitionLabelList([DataSourceRequest] DataSourceRequest request, PartitionLabelViewModel partitionLabel)
        {
            var showAllRows = this.User.IsInRole(ApplicationRoles.Administrators) || this.User.IsInRole(ApplicationRoles.Developers);

            var list = this.partitionLabelService.ReadPartitionLabelList();// partitionLabel, this.UserName, showAllRows);
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePartionLabel([DataSourceRequest] DataSourceRequest request, PartitionLabelViewModel viewModel)
        {
            PartitionLabelViewModel updatedViewModel;

            if (viewModel != null && this.ModelState.IsValid)
            {
                updatedViewModel = this.partitionLabelService.UpdatePartitionLabel(viewModel);
            }
            else
            {
                updatedViewModel = viewModel;
            }

            return this.Json(new[] { updatedViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        [HttpPost]
        public ActionResult DestroyPartitionMapping([DataSourceRequest] DataSourceRequest request, PartitionLabelViewModel viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                this.partitionLabelService.DeletePartitionLabel(viewModel);
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        [HttpGet]
        public ActionResult UserPartitionMapping()
        {

            //  using (var db = new LinkConsultancyAscottEntities(this.DatabaseFactory.ConnectionString()))
            //{
            //ViewData["employees"] = new NorthwindDataContext()
            //         .Employees
            //         .Select(e => new Employee
            //         {
            //             EmployeeID = e.EmployeeID,
            //             EmployeeName = e.FirstName + " " + e.LastName
            //         })
            //         .OrderBy(e => e.EmployeeName);



            this.ViewBag.Title = "TLC - User Partition Mapping";

            ViewBag.PartitionsList = this.partitionLabelService.ReadPartitionLabelList();
            return this.View();//"AccessManagement/Acl/PartitionLabelView");
        }

        /// <summary>
        /// Reads the acl list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <returns>Returns ActionResult.</returns>
        public ActionResult ReadUserPartitionMappingList([DataSourceRequest] DataSourceRequest request, UserPartitionMappingViewModel partitionLabel)
        {
            var showAllRows = this.User.IsInRole(ApplicationRoles.Administrators) || this.User.IsInRole(ApplicationRoles.Developers);

            var list = this.partitionMappingService.ReadUserPartitionMappingList();// partitionLabel, this.UserName, showAllRows);
            foreach (var item in list)
            {

            }

            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateUserPartitionMapping([DataSourceRequest] DataSourceRequest request, UserPartitionMappingViewModel viewModel)
        {
            try
            {
                string userRole = string.Empty;
                UserPartitionMappingViewModel updatedViewModel;
                viewModel.PartitionId = viewModel.Partition.PartitionId;

                if (viewModel != null && this.ModelState.IsValid)
                {
                    var res = await this.UserManager.RemoveFromRoleAsync(viewModel.TLCId, "Administrators");
                    var result = await this.UserManager.RemoveFromRoleAsync(viewModel.TLCId, "User");

                    //this.userService.RemoveUserRole(viewModel.TLCEmail, "User");
                    if (viewModel.TLCRole.Id == 1)
                    {
                        userRole = "Administrators";
                    }
                    else
                    {
                        userRole = "Users";
                    }

                    await this.UserManager.AddToRoleAsync(viewModel.TLCId, userRole);

                    updatedViewModel = await this.partitionMappingService.UpdateUserPartitionMapping(viewModel);
                }
                else
                {
                    updatedViewModel = viewModel;
                }

                return this.Json(new[] { updatedViewModel }.ToDataSourceResult(request, this.ModelState));
            }
            catch (System.Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> DestroyeUserPartitionMapping([DataSourceRequest] DataSourceRequest request, UserPartitionMappingViewModel viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                await this.partitionMappingService.DeleteUserPartitionMapping(viewModel);
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        [HttpPost]
        public async Task<ActionResult> CreateUserPartitionMapping([DataSourceRequest] DataSourceRequest request, UserPartitionMappingViewModel viewModel)
        {


            UserPartitionMappingViewModel updatedViewModel;

            if (viewModel != null && this.ModelState.IsValid)
            {
                updatedViewModel = await this.partitionMappingService.UpdateUserPartitionMapping(viewModel);
            }
            else
            {
                updatedViewModel = viewModel;
            }

            return this.Json(new[] { updatedViewModel }.ToDataSourceResult(request, this.ModelState));



            //LinkConsultancyAscott.Data.PartitionLabel dto = new LinkConsultancyAscott.Data.PartitionLabel();
            //dto.PartitionLabelId = viewModel.PartitionLabelId;
            //dto.PartitionLabels = viewModel.PartitionLabels;
            //dto.IstraIp = viewModel.IstraIp;
            // PartitionLabelViewModel addViewModel = this.partitionLabelService.CreatePartitionLabel(viewModel);
            //     return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        [HttpGet]
        public ActionResult SecurityACL()
        {
            this.ViewBag.Title = "TLC - Security ACL";
            return this.View();
        }

        public ActionResult SecurityAclListTabContent()
        {
            return this.PartialView();
        }
        
        [HttpGet]
        public ActionResult GetAllNodesForDropdownList()
        {
            var list = this.cataleyaNodeService.ReadCataleyaNodeList();
            return this.Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteSecurityACLTabContent()
        {
            return this.PartialView();
        }

        [HttpPost]
        public async Task<JsonResult> DestroySecurityACLByBatchNo(int batchNo)
        {
            try
            {
                var cataleyaAclHistoryByBatch = this.aclService.GetCataleyaAclHistory(batchNo);
                int newBatchNo = this.aclService.GetLastBatchNo();
                newBatchNo++;
                foreach (var item in cataleyaAclHistoryByBatch)
                {
                    if (item.isDeleted) {
                        return Json("Specified batch is already deleted!");
                    }
                    var isValidAclId = await this.ValidateCataleyaSecurityAclId(item.NodeId, Convert.ToInt32(item.CataleyaSecurityAclId));
                    if (isValidAclId)
                    {
                        if (item.CataleyaSecurityAclId > 0)
                        {
                            await this.cataleyaService.DestroySecurityACL(item.NodeId, Convert.ToInt32(item.CataleyaSecurityAclId), newBatchNo);
                        }
                    }
                    else
                    {
                        return Json("Specified batch is already deleted!", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        [HttpPost]
        public async Task<JsonResult> DestroySecurityACL(int nodeId, string[] securityAclIds)
        {
            try
            {
                if (securityAclIds[0] == "")
                {
                    securityAclIds = null;
                }
                int batchNo = this.aclService.GetLastBatchNo();
                batchNo++;
                if (securityAclIds != null && securityAclIds.Length > 0)
                {
                    foreach (var id in securityAclIds)
                    {
                        var isValidAclId = await this.ValidateCataleyaSecurityAclId(nodeId, Convert.ToInt32(id));
                        if (isValidAclId)
                        {
                            await this.cataleyaService.DestroySecurityACL(nodeId, Convert.ToInt32(id), batchNo);
                        }
                        else
                        {
                            return Json("Wrong ACL Id entered!!!", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    await this.cataleyaService.DestroySecurityACL(nodeId, null, batchNo);
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        private async Task<bool> ValidateCataleyaSecurityAclId(int nodeId, int securityAclId)
        {
            var result = await this.cataleyaService.SecurityAclByIdAsync((int)securityAclId, nodeId);
            if (result.total > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ActionResult ReadCataleyaAclHistoryList([DataSourceRequest] DataSourceRequest request)
        {
            var filter = request.Filters;
            var list = this.aclService.GetCataleyaAclHistory();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}
