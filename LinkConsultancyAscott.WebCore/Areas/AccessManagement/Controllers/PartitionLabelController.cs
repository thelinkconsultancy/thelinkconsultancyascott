﻿using System.Threading.Tasks;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

using LinkConsultancyAscott.Service.Extensions;
using LinkConsultancyAscott.Service.Models;
using LinkConsultancyAscott.Service.Services.AclService;
using LinkConsultancyAscott.Service.Services.PartitionLabel;
using LinkConsultancyAscott.Service.Types;
using LinkConsultancyAscott.Service.ViewModels.Acl;
using LinkConsultancyAscott.Service.ViewModels.Administration;
// using LinkConsultancyAscott.WebCore.Models;

namespace LinkConsultancyAscott.WebCore.Areas.AccessManagement.Controllers
{
//[Authorize(Roles = ApplicationRoles.Users)]
    public class PartitionLabelController : Controller
    {

        /// <summary>
        /// The partition label service
        /// </summary>
       // private readonly IPartitionLabelService partitionLabelService;



        /// <summary>
        /// Initializes a new instance of the <see cref="PartitionLabelController"/> class.
        /// </summary>
        /// <param name="partitionLabelService">The service.</param>
        public PartitionLabelController()//IPartitionLabelService partitionLabelService)// , IPartitionLabelService partitionLabelService)
        {
         //   this.partitionLabelService = partitionLabelService;
        }




        [HttpGet]
        public ActionResult PartitionLabel()
        {
            this.ViewBag.Title = "TLC - Partition Label";
            return this.View();//"AccessManagement/Acl/PartitionLabelView");
        }


        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult CreatePartionLabel([DataSourceRequest] DataSourceRequest request, PartitionLabelViewModel viewModel)
        {
            PartitionLabelViewModel newViewModel;

            // Perform model binding (fill the product properties and validate it).           
            if (viewModel != null && this.ModelState.IsValid)
            {
                // The model is valid - insert the product. 
                newViewModel = viewModel;
                  //  this.partitionLabelService.CreatePartitionLabel(viewModel);//.ConfigureAwait(false);
            }
            else
            {
                newViewModel = viewModel;
            }

            // Rebind the grid       
            return this.Json(new[] { newViewModel }.ToDataSourceResult(request, this.ModelState));
        }



        [HttpGet]
        public ActionResult UserPartitionMapping()
        {
            this.ViewBag.Title = "TLC - User Partition Mapping";
            return this.View();//"AccessManagement/Acl/PartitionLabelView");
        }



    }
}
