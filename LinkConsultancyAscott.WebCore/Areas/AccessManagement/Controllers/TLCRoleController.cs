﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LinkConsultancyAscott.Service.ViewModels.Acl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LinkConsultancyAscott.WebCore.Areas.AccessManagement.Controllers
{
    class TLCRoleListController : Controller
    {
        [HttpGet]
        public ActionResult TLCRoleList()
        {
            this.ViewBag.Title = "TLC - Roles ";
            return this.View();
        }
        
        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult CreateTLCRole([DataSourceRequest] DataSourceRequest request, TLCRoleViewModel viewModel)
        {
            TLCRoleViewModel newViewModel;

            // Perform model binding (fill the product properties and validate it).           
            if (viewModel != null && this.ModelState.IsValid)
            {
                // The model is valid - insert the product. 
                newViewModel = viewModel;
                //  this.partitionLabelService.CreatePartitionLabel(viewModel);//.ConfigureAwait(false);
            }
            else
            {
                newViewModel = viewModel;
            }

            // Rebind the grid       
            return this.Json(new[] { newViewModel }.ToDataSourceResult(request, this.ModelState));
        }
    }
}
