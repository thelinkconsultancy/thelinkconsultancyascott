﻿namespace LinkConsultancyAscott.WebCore
{
    using System.Configuration;

    public static class KendoSettings
    {
        static KendoSettings()
        {
            // Retrieve settings from Web.config. Handle case when not there...
            var appSettings = ConfigurationManager.AppSettings;
            KendoVersion = appSettings["KendoVersion"] ?? "2018.1.117";
            KendoTheme = appSettings["KendoTheme"] ?? "blueopal";
        }

        public static string KendoVersion { get; set; }

        public static string KendoTheme { get; set; }

    }
}