﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinkConsultancyAscottHub.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Class LinkConsultancyAscottHub.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Hubs
{
    using Microsoft.AspNet.SignalR;

    /// <summary>
    /// Class LinkConsultancyAscottHub.
    /// </summary>
    /// <seealso cref="Hub" />
    public class LinkConsultancyAscottHub : Hub
    {
        /// <summary>
        /// The log.
        /// </summary>
        protected static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The hub context
        /// </summary>
        private static readonly IHubContext HubContext = GlobalHost.ConnectionManager.GetHubContext<LinkConsultancyAscottHub>();

        /// <summary>
        /// Clients the initialize.
        /// </summary>
        public void ClientInit()
        {
            Logger.Info("Entered LinkConsultancyAscottHub.ClientInit");
            Logger.Info("Exited  LinkConsultancyAscottHub.ClientInit");
        }

        public static void AclUpdated(string contextUserName)
        {
            Logger.Info("Entered LinkConsultancyAscottHub.AclUpdated [" + contextUserName + "]");
            HubContext.Clients.All.aclUpdated();
            Logger.Info("Exited  LinkConsultancyAscottHub.AclUpdated [" + contextUserName + "]");
        }
    }
}
