﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeControllerTests.cs" company="">
//   
// </copyright>
// <summary>
//   Class HomeControllerTest.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Tests.Controllers
{
    using System.Web.Mvc;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using WebCore.Controllers;

    /// <summary>
    /// Class HomeControllerTest.
    /// </summary>
    [TestClass]
    public class HomeControllerTests
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController(null);

            // Act
            ViewResult result = controller.Start() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
