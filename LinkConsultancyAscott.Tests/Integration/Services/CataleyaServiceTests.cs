﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaServiceTests.cs" company="">
//   
// </copyright>
// <summary>
//   Class CataleyaServiceTests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Tests.Integration.Services
{
    using System;
    using System.Configuration;
    using System.Linq;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Repository.UserRepository;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Should;

    /// <summary>
    /// Class SbcWebApiTests.
    /// </summary>
    [TestClass]
    public class CataleyaServiceTests
    {
        /// <summary>
        /// Tests the list all returns expected result.
        /// </summary>
        [TestMethod]
        public void SecurityAclListAsyncReturnsExpectedResultsTest()
        {
            // Arrange
            var service = this.GetMockedService();

            // Act
            var response = service.SecurityAclListAsync(null).Result;

            // Assert
            response.ShouldNotBeNull();
       }

        [TestMethod]
        public void PublicIpInterfacesFromZonesListAsyncReturnsExpectedZone2ResultsTest()
        {
            // Arrange
            var service = this.GetMockedService();

            // Act
            var response = service.PublicIpInterfacesFromZonesListAsync(2).Result.ToList();

            // Assert
            response.ShouldNotBeNull();
            response.Count.ShouldEqual(1);
        }

        [TestMethod]
        public void PublicIpInterfacesFromZonesListAsyncReturnsExpectedZone1ResultsTest()
        {
            // Arrange
            var service = this.GetMockedService();

            // Act
            var response = service.PublicIpInterfacesFromZonesListAsync(1).Result.ToList();

            // Assert
            response.ShouldNotBeNull();
            response.Count.ShouldEqual(2);
        }

        [TestMethod]
        public void ZoneListAsyncReturnsExpectedResultsTest()
        {
            // Arrange
            var service = this.GetMockedService();

            // Act
            var response = service.ZoneListAsync(2).Result;

            // Assert
            response.ShouldNotBeNull();
        }

        [TestMethod]
        public void TestGetTokenReturnsExpectedResult()
        {
            // Arrange
            var service = this.GetMockedService();

            // Act
            var response = service.GetAccessToken().Result;

            // Assert
            response.ShouldNotBeNull();
        }

        private ICataleyaService GetMockedService()
        {
            var cataleyaApiUrl = ConfigurationManager.AppSettings["cataleyaApiUrl"];
            var cataleyaUsername = ConfigurationManager.AppSettings["cataleyaUsername"];
            var cataleyaPassword = ConfigurationManager.AppSettings["cataleyaPassword"];
            var defaultNodeId = ConfigurationManager.AppSettings["defaultNodeId"];

            Mock<ICalaleyaInfoRepository> calaleyaInfoRepository = new Mock<ICalaleyaInfoRepository>();
            Mock<ICurrentUserService> currentUserService = new Mock<ICurrentUserService>();
            Mock<IAppConfigRepository> appConfigRepository = new Mock<IAppConfigRepository>();

            /*repository.Setup(p => p.GetValue(It.IsAny<string>())).Returns(
                (string key) =>
                    {
                        if (key.Equals(AppConstants.CataleyaApiUrlKey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return cataleyaApiUrl;
                        }

                        if (key.Equals(AppConstants.CataleyaUsernameKey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return cataleyaUsername;
                        }

                        if (key.Equals(AppConstants.CataleyaPasswordKey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return cataleyaPassword;
                        }

                        if (key.Equals(AppConstants.CataleyaDefaultNodeId, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return defaultNodeId;
                        }
                        
                        return string.Empty;
                    });*/

            return new CataleyaService(calaleyaInfoRepository.Object, currentUserService.Object, appConfigRepository.Object);
        }
    }
}
